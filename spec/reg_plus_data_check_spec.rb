require "json"
require "date"
require "rspec"
require_relative "data_check_spec_helper.rb"
require_relative "../pages/reg.rb"
require_relative "../db/AccountDB.rb"
require_relative "../db/RsubscriberDB.rb"
require_relative "../db/CpnDB.rb"
require_relative "../db/EndPointDB.rb"
require_relative "../db/LocationDB.rb"
require_relative "../db/EmailAddressDB.rb"
require_relative "../db/CreditCardDB.rb"
require_relative "../db/IppDB.rb"
require_relative "../db/RegDB.rb"
require_relative "../db/OrdersDB.rb"
require_relative "../db/OrderDetailDB.rb"
include RSpec::Expectations

describe "Device Registration" do


  before(:each) do

    d = DateTime.now
    @lname = "SCHNEIDER"
    @email ="STEVE.SCHNEIDER%"+@fname+"@MAGICJACK.COM"
    

  end
  

#OLD:   def register_with(jack_type, fname, numType, renewType, ippType, addr911, inCity, actCodeNotification)   

#NEW: def register_with(jack_type, fname, actCodeNotification, numType, serviceType, addr911, inCity, renewType, ippType, newbillLocation) 

  it "Registers a plus device with a US number, incity 911, no renewal 10 IPP, no auto renew" do    
	@registerJack.register_with(@genPlus,@fname,'EMAIL','US','BOTH','SKIP','inCityYes','SKIP', 'SKIP','NO')
		#@registerJack.register_with(@genExpress,@fname,'EMAIL','US','BOTH','911addressCorrect','inCityYes','SKIP', 'IPP10','NO')
		 
	    @accountData.set_account_data(@fname)
	    @IppData.set_ipp_data(@accountData.get_accountid)
	    
  end
  
  
	 
  it "Tests account data after basic registration" do
	       
	      @accountData.set_account_data(@fname)
	      @locationData.set_location_data(@accountData.get_locationid)
	      @emailaddressData.set_email_address_data(@accountData.get_emailaddressid)

				expect(@accountData.get_active).to eq('Y')
				expect(@accountData.get_customer_name).to eq(@fname + " " + @lname)
				expect(@accountData.get_account_type).to eq('M')
				expect(@accountData.get_busres).to eq('R')
				expect(@accountData.get_loyal).to eq('N')
				expect(@accountData.get_locationid).to eq @locationData.get_locationid

  end

  it "Tests rsub data after basic registration" do
	       
				rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
				@rsubscriberData.set_rsubscriber_data(rsubscriberPK)
				
				@cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
				@endpointData.set_endpoint_data(@rsubscriberData.get_endpointid)
			
				expect(@rsubscriberData.get_accountid).to_not be_zero
				expect(@rsubscriberData.get_rsubscriberid).to eq rsubscriberPK
				expect(@rsubscriberData.get_active).to eq('Y')
				expect(@rsubscriberData.get_accountid).to eq @accountData.get_accountid
				expect(@rsubscriberData.get_cpnid).to eq @cpnData.get_cpnid
				expect(@rsubscriberData.get_startservice).to eq(@currentDate)
				expect(@rsubscriberData.get_endservice).to eq(@plusExpDate)
				expect(@rsubscriberData.get_device_type).to eq(@plusDeviceType)
				expect(@rsubscriberData.get_e911serviceplan_productcodeid).to eq('318')
				expect(@rsubscriberData.get_autorenew).to eq('N')
				expect(@rsubscriberData.get_service_type).to eq('B')
				expect(@rsubscriberData.get_pastdue).to eq('N')
				expect(@rsubscriberData.get_insurance_productcodeid).to eq(114)
				expect(@rsubscriberData.get_lost).to eq('N')
				expect(@rsubscriberData.get_position).to eq(1)
				expect(@rsubscriberData.get_devicename).to eq(@fname)
				expect(@rsubscriberData.get_endpointid).to_not be_zero
				expect(@rsubscriberData.get_subscriptionid).not_to eq(0)
				expect(@rsubscriberData.get_serviceplan_productcodeid).to eq('115')
				expect(@rsubscriberData.get_renewal_lag).to eq('0')
				expect(@rsubscriberData.get_suspended).to eq('N')
				expect(@rsubscriberData.get_subscriptionactive).to eq('Y')
				expect(@rsubscriberData.get_treatments).to eq('0')
				expect(@rsubscriberData.get_vmtimer).to eq(25)
				expect(@rsubscriberData.get_e911service_locationid).to eq(0)
				expect(@rsubscriberData.get_e911service_billdate).to eq('01/01/2000')

  end
 
   it "Tests cpn data after basic registration" do
	       
				rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
				@rsubscriberData.set_rsubscriber_data(rsubscriberPK)	
				@cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
				@endpointData.set_endpoint_data(@rsubscriberData.get_endpointid)
				@emailaddressData.set_email_address_data(@accountData.get_emailaddressid)		
				
				expect(@cpnData.get_cpn_type).to eq(@cpnTypeS)
				expect(@cpnData.get_accountid).to eq(@accountData.get_accountid)
				expect(@cpnData.get_calling_party_no).to_not eq(0)
				expect(@cpnData.get_class_of_service).to eq('V')
				expect(@cpnData.get_type_of_service).to eq('3')
				expect(@cpnData.get_dial_tone_company_id).to eq('YMX00')
				expect(@cpnData.get_e911_status).to eq('PQ')
				expect(@cpnData.get_e911_tcsi_code).to eq('PICQ')
				expect(@cpnData.get_e911_tcsi_date).to eq(@currentDate)
				expect(@cpnData.get_inservice).to eq(@currentDate)
				expect(@cpnData.get_outservice).to eq(@standardExpDate)
				expect(@cpnData.get_cpn_type).to eq(@cpnTypeS)
				expect(@cpnData.get_emailaddressid).to eq(@emailaddressData.get_emailaddressid)
				expect(@cpnData.get_state).to eq('TN')
				expect(@cpnData.get_country).to eq('US')
				expect(@cpnData.get_usagebits).to eq(1)
				expect(@cpnData.get_options).to eq(0)
				expect(@cpnData.get_autorenewdate).to eq(@standardCPNAutoRenew)
				expect(@cpnData.get_autorenew).to eq('N')												

  end
   
   it "Tests endpoint data after basic registration" do
	       
				rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
				@rsubscriberData.set_rsubscriber_data(rsubscriberPK)	
				@cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
				@endpointData.set_endpoint_data(@rsubscriberData.get_endpointid)
				@emailaddressData.set_email_address_data(@accountData.get_emailaddressid)		
				
				expect(@endpointData.get_endpointid).to eq(@rsubscriberData.get_endpointid)
				expect(@endpointData.get_accountid).to eq(@accountData.get_accountid)
				expect(@endpointData.get_subscriberid).to eq(@rsubscriberData.get_rsubscriberid)
				expect(@endpointData.get_cpnid).to eq(@cpnData.get_cpnid)
				expect(@endpointData.get_enumber).to eq('E'+@cpnData.get_calling_party_no+'01')
				expect(@endpointData.get_rgid).to eq('900')
				expect(@endpointData.get_subtype).to eq('R')
				expect(@endpointData.get_locationid).to eq(0)
				expect(@endpointData.get_serial_no).to eq(@rsubscriberData.get_serial_no)
				expect(@endpointData.get_position).to eq(1)
				expect(@endpointData.get_subscriptionid).to eq(@rsubscriberData.get_subscriptionid)
				expect(@endpointData.get_device_type).to eq(@plusDeviceType)
				expect(@endpointData.get_serviceplan_productcodeid).to eq(115)
				expect(@endpointData.get_treatments).to eq('0')
				expect(@endpointData.get_cpn_type).to eq(@cpnTypeS)
				expect(@endpointData.get_intrado_arg).to eq('XXX')
				expect(@endpointData.get_e911_color).to eq('N')
				expect(@endpointData.get_e911_description).to eq('E911 Not Setup')
				expect(@endpointData.get_e911_verified).to eq('01/01/2000')
				expect(@endpointData.get_e911_status).to eq('-2')

  end
   
  it "Tests location data after basic registration" do
	       
				rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
				@rsubscriberData.set_rsubscriber_data(rsubscriberPK)	
				@cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
				@endpointData.set_endpoint_data(@rsubscriberData.get_endpointid)
				@emailaddressData.set_email_address_data(@accountData.get_emailaddressid)		
				
				expect(@locationData.get_location_type).to eq('S')
				expect(@locationData.get_accountid).to eq(@accountData.get_accountid)
				expect(@locationData.get_first_name).to eq(@fname)
				expect(@locationData.get_last_name).to eq(@lname)
				expect(@locationData.get_dba).to eq(@fname + ' ' + @lname)
				expect(@locationData.get_house_no).to eq('700')
				expect(@locationData.get_street_name).to eq('2ND ')
				expect(@locationData.get_street_name_suffix).to eq('AVE')
				expect(@locationData.get_msag_community_name).to eq('NASHVILLE')
				expect(@locationData.get_state).to eq('TN')
				expect(@locationData.get_zip_code).to eq('37210')
				expect(@locationData.get_active).to eq('Y')
				expect(@locationData.get_country).to eq('US')
				expect(@locationData.get_phone).to eq('0000000000')
				expect(@locationData.get_geocode).to eq('4703752000')
				expect(@locationData.get_incity).to eq('U')
				expect(@locationData.get_inlocal).to eq('O')

  end
  
   it "Tests email address data after basic registration" do
	       
				rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
				@rsubscriberData.set_rsubscriber_data(rsubscriberPK)	
				@cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
				@endpointData.set_endpoint_data(@rsubscriberData.get_endpointid)
				@emailaddressData.set_email_address_data(@accountData.get_emailaddressid)		
				
				expect(@emailaddressData.get_email_type).to eq('M')
				expect(@emailaddressData.get_accountid).to eq(@accountData.get_accountid)
				expect(@emailaddressData.get_emailaddr).to eq(@email)
				expect(@emailaddressData.get_emailkey).to eq(@email)
				expect(@emailaddressData.get_password).to eq('VWVWVW12')
				expect(@emailaddressData.get_marketing).to eq('9')

  end
					
#	   it "Tests credit card data after basic registration" do
#	       
#				creditcardidPK = @creditcardData.get_creditcardid_pk(@accountData.get_accountid)
#				@creditcardData.setCreditCardData(creditcardidPK)    
#	
#				expect(@creditcardData.get_accountid).to eq(@accountData.get_accountid)
#				expect(@creditcardData.get_full_name).to eq(@fname + '  ' +@lname)
#				expect(@creditcardData.get_first_name).to eq(@fname)
#				expect(@creditcardData.get_last_name).to eq(@lname)
#				expect(@creditcardData.get_card_type).to eq('V')
#				expect(@creditcardData.get_cardnumber).to_not eq('0')
#				expect(@creditcardData.get_exp_date).to eq('122019')
#				expect(@creditcardData.get_active).to eq('Y')
#				expect(@creditcardData.get_fraud_override).to eq('N')
#				expect(@creditcardData.get_cvv2_match).to eq('Y')
#				expect(@creditcardData.get_processor_avs).to eq('F')
#				expect(@creditcardData.get_addr_match).to eq('-')
#				expect(@creditcardData.get_zip_match).to eq('-')
#				expect(@creditcardData.get_debit).to eq('N')
#				expect(@creditcardData.get_locationid).to eq(@accountData.get_locationid)
#				expect(@creditcardData.get_first_charge_date).to eq(@currentDate)
#				expect(@creditcardData.get_charge_count).to eq('1')
#				expect(@creditcardData.get_use_for_recurring).to eq('D')
#				expect(@creditcardData.get_istoken).to eq('Y')
#				expect(@creditcardData.get_mop).to eq('VI')
#				
#  end

   it "Tests orders data after basic registration" do
		 
					@emailaddressData.set_email_address_data(@accountData.get_emailaddressid)				
					@orderPK = @ordersData.get_order_pk(@accountData.get_accountid)
					@ordersData.set_orders_data(@orderPK)	  
				
					expect(@ordersData.get_orderid).to_not be_zero
					expect(@ordersData.get_inbound_orderid).to eq('0')
					expect(@ordersData.get_accountid).to eq(@accountData.get_accountid)
					expect(@ordersData.get_emailaddressid).to eq(@emailaddressData.get_emailaddressid)
#					expect(@ordersData.get_creditcardid).to eq(@creditcardData.get_creditcardid)
					expect(@ordersData.get_order_number).to_not eq('0' )
					expect(@ordersData.get_order_date).to eq(@currentDate)
					expect(@ordersData.get_order_time).to_not eq('')
					expect(@ordersData.get_order_status).to eq('N')
					expect(@ordersData.get_statusdate).to eq(@currentDate)
					expect(@ordersData.get_subtotal).to eq('0')
					expect(@ordersData.get_shipping).to eq('0')
					expect(@ordersData.get_taxes).to eq('0')
					expect(@ordersData.get_total).to eq('0')
					expect(@ordersData.get_taxrateid).to eq('1012')
					expect(@ordersData.get_taxrate).to eq('.078')
					expect(@ordersData.get_adj_subtotal).to eq('0')
					expect(@ordersData.get_adj_taxes).to eq('0')
					expect(@ordersData.get_adj_shipping).to eq('0')
					expect(@ordersData.get_adj_total).to eq('0')
					expect(@ordersData.get_adj_sum_total).to eq('0')
					expect(@ordersData.get_unpaid).to eq('N')
					expect(@ordersData.get_payment).to eq('N')
					expect(@ordersData.get_result_code).to eq('0')
					expect(@ordersData.get_last_auth_attempt).to eq('01/01/2000')
					expect(@ordersData.get_auth_attempts).to eq('0')
					expect(@ordersData.get_auth_total).to eq(0.0)
					expect(@ordersData.get_authmessage).to eq(NIL)
					expect(@ordersData.get_sourceind).to eq('W')
					expect(@ordersData.get_nflags).to eq(0)
					expect(@ordersData.get_campaignid).to eq('1')
					expect(@ordersData.get_cancelreason).to eq('0')

  end
   
   it "Tests order detail data after basic registration" do
	      
	      orderDetailRecords = Array.new
	      
			  @orderDetaildata = OrderDetailDB.new
			  @orderPK = @ordersData.get_order_pk(@accountData.get_accountid)
	      orderDetailRecords=@orderDetaildata.set_order_detail_records(@orderPK)   
	
	      #puts orderDetailRecords.to_s
	     
				expect(orderDetailRecords.to_s).to include("FREENUMBER")
				expect(orderDetailRecords.to_s).to include("NO911SERVICEELECTION")
				expect(orderDetailRecords.to_s).to include("NOMJPLUSREPLACEMENT")
				expect(orderDetailRecords.to_s).to include("MJPLUSSTDDIALPLAN")
				expect(orderDetailRecords.to_s).to include('46')
				expect(orderDetailRecords.to_s).to include('318')
				expect(orderDetailRecords.to_s).to include('114')
				expect(orderDetailRecords.to_s).to include('115')
  end
                
    

end
