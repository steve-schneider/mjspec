require "json"
require "date"
require "rspec"
require_relative "spec_helper.rb"
require_relative "../pages/reg.rb"
require_relative "../db/AccountDB.rb"
require_relative "../db/RsubscriberDB.rb"
require_relative "../db/CpnDB.rb"
require_relative "../db/EndPointDB.rb"
require_relative "../db/LocationDB.rb"
require_relative "../db/EmailAddressDB.rb"
require_relative "../db/CreditCardDB.rb"
require_relative "../db/IppDB.rb"
require_relative "../db/RegDB.rb"
require_relative "../db/OrdersDB.rb"
require_relative "../db/OrderDetailDB.rb"
require_relative "../db/OrderStatusDB.rb"
require_relative "../db/InvoiceDB.rb"
require_relative "../db/ActionDB.rb"
require_relative "../db/NotesDB.rb"
include RSpec::Expectations

describe "Device Registration" do

  before(:each) do
    d = DateTime.now
    @fname = "AUT-"+d.strftime('%F')+"-"+d.strftime('%H-%M-%S')  
    @lname = "SCHNEIDER"  


   # Registration screen Device Type selection options.
    # "Generate magicJackEXPRESS gets 3 months"
    @genExpress = "Generate magicJackEXPRESS"
	@expExpDate = d.next_month(3).strftime('%m/%d/%Y')
	@expDevicetype = "W"

    # "Generate magicJackGO gets 12 months"    
	@genGo = "Generate magicJackGO"
	@goExpDate = d.next_month(12).strftime('%m/%d/%Y')
	@goDeviceType = "B"

    # "Generate magicJackGO Mexico gets 12 months"
    @genGoMx = "Generate magicJackGO Mexico"
	@gomxExpDate = d.next_month(12).strftime('%m/%d/%Y')
	@gomxDeviceType = "E"

    # "Generate 2014 magicJack PLUS gets 6 months"
    @genfourteen = "Generate 2014 magicJack PLUS"
	@fourteenExpDate = d.next_month(6).strftime('%m/%d/%Y')
	@fourteenDeviceType = "F"

    # "Generate magicJack PLUS"
	@genPlus = "Generate magicJack PLUS"
	@plusExpDate = d.next_month(12).strftime('%m/%d/%Y') 
	@plusDeviceType = "P"   

    # "Generate magicJack should generate a message no longer able to register"
    @genJack = "Generate magicJack"
	@jackExpDate = d.next_month(12).strftime('%m/%d/%Y')     
	@jackDeviceType = "M"

    # Renewal upsell date increases
    @addOneYrRenewal = d.next_month(12).strftime('%m/%d/%Y')
    @addFiveYrRenewal = d.next_month(60).strftime('%m/%d/%Y')    

    # CPN type
    @cpnTypeI = "I" # International Speed Dial Number
    @cpnTypeM = "M" # MagicNumber
    @cpnTypeP = "P" # Port-in Number
    @poerExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeS = "S" # Standard Telephone Number
    @standardExpDate = "12/31/2100"    
    @cpnTypeV = "V" # Vanity Number
    @vanityExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeK = "K" # Kustom Numer
    @kustomExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeA = "A" # Alternate contact Number
    @cpnTypeC = "C" # Canadian
    @canadaExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeG = "G" # Group Call
    @cpnTypeF = "F" # Park Call Forwarding

    @currentDate = d.strftime('%m/%d/%Y') 

    @expDate = d.next_year(1).strftime('%m/%d/%Y')
    @expDate1yr = d.next_year(2).strftime('%m/%d/%Y')
    @VanityExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')
    @expDate5yr = d.next_year(6).strftime('%m/%d/%Y')

    @registerJack = Reg.new(@driver)
    @rsubscriberData = RsubscriberDB.new
    @emailaddressData = EmailAddressDB.new
    @accountData = AccountDB.new
    @locationData = LocationDB.new
    @endpointData = EndPointDB.new
    @creditcardData = CreditCardDB.new
    @IppData = IppDB.new
    @cpnData = CpnDB.new
    @regData = RegDB.new
    @ordersData = OrdersDB.new
    @orderdetailData = OrderDetailDB.new
    @orderstatusData = OrderStatusDB.new
    @invoiceData = InvoiceDB.new
    @actionData = ActionDB.new
    @notesData = NotesDB.new
  end
  
#NEW: def register_with(jack_type, fname, actCodeNotification, numType, serviceType, addr911, inCity, renewType, ippType, newbillLocation) 

  it "Registers a basic Express with a US number, IN/Out going, SKIP 911, SKIP renewal, SKIP IPP, SKIP auto renew" do    
	@registerJack.register_with(@genExpress,@fname,'EMAIL','US','BOTH','SKIP','inCityNA','SKIP','SKIP','NO')
	 
	begin
		@accountData.set_account_data(@fname)
	    rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
	    @rsubscriberData.set_rsubscriber_data(rsubscriberPK)
	    @cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
	    @endpointData.set_endpoint_data(@rsubscriberData.get_endpointid)
	    @locationData.set_location_data(@accountData.get_locationid)
	    @emailaddressData.set_email_address_data(@accountData.get_emailaddressid)	      
	   	@orderPK = @ordersData.get_order_pk(@accountData.get_accountid)
	   	@ordersData.set_orders_data(@orderPK) 

	   	@orderDetaildata = OrderDetailDB.new
	   	orderDets=@orderDetaildata.set_order_detail_records(@orderPK)  
	    orderDetailRecords=orderDets.to_s

	   	@orderStatusdata = OrderStatusDB.new
	   	orderStats=@orderStatusdata.set_order_status_records(@orderPK)
	   	orderStatusRecords=orderStats.to_s

	   	@actiondata = ActionDB.new
	   	action = @actiondata.set_action_records(@accountData.get_accountid)
	   	actionRecords=action.to_s

	   	@notesdata = NotesDB.new
	   	notes = @notesdata.set_notes_records(@accountData.get_accountid)
	   	notesRecords=notes.to_s

#	   	@IppData.setIppData(@accountData.get_accountid)
	         	  
	 	aggregate_failures "Registers a Express" do
	      	expect(@rsubscriberData.get_accountid).to_not be_zero
	      	expect(@accountData.get_active).to eq ('Y')
			expect(@accountData.get_customer_name).to eq (@fname + " " + @lname)			
			expect(@accountData.get_account_type).to eq ('M') # MagicJack
			expect(@accountData.get_busres).to eq ('R') # Residential
			expect(@accountData.get_loyal).to eq ('N') # Not Loyal
			expect(@accountData.get_locationid).to eq @locationData.get_locationid
			expect(@accountData.get_emailaddressid).to eq @emailaddressData.get_emailaddressid			
            expect(@rsubscriberData.get_rsubscriberid).to eq rsubscriberPK
            expect(@rsubscriberData.get_active).to eq ('Y')
            expect(@rsubscriberData.get_accountid).to eq @accountData.get_accountid
            expect(@rsubscriberData.get_cpnid).to eq @cpnData.get_cpnid
            expect(@rsubscriberData.get_startservice).to eq @currentDate
			expect(@rsubscriberData.get_endservice).to eq @expExpDate
			expect(@rsubscriberData.get_device_type).to eq @expDevicetype # W
			expect(@rsubscriberData.get_e911serviceplan_productcodeid).to eq ('318') # NO 911 Service Plan			
			expect(@rsubscriberData.get_autorenew).to eq ('N')  # No auto renewal
			expect(@rsubscriberData.get_service_type).to eq ('B') # Both incoming and outgoing
			expect(@rsubscriberData.get_pastdue).to eq ('N')
			expect(@rsubscriberData.get_insurance_productcodeid) == ('360') # NO magicJackExpress Replacement Plan
			expect(@rsubscriberData.get_lost).to eq ('N')
			expect(@rsubscriberData.get_position) == ('1') 
			expect(@rsubscriberData.get_devicename).to eq @fname
			expect(@rsubscriberData.get_endpointid).to_not be_zero
			expect(@rsubscriberData.get_subscriptionid) != ('0')
			expect(@rsubscriberData.get_serviceplan_productcodeid) == ('358') # magicJackExpress Standard Dialing Plan
			expect(@rsubscriberData.get_renewal_lag) == ('0')
			expect(@rsubscriberData.get_suspended).to eq ('N')
			expect(@rsubscriberData.get_subscriptionactive).to eq ('Y')
			expect(@rsubscriberData.get_treatments) == ('0')
			expect(@rsubscriberData.get_vmtimer) == ('25')
			expect(@rsubscriberData.get_e911service_locationid) == ('0')
			expect(@rsubscriberData.get_e911service_billdate).to eq ('01/01/2000')
			expect(@cpnData.get_cpn_type).to eq @cpnTypeS # Standard
			expect(@cpnData.get_accountid).to eq @accountData.get_accountid
			expect(@cpnData.get_calling_party_no) != ('0')
			expect(@cpnData.get_class_of_service).to eq ('V') # our magicjack Number
			expect(@cpnData.get_type_of_service) == ('3')
			expect(@cpnData.get_dial_tone_company_id).to eq ('YMX00')
			expect(@cpnData.get_e911_status).to eq ('PQ')
			expect(@cpnData.get_e911_tcsi_code).to eq ('PICQ')
			expect(@cpnData.get_e911_tcsi_date).to eq @currentDate
			expect(@cpnData.get_inservice).to eq @currentDate
			expect(@cpnData.get_outservice).to eq @standardExpDate
			expect(@cpnData.get_number_productcodeid) == ('46') # US Phone Number - Free
			expect(@cpnData.get_emailaddressid).to eq @emailaddressData.get_emailaddressid
			expect(@cpnData.get_state).to eq ('TN')
			expect(@cpnData.get_country).to eq ('US')
			expect(@cpnData.get_usagebits) == ('1') # Number of devices with number?
			expect(@cpnData.get_options) == ('0')
			expect(@cpnData.get_autorenewdate).to eq @expExpDate
			expect(@cpnData.get_autorenew).to eq ('N')
			expect(@endpointData.get_endpointid).to eq @rsubscriberData.get_endpointid
			expect(@endpointData.get_accountid).to eq @accountData.get_accountid
			expect(@endpointData.get_subscriberid).to eq @rsubscriberData.get_rsubscriberid 
			expect(@endpointData.get_cpnid).to eq @cpnData.get_cpnid
			expect(@endpointData.get_enumber).to eq ('E'+@cpnData.get_calling_party_no+'01')
			expect(@endpointData.get_rgid).to eq ('900')
			expect(@endpointData.get_subtype).to eq ('R') # Registered
			expect(@endpointData.get_locationid) == ('0')
			expect(@endpointData.get_serial_no).to eq @rsubscriberData.get_serial_no
			expect(@endpointData.get_position) == ('1')
			expect(@endpointData.get_subscriptionid).to eq @rsubscriberData.get_subscriptionid
			expect(@endpointData.get_device_type).to eq @expDevicetype # W
			expect(@endpointData.get_serviceplan_productcodeid) == ('358') # magicJackExpress Standard Dialing Plan
			expect(@endpointData.get_treatments) == ('0')
			expect(@endpointData.get_cpn_type).to eq @cpnTypeS # S
			expect(@endpointData.get_intrado_arg).to eq ('XXX')
			expect(@endpointData.get_e911_color).to eq ('N')
			expect(@endpointData.get_e911_description).to eq ('E911 Not Setup')
			expect(@endpointData.get_e911_verified) == @currentDate
			expect(@endpointData.get_e911_status) == ('2')
			expect(@locationData.get_location_type).to eq ('S') # Shipping
			expect(@locationData.get_accountid).to eq @accountData.get_accountid
			expect(@locationData.get_first_name).to eq @fname
			expect(@locationData.get_last_name).to eq @lname
			expect(@locationData.get_dba).to eq (@fname + ' ' + @lname)
			expect(@locationData.get_house_no).to eq ('700') # currently hard coded in reg.rb
			expect(@locationData.get_street_name).to eq ('2ND ') # currently hard coded in reg.rb
			expect(@locationData.get_street_name_suffix).to eq ('AVE') # currently hard coded in reg.rb
			expect(@locationData.get_msag_community_name).to eq ('NASHVILLE') # currently hard coded in reg.rb
			expect(@locationData.get_state).to eq ('TN') # currently hard coded in reg.rb
			expect(@locationData.get_zip_code).to eq ('37210') # currently hard coded in reg.rb
			expect(@locationData.get_active).to eq ('Y')
			expect(@locationData.get_country).to eq ('US') # currently hard coded in reg.rb
			expect(@locationData.get_phone) == ('0000000000')
			expect(@locationData.get_geocode).to eq ('4703752000') # prepopuplated in db?  more test needed on geocode
			expect(@locationData.get_incity).to eq ('U') # Incity parameter U = UNSET
			expect(@locationData.get_inlocal).to eq ('O') # default value
			expect(@emailaddressData.get_email_type).to eq ('M') # default value
			expect(@emailaddressData.get_accountid).to eq @accountData.get_accountid
			expect(@emailaddressData.get_emailaddr) == EMAIL # defined within reg.rb
			expect(@emailaddressData.get_emailkey) == EMAIL # defined within reg.rb
			expect(@emailaddressData.get_password).to eq ('VWVWVW12') # defined within reg.rb
			expect(@emailaddressData.get_marketing).to eq ('9') # customer will receive marketing emails
			expect(@ordersData.get_orderid).to_not be_zero
			expect(@ordersData.get_inbound_orderid).to eq ('0')
			expect(@ordersData.get_accountid).to eq @accountData.get_accountid
			expect(@ordersData.get_emailaddressid).to eq @emailaddressData.get_emailaddressid
			expect(@ordersData.get_order_number) != ('0') 
			expect(@ordersData.get_order_date).to eq @currentDate
			expect(@ordersData.get_order_time) != ('')
			expect(@ordersData.get_order_status).to eq ('N')
			expect(@ordersData.get_statusdate).to eq @currentDate
			expect(@ordersData.get_subtotal).to eq ('0')
			expect(@ordersData.get_shipping).to eq ('0')
			expect(@ordersData.get_taxes) == ('0.78')
			expect(@ordersData.get_total) == ('10.78')
			expect(@ordersData.get_taxrateid).to eq ('1012')
			expect(@ordersData.get_taxrate) == ('0.078')
			expect(@ordersData.get_adj_subtotal).to eq ('0')
			expect(@ordersData.get_adj_taxes).to eq ('0')
			expect(@ordersData.get_adj_shipping).to eq ('0')
			expect(@ordersData.get_adj_total).to eq ('0')
			expect(@ordersData.get_adj_sum_total) == ('10.78')
			expect(@ordersData.get_unpaid).to eq ('N')
			expect(@ordersData.get_payment).to eq ('N')
			expect(@ordersData.get_result_code) == ('0')
			expect(@ordersData.get_last_auth_attempt) == @currentDate
			expect(@ordersData.get_auth_attempts) == ('1')
			expect(@ordersData.get_auth_total) == ('10.78')
			expect(@ordersData.get_authmessage) == ('nil')
			expect(@ordersData.get_sourceind).to eq ('W')
			expect(@ordersData.get_nflags) == ('0')
			expect(@ordersData.get_campaignid)  == ('1')
			expect(@ordersData.get_cancelreason) == ('0')

			expect(orderDetailRecords.to_s).to include("FREENUMBER")
			expect(orderDetailRecords.to_s).to include("MJEXPRESSSTDDIALPLAN")
			expect(orderDetailRecords.to_s).to include("NO911SERVICEELECTION")
			expect(orderDetailRecords.to_s).to include("NOEXPRESSREPLACEMENT")
			expect(orderDetailRecords.to_s).to include("46")
			expect(orderDetailRecords.to_s).to include("358")
			expect(orderDetailRecords.to_s).to include("318")
			expect(orderDetailRecords.to_s).to include("360")
			expect(orderDetailRecords.to_s).to include("1")
			expect(orderDetailRecords.to_s).to include("I")
			expect(orderDetailRecords.to_s).to include("N")
			expect(orderDetailRecords.to_s).to include @cpnData.get_cpnid.to_s

			expect(orderStatusRecords.to_s).to include("N")
			expect(orderStatusRecords.to_s).to include @ordersData.get_order_number.to_s
		 	
		 	expect(actionRecords.to_s).to include("E911DISCLOSURE")
		 	expect(actionRecords.to_s).to include @rsubscriberData.get_serial_no.to_s
			expect(actionRecords.to_s).to include("100000")
			
			expect(notesRecords.to_s).to include("7")
			expect(notesRecords.to_s).to include("Y")


    	end
     	rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        	puts e.message
       		puts @fname
       		exit(1)
     	end	    
  	end

=begin
  it "Registers an Express with a US number, IN/Out going, incity 911, no renewal, no IPP, no auto renew" do    
	@registerJack.register_with(@genExpress,@fname,'EMAIL','US','BOTH','911addressCorrect','inCityYes','SKIP','SKIP','NO')
	 
	  begin
	    @accountData.setAccountData(@fname)
	    rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
	    @rsubscriberData.setRsubscriberData(rsubscriberPK)
	    @cpnData.setCpnData(@rsubscriberData.get_cpnid)
	    @endpointData.setEndPointData(@rsubscriberData.get_endpointid)
	    @locationData.setLocationData(@accountData.get_locationid)
	    @emailaddressData.setEmailAddressData(@accountData.get_emailaddressid)	      
	    #creditcardidPK = @creditcardData.get_creditcardid_pk(@accountData.get_accountid)
	    #@creditcardData.setCreditCardData(creditcardidPK)
	   	orderPK = @ordersData.get_order_pk(@accountData.get_accountid)
	   	@ordersData.setOrdersData(orderPK) 
	   	@orderDetailRecords.set_orderDetailRecords(@ordersData.get_orderid)
	   	@orderStatusRecords.set_orderStatusRecords(@ordersData.get_orderid)
	   	@invoiceRecords.set_invoiceRecords(@ordersData.get_orderid)
	   	@actionRecords.set_actionRecords(@accountData.get_accountid)
	   	@notesRecords.set_notesRecords(@accountData.get_accountid)
	   	@IppData.setIppData(@accountData.get_accountid)
	         	  
	 	aggregate_failures "Registers a Express" do
	      	expect(@rsubscriberData.get_accountid).to_not be_zero
	      	expect(@accountData.get_active).to eq 'Y'
			expect(@accountData.get_customer_name).to eq @fname + " " + @lname			
			expect(@accountData.get_account_type).to eq 'M' # MagicJack
			expect(@accountData.get_busres).to eq 'R' # Residential
			expect(@accountData.get_loyal).to eq 'N' # Not Loyal
			expect(@accountData.get_locationid).to eq @locationData.get_locationid
			expect(@accountData.get_emailaddressid).to eq @emailaddressData.get_emailaddressid			
            expect(@rsubscriberData.get_rsubscriberid).to eq rsubscriberPK
            expect(@rsubscriberData.get_active).to eq 'Y'
            expect(@rsubscriberData.get_accountid).to eq @accountData.get_accountid
            expect(@rsubscriberData.get_cpnid).to eq @cpnData.get_cpnid
            expect(@rsubscriberData.get_startservice).to eq @currentDate
			expect(@rsubscriberData.get_endservice).to eq @expExpDate
			expect(@rsubscriberData.get_device_type).to eq @expDevicetype # W
			expect(@rsubscriberData.get_e911serviceplan_productcodeid).to eq '288' # MJ 911 Service Plan
			expect(@rsubscriberData.get_autorenew).to eq 'N'  # No auto renewal
			expect(@rsubscriberData.get_service_type).to eq 'B' # Both incoming and outgoing
			expect(@rsubscriberData.get_pastdue).to eq 'N'
			expect(@rsubscriberData.get_insurance_productcodeid) == '360' # NO magicJackExpress Replacement Plan
			expect(@rsubscriberData.get_lost).to eq 'N'
			expect(@rsubscriberData.get_position) == '1' 
			expect(@rsubscriberData.get_devicename).to eq @fname
			expect(@rsubscriberData.get_endpointid).to_not be_zero
			expect(@rsubscriberData.get_subscriptionid) != '0'
			expect(@rsubscriberData.get_serviceplan_productcodeid) == '358' # magicJackExpress Standard Dialing Plan
			expect(@rsubscriberData.get_renewal_lag) == '0'
			expect(@rsubscriberData.get_suspended).to eq 'N'
			expect(@rsubscriberData.get_subscriptionactive).to eq 'Y'
			expect(@rsubscriberData.get_treatments) == '0'
			expect(@rsubscriberData.get_vmtimer) == '25'
			expect(@rsubscriberData.get_e911service_locationid).to eq @locationData.get_locationid
			expect(@rsubscriberData.get_e911service_billdate).to eq @expExpDate
			expect(@cpnData.get_cpn_type).to eq @cpnTypeS # Standard
			expect(@cpnData.get_accountid).to eq @accountData.get_accountid
			expect(@cpnData.get_calling_party_no) != '0'
			expect(@cpnData.get_class_of_service).to eq 'V' # our magicjack Number
			expect(@cpnData.get_type_of_service) == '3'
			expect(@cpnData.get_dial_tone_company_id).to eq 'YMX00'
			expect(@cpnData.get_e911_status).to eq 'PQ'
			expect(@cpnData.get_e911_tcsi_code).to eq 'PICQ'
			expect(@cpnData.get_e911_tcsi_date).to eq @currentDate
			expect(@cpnData.get_inservice).to eq @currentDate
			expect(@cpnData.get_outservice).to eq @standardExpDate
			expect(@cpnData.get_number_productcodeid) == '46' # US Phone Number - Free
			expect(@cpnData.get_emailaddressid).to eq @emailaddressData.get_emailaddressid
			expect(@cpnData.get_state).to eq 'TN'
			expect(@cpnData.get_country).to eq 'US'
			expect(@cpnData.get_usagebits) == '1' # Number of devices with number?
			expect(@cpnData.get_options) == '0'
			expect(@cpnData.get_autorenewdate).to eq @expExpDate
			expect(@cpnData.get_autorenew).to eq 'N'
			expect(@endpointData.get_endpointid).to eq @rsubscriberData.get_endpointid
			expect(@endpointData.get_accountid).to eq @accountData.get_accountid
			expect(@endpointData.get_subscriberid).to eq @rsubscriberData.get_rsubscriberid 
			expect(@endpointData.get_cpnid).to eq @cpnData.get_cpnid
			expect(@endpointData.get_enumber).to eq 'E'+@cpnData.get_calling_party_no+'01'
			expect(@endpointData.get_rgid).to eq '900'
			expect(@endpointData.get_subtype).to eq 'R' # Registered
			expect(@endpointData.get_locationid).to eq @locationData.get_locationid
			expect(@endpointData.get_serial_no).to eq @rsubscriberData.get_serial_no
			expect(@endpointData.get_position) == '1'
			expect(@endpointData.get_subscriptionid).to eq @rsubscriberData.get_subscriptionid
			expect(@endpointData.get_device_type).to eq @expDevicetype # W
			expect(@endpointData.get_serviceplan_productcodeid) == '358' # magicJackExpress Standard Dialing Plan
			expect(@endpointData.get_treatments) == '0'
			expect(@endpointData.get_cpn_type).to eq @cpnTypeS # S
			expect(@endpointData.get_intrado_arg).to eq 'TNI'
			expect(@endpointData.get_e911_color).to eq 'Y'
			expect(@endpointData.get_e911_description).to eq 'E911 Address Processing'
			expect(@endpointData.get_e911_verified) == @currentDate
			expect(@endpointData.get_e911_status) == '2'
			expect(@locationData.get_location_type).to eq 'S' # Shipping
			expect(@locationData.get_accountid).to eq @accountData.get_accountid
			expect(@locationData.get_first_name).to eq @fname
			expect(@locationData.get_last_name).to eq @lname
			expect(@locationData.get_dba).to eq @fname + ' ' + @lname
			expect(@locationData.get_house_no).to eq '700' # currently hard coded in reg.rb
			expect(@locationData.get_street_name).to eq '2ND ' # currently hard coded in reg.rb
			expect(@locationData.get_street_name_suffix).to eq 'AVE' # currently hard coded in reg.rb
			expect(@locationData.get_msag_community_name).to eq 'NASHVILLE' # currently hard coded in reg.rb
			expect(@locationData.get_state).to eq 'TN' # currently hard coded in reg.rb
			expect(@locationData.get_zip_code).to eq '37210' # currently hard coded in reg.rb
			expect(@locationData.get_active).to eq 'Y'
			expect(@locationData.get_country).to eq 'US' # currently hard coded in reg.rb
			expect(@locationData.get_phone).to eq @cpnData.get_calling_party_no
			expect(@locationData.get_geocode).to eq '4703752000' # prepopuplated in db?  more test needed on geocode
			expect(@locationData.get_incity).to eq 'I' # Incity parameter passed into this test
			expect(@locationData.get_inlocal).to eq 'O' # default value
			expect(@emailaddressData.get_email_type).to eq 'M' # default value
			expect(@emailaddressData.get_accountid).to eq @accountData.get_accountid
			expect(@emailaddressData.get_emailaddr) == EMAIL # defined within reg.rb
			expect(@emailaddressData.get_emailkey) == EMAIL # defined within reg.rb
			expect(@emailaddressData.get_password).to eq 'VWVWVW12' # defined within reg.rb
			expect(@emailaddressData.get_marketing).to eq '9' # customer will receive marketing emails
#			expect(@creditcardData.get_accountid).to eq @accountData.get_accountid
#			expect(@creditcardData.get_full_name).to eq @fname + '  ' +@lname
#			expect(@creditcardData.get_first_name).to eq @fname
#			expect(@creditcardData.get_last_name).to eq @lname
#			expect(@creditcardData.get_card_type).to eq 'V' # from reg.rb
#			expect(@creditcardData.get_cardnumber) != '0'
#			expect(@creditcardData.get_exp_date).to eq '122019' # set in reg.rb
#			expect(@creditcardData.get_active).to eq 'Y'
#			expect(@creditcardData.get_fraud_override).to eq 'N'
#			expect(@creditcardData.get_cvv2_match).to eq 'Y'
#			expect(@creditcardData.get_processor_avs).to eq 'F'
#			expect(@creditcardData.get_addr_match).to eq '-'
#			expect(@creditcardData.get_zip_match).to eq '-'
#			expect(@creditcardData.get_debit).to eq 'N'
#			expect(@creditcardData.get_locationid).to eq @accountData.get_locationid
#			expect(@creditcardData.get_first_charge_date) == @currentDate
#			expect(@creditcardData.get_charge_count).to eq '1'
#			expect(@creditcardData.get_use_for_recurring).to eq 'D'
#			expect(@creditcardData.get_istoken).to eq 'Y'
#			expect(@creditcardData.get_mop).to eq 'VI'
			expect(@ordersData.get_orderid).to_not be_zero
			expect(@ordersData.get_inbound_orderid).to eq '0'
			expect(@ordersData.get_accountid).to eq @accountData.get_accountid
			expect(@ordersData.get_emailaddressid).to eq @emailaddressData.get_emailaddressid
#			expect(@ordersData.get_creditcardid).to eq @creditcardData.get_creditcardid
			expect(@ordersData.get_order_number) != '0' 
			expect(@ordersData.get_order_date).to eq @currentDate
			expect(@ordersData.get_order_time) != ''
			expect(@ordersData.get_order_status).to eq 'N'
			expect(@ordersData.get_statusdate).to eq @currentDate
			expect(@ordersData.get_subtotal).to eq '10'
			expect(@ordersData.get_shipping).to eq '0'
			expect(@ordersData.get_taxes) == '0.78'
			expect(@ordersData.get_total) == '10.78'
			expect(@ordersData.get_taxrateid).to eq '1012'
			expect(@ordersData.get_taxrate) == '0.078'
			expect(@ordersData.get_adj_subtotal).to eq '0'
			expect(@ordersData.get_adj_taxes).to eq '0'
			expect(@ordersData.get_adj_shipping).to eq '0'
			expect(@ordersData.get_adj_total).to eq '0'
			expect(@ordersData.get_adj_sum_total) == '10.78'
			expect(@ordersData.get_unpaid).to eq 'N'
			expect(@ordersData.get_payment).to eq 'N'
			expect(@ordersData.get_result_code) == '0'
			expect(@ordersData.get_last_auth_attempt) == @currentDate
			expect(@ordersData.get_auth_attempts) == '1'
			expect(@ordersData.get_auth_total) == '10.78'
			expect(@ordersData.get_authmessage).to eq 'Approved'
			expect(@ordersData.get_sourceind).to eq 'W'
			expect(@ordersData.get_nflags) == '0'
			expect(@ordersData.get_campaignid)  == '1'
			expect(@ordersData.get_cancelreason) == '0'
			expect(@orderDetailRecords.to_s).to include("FREENUMBER")
			expect(@orderDetailRecords.to_s).to include("MJEXPRESSSTDDIALPLAN"),                    "expected order detail to contain MJEXPRESSSTDDIALPLAN"
			expect(@orderDetailRecords.to_s).to include("E911MJSUBSCRIPTION"),                      "expected order detail to contain E911MJSUBSCRIPTION"
			expect(@orderDetailRecords.to_s).to include("NOEXPRESSREPLACEMENT"),                    "expected order detail to contain NOEXPRESSREPLACEMENT"
			expect(@orderDetailRecords.to_s).to include("46"),                                      "expected order detail to contain 46" #product code for FREENUMBER
			expect(@orderDetailRecords.to_s).to include("358"),                                     "expected order detail to contain 358" #product code for MJEXPRESSSTDDIALPLAN
			expect(@orderDetailRecords.to_s).to include("288"),                                     "expected order detail to contain 288" #product code for E911MJSUBSCRIPTION
			expect(@orderDetailRecords.to_s).to include("360"),                                     "expected order detail to contain 360" #product code for NOEXPRESSREPLACEMENT
			expect(@orderDetailRecords.to_s).to include("1"),                                       "expected order detail to contain 1" #quantity, pieces, 
			expect(@orderDetailRecords.to_s).to include("I"),                              		    "expected order detail to contain I" #detail_type 			
			expect(@orderDetailRecords.to_s).to include("N"),                                		"expected order detail to contain N" #paid 			
			expect(@orderDetailRecords.to_s).to include(@cpnData.get_cpnid),						"expected order detail to contain cpnid" # cpnid for FREENUMBER 			
			expect(@orderDetailRecords.to_s).to include(@locationData.get_locationid),				"expected order detail to contain locationid" # locationid for E911MJSUBSCRIPTION 			
			expect(@orderStatusRecords.to_s).to include("N"),                      					"expected order status to contain [N]ewPending" # New Pending status
			expect(@orderStatusRecords.to_s).to include(@ordersData.get_order_number),  			"expected order status description to contain [N]ewPending with order number" # New Pending status with order number
		 	expect(@invoiceRecords.to_s).to include("I"),                      						"expected invoice to contain I for invoice_type" # Invoice_type
		 	expect(@invoiceRecords.to_s).to include(@ordersData.get_order_number),                  "expected invoice to contain order number for description" 
		 	expect(@invoiceRecords.to_s).to include("Y"),                      						"expected invoice to contain Y for active" 
		 	expect(@actionRecords.to_s).to include("911BILL"),										"expect action to contain 911Bill action letter"
		 	expect(@actionRecords.to_s).to include("E911DISCLOSURE"),								"expect action to contain E911DISCLOSURE action letter"
		 	expect(@actionRecords.to_s).to include(@deviceData.get_serial_no),						"expect action to contain serial no value"
			expect(@actionRecords.to_s).to include("100000"),										"expect action to contain successfull error_type"
			expect(@notesRecords.to_s).to include("7"),												"expect notes to contain note type"
			expect(@notesRecords.to_s).to include("Y"),												"expect notes to contain active "


    	end

     	rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        	puts e.message
       		puts @fname
       		exit(1)
     	end	    
	  	      
  	end
=end
 
end
