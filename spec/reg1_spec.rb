require "json"
require "date"
require "rspec"
require_relative "spec_helper.rb"
require_relative "../pages/reg.rb"
require_relative "../db/AccountDB.rb"
require_relative "../db/RsubscriberDB.rb"
require_relative "../db/CpnDB.rb"
require_relative "../db/EndPointDB.rb"
require_relative "../db/LocationDB.rb"
require_relative "../db/EmailAddressDB.rb"
require_relative "../db/CreditCardDB.rb"
require_relative "../db/IppDB.rb"
require_relative "../db/RegDB.rb"
require_relative "../db/OrdersDB.rb"
require_relative "../db/OrderDetailDB.rb"
include RSpec::Expectations

describe "Device Registration" do

  before(:each) do
    d = DateTime.now
    @fname = "AUT-"+d.strftime('%F')+"-"+d.strftime('%H-%M-%S')  
    @lname = "SCHNEIDER"
    @email ="STEVE.SCHNEIDER%"+@fname+"@MAGICJACK.COM"

   # Registration screen Device Type selection options.
    # "Generate magicJackEXPRESS gets 3 months"
  @genExpress = "Generate magicJackEXPRESS"
	@expExpDate = d.next_month(3).strftime('%m/%d/%Y')
	@expDevicetype = "W"

    # "Generate magicJackGO gets 12 months"    
	@genGo = "Generate magicJackGO"
	@goExpDate = d.next_month(12).strftime('%m/%d/%Y')
	@goDeviceType = "B"

    # "Generate magicJackGO Mexico gets 12 months"
  @genGoMx = "Generate magicJackGO Mexico"
	@gomxExpDate = d.next_month(12).strftime('%m/%d/%Y')
	@gomxDeviceType = "E"

    # "Generate 2014 magicJack PLUS gets 6 months"
    @genfourteen = "Generate 2014 magicJack PLUS"
	@fourteenExpDate = d.next_month(6).strftime('%m/%d/%Y')
	@fourteenDeviceType = "F"

    # "Generate magicJack PLUS"
	@genPlus = "Generate magicJack PLUS"
	@plusExpDate = d.next_month(12).strftime('%m/%d/%Y') 
	@plusDeviceType = "P"   

    # "Generate magicJack should generate a message no longer able to register"
    @genJack = "Generate magicJack"
	@jackExpDate = d.next_month(12).strftime('%m/%d/%Y')     
	@jackDeviceType = "M"

    # Renewal upsell date increases
    @addOneYrRenewal = d.next_month(12).strftime('%m/%d/%Y')
    @addFiveYrRenewal = d.next_month(60).strftime('%m/%d/%Y')    

    # CPN type
    @cpnTypeI = "I" # International Speed Dial Number
    @cpnTypeM = "M" # MagicNumber
    @cpnTypeP = "P" # Port-in Number
    @poerExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeS = "S" # Standard Telephone Number
    @standardExpDate = "12/31/2100"    
    @cpnTypeV = "V" # Vanity Number
    @vanityExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeK = "K" # Kustom Numer
    @kustomExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeA = "A" # Alternate contact Number
    @cpnTypeC = "C" # Canadian
    @canadaExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeG = "G" # Group Call
    @cpnTypeF = "F" # Park Call Forwarding

    @currentDate = d.strftime('%m/%d/%Y') 

    @expDate = d.next_year(1).strftime('%m/%d/%Y')
    @expDate1yr = d.next_year(2).strftime('%m/%d/%Y')
    @VanityExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')
    @expDate5yr = d.next_year(6).strftime('%m/%d/%Y')

    @registerJack = Reg.new(@driver)
    @rsubscriberData = RsubscriberDB.new
    @emailaddressData = EmailAddressDB.new
    @accountData = AccountDB.new
    @locationData = LocationDB.new
    @endpointData = EndPointDB.new
    @creditcardData = CreditCardDB.new
    @IppData = IppDB.new
    @cpnData = CpnDB.new
    @regData = RegDB.new
    @ordersData = OrdersDB.new
    @orderDetaildata = OrderDetailDB.new
  end
  

#OLD:   def register_with(jack_type, fname, numType, renewType, ippType, addr911, inCity, actCodeNotification)   

#NEW: def register_with(jack_type, fname, actCodeNotification, numType, serviceType, addr911, inCity, renewType, ippType, newbillLocation) 

  it "Registers an Express with a US number, incity 911, no renewal 10 IPP, no auto renew" do    
	@registerJack.register_with(@genExpress,@fname,'EMAIL','US','BOTH','SKIP','inCityYes','SKIP', 'IPP10','NONEWBILL')
		#@registerJack.register_with(@genExpress,@fname,'EMAIL','US','BOTH','911addressCorrect','inCityYes','SKIP', 'IPP10','NONEWBILL')

	 
	  begin
	    @accountData.setAccountData(@fname)
	    rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
	    @rsubscriberData.setRsubscriberData(rsubscriberPK)
	    @cpnData.setCpnData(@rsubscriberData.get_cpnid)
	    @endpointData.setEndPointData(@rsubscriberData.get_endpointid)
	    @locationData.setLocationData(@accountData.get_locationid)
	    @emailaddressData.setEmailAddressData(@accountData.get_emailaddressid)	      
	    creditcardidPK = @creditcardData.get_creditcardid_pk(@accountData.get_accountid)
	    @creditcardData.setCreditCardData(creditcardidPK)    
     
	   	orderPK = @ordersData.get_order_pk(@accountData.get_accountid)   	
	   	   	
	   	@ordersData.setOrdersData(orderPK)	   	
	   		   	
	   	@orderDetaildata = OrderDetailDB.new
	    @orderDetailRecords=@orderDetaildata.set_orderDetailRecords(orderPK)
	      
	     #puts @orderDetailRecords

	   	@IppData.setIppData(@accountData.get_accountid)
	         	  
	 	aggregate_failures "Registers a Express" do
	    expect(@rsubscriberData.get_accountid).to_not be_zero
	
			expect(@accountData.get_active).to eq('Y'),                                           	"expected account to be active"
			expect(@accountData.get_customer_name).to eq(@fname + " " + @lname),                  	"expected customer name to be #{@fname} #{@lname}" 
			expect(@accountData.get_account_type).to eq('M'),                                     	"expected account type to be M"  # MagicJack
			expect(@accountData.get_busres).to eq('R'), 																						"expected busred flag to be R" # Residential
			expect(@accountData.get_loyal).to eq('N'),                                            	"expected loyal flag to be N" # Not Loyal
			expect(@accountData.get_locationid).to eq @locationData.get_locationid
			expect(@accountData.get_emailaddressid).to eq @emailaddressData.get_emailaddressid

			expect(@rsubscriberData.get_rsubscriberid).to eq rsubscriberPK
			expect(@rsubscriberData.get_active).to eq('Y'),                                        	"expected rsub device to be active"
			expect(@rsubscriberData.get_accountid).to eq @accountData.get_accountid
			expect(@rsubscriberData.get_cpnid).to eq @cpnData.get_cpnid
			expect(@rsubscriberData.get_startservice).to eq(@currentDate),                        	"rsub device startservice date was #{@rsubscriberData.get_startservice} expected it to be #{@currentDate}"
			expect(@rsubscriberData.get_endservice).to eq(@expExpDate),                           	"rsub device endservice date was #{@rsubscriberData.get_endservice} expected it to be #{@expExpDate}"
			expect(@rsubscriberData.get_device_type).to eq(@expDevicetype),                       	"expected rsub device type to be #{@expDevicetype} was #{@rsubscriberData.get_device_type} instead"
			expect(@rsubscriberData.get_e911serviceplan_productcodeid).to eq('288'),              	"expected rsub 911 product code id to be 288 but was #{@rsubscriberData.get_e911serviceplan_productcodeid} instead" # MJ 911 Service Plan
			expect(@rsubscriberData.get_autorenew).to eq('N'),                                    	"expected rsub autorenew to be 'N' but was #{@rsubscriberData.get_autorenew} instead"
			expect(@rsubscriberData.get_service_type).to eq('B'),                                 	"expected rsub service type to be 'B' but was #{@rsubscriberData.get_service_type} instead" 
			expect(@rsubscriberData.get_pastdue).to eq('N'),                                      	"expected rsub past due to be 'N' but was #{@rsubscriberData.get_pastdue} instead" 
			expect(@rsubscriberData.get_insurance_productcodeid).to eq(360),                    	  "expected rsub insurance product code id to be '360' but was #{@rsubscriberData.get_insurance_productcodeid} instead"
			expect(@rsubscriberData.get_lost).to eq('N'),                                         	"expected rsub lost to be 'N' but was #{@rsubscriberData.get_lost} instead"
			expect(@rsubscriberData.get_position).to eq(1),																			  	"expected rsub position to be '1' but was #{@rsubscriberData.get_position} instead" 
			expect(@rsubscriberData.get_devicename).to eq(@fname),                                	"expected rsub device name to be '1' #{@fname} but was #{@rsubscriberData.get_devicename} instead" 
			expect(@rsubscriberData.get_endpointid).to_not be_zero
			expect(@rsubscriberData.get_subscriptionid).not_to eq(0),                           	  "expected rsub subscription id to not be '0'" 
			expect(@rsubscriberData.get_serviceplan_productcodeid).to eq('358'),                  	 "expected rsub service plan product code id to be '358' but was #{@rsubscriberData.get_serviceplan_productcodeid} instead" # magicJackExpress Standard Dialing Plan
			expect(@rsubscriberData.get_renewal_lag).to eq('0'),                                  	"expected rsub renewal lag to be '0' but was #{@rsubscriberData.get_renewal_lag} instead" 
			expect(@rsubscriberData.get_suspended).to eq('N'),                                    	"expected rsub subscription suspended to be 'N'" 
			expect(@rsubscriberData.get_subscriptionactive).to eq('Y'),                           	"expected rsub subscription active to be 'Y'"
			expect(@rsubscriberData.get_treatments).to eq('0'),                                   	"expected rsub treatments active to be '0' but was #{@rsubscriberData.get_treatments} instead"
			expect(@rsubscriberData.get_vmtimer).to eq(25),                                     	  "expected rsub vmtimer active to be 25 but was #{@rsubscriberData.get_vmtimer} instead"
			expect(@rsubscriberData.get_e911service_locationid).to eq(@locationData.get_locationid),"expected rsub 911 service location id to be #{@locationData.get_locationid} but was #{@rsubscriberData.get_e911service_locationid} instead"
			expect(@rsubscriberData.get_e911service_billdate).to eq(@expExpDate), 									"expected rsub 911 billdate date to be #{@expExpDate} but was #{@rsubscriberData.get_e911service_billdate} instead"
		
			
			expect(@cpnData.get_cpn_type).to eq(@cpnTypeS),                                         "expected cpn type to be #{@cpnTypeS} but was #{@cpnData.get_cpn_type} instead" # Standard
			expect(@cpnData.get_accountid).to eq(@accountData.get_accountid),                       "expected cpn accountid to be #{@accountData.get_accountid} but was #{@cpnData.get_accountid} instead"                        
			expect(@cpnData.get_calling_party_no).to_not eq(0),                                     "expected cpn calling party number to not be '0' instead of #{@cpnData.get_calling_party_no}"                                    
			expect(@cpnData.get_class_of_service).to eq('V') ,                                      "expected cpn type to be 'V' but was #{@cpnData.get_class_of_service} instead" # our magicjack Number
			expect(@cpnData.get_type_of_service).to eq('3'),                                        "expected cpn type of service to be 3 but was #{@cpnData.get_type_of_service} instead"
			expect(@cpnData.get_dial_tone_company_id).to eq('YMX00'), 															"expected cpn dial tone company id to be 'YMX00' but was #{@cpnData.get_dial_tone_company_id} instead"
			expect(@cpnData.get_e911_status).to eq('PQ'), 																					"expected cpn e911 status to be 'PQ' but was #{@cpnData.get_e911_status} instead"
			expect(@cpnData.get_e911_tcsi_code).to eq('PICQ'),                                      "expected cpn e911 tcsi code to be 'PICQ' but was #{@cpnData.get_e911_tcsi_code} instead"
			expect(@cpnData.get_e911_tcsi_date).to eq(@currentDate),                                "expected cpn 911 tcsi date was #{@cpnData.get_e911_tcsi_date} instead of #{@currentDate}"
			expect(@cpnData.get_inservice).to eq(@currentDate),                                     "expected cpn in service date to be #{@cpnData.get_inservice} instead of #{@currentDate}"
			expect(@cpnData.get_outservice).to eq(@standardExpDate),                                "expected cpn out service date to be #{@cpnData.get_outservice} instead of #{@standardExpDate}"
			expect(@cpnData.get_cpn_type).to eq(@cpnTypeS),                                         "expected cpn type to be #{@cpnData.get_cpn_type} instead of #{@cpnTypeS}"
			expect(@cpnData.get_number_productcodeid).to eq(46),                                    "expected cpn product code id to be '46' instead of #{@cpnData.get_number_productcodeid}" # US Phone Number - Free
			expect(@cpnData.get_emailaddressid).to eq(@emailaddressData.get_emailaddressid),        "expected cpn email address id to be #{@emailaddressData.get_emailaddressid} instead of #{@cpnData.get_emailaddressid}"
			expect(@cpnData.get_state).to eq('TN'),                                                 "expected cpn state to be 'TN' instead of #{@cpnData.get_state}"
			expect(@cpnData.get_country).to eq('US'),                                               "expected cpn state to be 'US' instead of #{@cpnData.get_country}"
			expect(@cpnData.get_usagebits).to eq(1),                                                "expected cpn usagebits to be '1' instead of #{@cpnData.get_usagebits}" # Number of devices with number?
			expect(@cpnData.get_options).to eq(0),                                                  "expected cpn options to be '0' instead of #{@cpnData.get_options}"
			expect(@cpnData.get_autorenewdate).to eq(@expExpDate),																	"expected cpn autorenewdate to be #{@expExpDate} instead of #{@cpnData.get_autorenewdate}"
			expect(@cpnData.get_autorenew).to eq('N'),																							"expected cpn autorenew to be N instead of #{@cpnData.get_autorenew}"
			
			expect(@endpointData.get_endpointid).to eq(@rsubscriberData.get_endpointid),					 "expected endpoint id to be #{@rsubscriberData.get_endpointid} instead of #{@endpointData.get_endpointid}"
			expect(@endpointData.get_accountid).to eq(@accountData.get_accountid),					       "expected endpoint accountid to be #{@accountData.get_accountid} instead of #{@endpointData.get_accountid}"
			expect(@endpointData.get_subscriberid).to eq(@rsubscriberData.get_rsubscriberid),			 "expected endpoint rsubscriber id to be #{@rsubscriberData.get_rsubscriberid} instead of #{@endpointData.get_subscriberid}" 
			expect(@endpointData.get_cpnid).to eq(@cpnData.get_cpnid),			 									     "expected endpoint cpn id to be #{@cpnData.get_cpnid} instead of #{@endpointData.get_cpnid}" 
			expect(@endpointData.get_enumber).to eq('E'+@cpnData.get_calling_party_no+'01'),			 "expected endpoint enumber to be #{'E'+@cpnData.get_calling_party_no+'01'} instead of #{@endpointData.get_enumber}" 
			expect(@endpointData.get_rgid).to eq('900'),			 													 					 "expected endpoint rgid to be 900 instead of #{@endpointData.get_rgid}" 
			expect(@endpointData.get_subtype).to eq('R'),			 													 					 "expected endpoint subtype to be R instead of #{@endpointData.get_rgid}"  # Registered
			expect(@endpointData.get_locationid).to eq(@locationData.get_locationid),			 				 "expected endpoint location id to be #{@locationData.get_locationid} instead of #{@endpointData.get_locationid}"  
			expect(@endpointData.get_serial_no).to eq(@rsubscriberData.get_serial_no),			 			 "expected endpoint serial number to be #{@rsubscriberData.get_serial_no} instead of #{@endpointData.get_serial_no}"
			expect(@endpointData.get_position).to eq('1'),			 			 														 "expected endpoint position to be 1 instead of #{@endpointData.get_subscriptionid}"
			expect(@endpointData.get_subscriptionid).to eq(@rsubscriberData.get_subscriptionid),	 "expected endpoint subscription id to be #{@rsubscriberData.get_subscriptionid} instead of #{@endpointData.get_subscriptionid}"
			expect(@endpointData.get_device_type).to eq(@expDevicetype),	 												 "expected endpoint device type to be #{@expDevicetype} instead of #{@endpointData.get_device_type}" # W
			expect(@endpointData.get_serviceplan_productcodeid).to eq('358'),	 										 "expected endpoint product code id to be 358 instead of #{@endpointData.get_serviceplan_productcodeid}" # magicJackExpress Standard Dialing Plan
			expect(@endpointData.get_treatments).to eq('0'),	 										                 "expected endpoint treatments to be 0 instead of #{@endpointData.get_treatments}"
			expect(@endpointData.get_cpn_type).to eq(@cpnTypeS),	 										             "expected endpoint cpn type to be #{@cpnTypeS} instead of #{@endpointData.get_cpn_type}" # S
			expect(@endpointData.get_intrado_arg).to eq('TNI'),	 										               "expected endpoint intrado arg to be TNI instead of #{@endpointData.get_intrado_arg}"
			expect(@endpointData.get_e911_color).to eq('Y'),	 										                 "expected endpoint e911 color to be Y instead of #{@endpointData.get_e911_color}"
			expect(@endpointData.get_e911_description).to eq('E911 Address Processing'),	 				 "expected endpoint e911 description to be E911 Address Processing instead of #{@endpointData.get_e911_description}"
			expect(@endpointData.get_e911_verified).to eq(@currentDate),	 				 								 "expected endpoint e911 verified to be #{@currentDate} instead of #{@endpointData.get_e911_verified}"
			expect(@endpointData.get_e911_status).to eq('2'),	 				 								             "expected endpoint e911 status to be 2 instead of #{@endpointData.get_e911_status}"
			
			expect(@locationData.get_location_type).to eq('S'),	 				 								           "expected location location type to be S instead of #{@locationData.get_location_type}" # Shipping
			expect(@locationData.get_accountid).to eq(@accountData.get_accountid),	 				 			 "expected location account id to be #{@accountData.get_accountid} instead of #{@locationData.get_accountid}"
			expect(@locationData.get_first_name).to eq(@fname),	 				 			 										 "expected location first name to be #{@fname} instead of #{@locationData.get_first_name}"
			expect(@locationData.get_last_name).to eq(@lname),	 				 			 										 "expected location last name to be #{@lname} instead of #{@locationData.get_last_name}"
			expect(@locationData.get_dba).to eq(@fname + ' ' + @lname),	 				 			 						 "expected location dba to be #{@fname + ' ' + @lname} instead of #{@locationData.get_dba}"
			expect(@locationData.get_house_no).to eq('700'),	 				 			 						           "expected location house no to be 700 instead of #{@locationData.get_house_no}" # currently hard coded in reg.rb
			expect(@locationData.get_street_name).to eq('2ND '),	 				 			 						       "expected location street name to be 2nd instead of #{@locationData.get_street_name}" # currently hard coded in reg.rb
			expect(@locationData.get_street_name_suffix).to eq('AVE'),	 				 			 						 "expected location street name suffix to be AVE instead of #{@locationData.get_street_name_suffix}" # currently hard coded in reg.rb
			expect(@locationData.get_msag_community_name).to eq('NASHVILLE'),	 				 			 			 "expected location msag community name to be NASHVILLE instead of #{@locationData.get_msag_community_name}" # currently hard coded in reg.rb
			expect(@locationData.get_state).to eq('TN'),	 				 			 			 									   "expected location state to be TN instead of #{@locationData.get_state}" # currently hard coded in reg.rb
			expect(@locationData.get_zip_code).to eq('37210'),	 				 			 			 							 "expected location zipcode to be 37210 instead of #{@locationData.get_zip_code}" # currently hard coded in reg.rb
			expect(@locationData.get_active).to eq('Y'),	 				 			 			 							       "expected location active to be Y instead of #{@locationData.get_active}"
			expect(@locationData.get_country).to eq('US'),	 				 			 			 							     "expected location country to be US instead of #{@locationData.get_country}" # currently hard coded in reg.rb
			expect(@locationData.get_phone).to eq(@cpnData.get_calling_party_no),	 				 			 	 "expected location calling party no to be #{@cpnData.get_calling_party_no} instead of #{@locationData.get_phone}"
			expect(@locationData.get_geocode).to eq('4703752000'),	 				 			 	               "expected location geocode to be 4703752000 instead of #{@locationData.get_geocode}" # prepopuplated in db?  more test needed on geocode
			expect(@locationData.get_incity).to eq('I'),	 				 			 	                         "expected location incity to be I instead of #{@locationData.get_incity}" # Incity parameter passed into this test
			expect(@locationData.get_inlocal).to eq('O'),	 				 			 	                         "expected location inlocal to be 0 instead of #{@locationData.get_inlocal}" # default value
			
			expect(@emailaddressData.get_email_type).to eq('M'),	 				 			 	                 "expected email email type to be M instead of #{@emailaddressData.get_email_type}"
			expect(@emailaddressData.get_accountid).to eq(@accountData.get_accountid),	 				 	 "expected email accountid to be #{@accountData.get_accountid} instead of #{@emailaddressData.get_accountid}"
			expect(@emailaddressData.get_emailaddr).to eq(@email),	 				 	 										 "expected email email address  to be #{@email} instead of #{@emailaddressData.get_emailaddr}" # defined within reg.rb
			expect(@emailaddressData.get_emailkey).to eq(@email),	 				 	 										   "expected email email key  to be #{@email} instead of #{@emailaddressData.get_emailkey}" # defined within reg.rb
			expect(@emailaddressData.get_password).to eq('VWVWVW12'),	 				 	 									 "expected email password  to be VWVWVW12 instead of #{@emailaddressData.get_password}" # defined within reg.rb
			expect(@emailaddressData.get_marketing).to eq('9'),	 				 	 									       "expected email get marketing emails to be 9 instead of #{@emailaddressData.get_marketing}" # customer will receive marketing emails
			
			expect(@creditcardData.get_accountid).to eq(@accountData.get_accountid),	 				 	 	 "expected creditcard accountid to be #{@accountData.get_accountid} instead of #{@creditcardData.get_accountid}" # customer will receive marketing emails
			expect(@creditcardData.get_full_name).to eq(@fname + '  ' +@lname),  	 				 	 	     "expected creditcard full name to be #{@fname + '  ' +@lname} instead of #{@creditcardData.get_full_name}"
			expect(@creditcardData.get_first_name).to eq(@fname),  	 				 	 	     						   "expected creditcard first name to be #{@fname} instead of #{@creditcardData.get_first_name}"
			expect(@creditcardData.get_last_name).to eq(@lname),  	 				 	 	     						   "expected creditcard last name to be #{@lname} instead of #{@creditcardData.get_last_name}"
			expect(@creditcardData.get_card_type).to eq('V'),  	 				 	 	     						       "expected creditcard last name to be V instead of #{@creditcardData.get_cardnumber}" # from reg.rb
			expect(@creditcardData.get_cardnumber).to_not eq('0'),  	 				 	 	     						 "expected creditcard card number to not be 0 instead of #{@creditcardData.get_cardnumber}"
			expect(@creditcardData.get_exp_date).to eq('122019'),  	 				 	 	     						   "expected creditcard expiration date to not be 122019 instead of #{@creditcardData.get_exp_date}" # set in reg.rb
			expect(@creditcardData.get_active).to eq('Y'),  	 				 	 	     						   		   "expected creditcard active to be Y instead of #{@creditcardData.get_active}" # set in reg.rb
			expect(@creditcardData.get_fraud_override).to eq('N'),  	 				 	 	     						 "expected creditcard active to be N instead of #{@creditcardData.get_fraud_override}"
			expect(@creditcardData.get_cvv2_match).to eq('Y'),  	 				 	 	     						     "expected creditcard cvv2 match to be Y instead of #{@creditcardData.get_cvv2_match}"
			expect(@creditcardData.get_processor_avs).to eq('F'),  	 				 	 	     						   "expected creditcard processor avs to be F instead of #{@creditcardData.get_processor_avs}"
			expect(@creditcardData.get_addr_match).to eq('-'),  	 				 	 	     						     "expected creditcard address match to be - instead of #{@creditcardData.get_addr_match}"
			expect(@creditcardData.get_zip_match).to eq('-'),  	 				 	 	     						       "expected creditcard zip match to be - instead of #{@creditcardData.get_zip_match}"
			expect(@creditcardData.get_debit).to eq('N'),  	 				 	 	     						           "expected creditcard debit to be N instead of #{@creditcardData.get_debit}"
			expect(@creditcardData.get_locationid).to eq(@accountData.get_locationid),  	 				 "expected creditcard location id to be #{@accountData.get_locationid} instead of #{@creditcardData.get_locationid}"
			expect(@creditcardData.get_first_charge_date).to eq(@currentDate),  	 				         "expected creditcard first charge date to be #{@currentDate} instead of #{@creditcardData.get_first_charge_date}"
			expect(@creditcardData.get_charge_count).to eq('1'),  	 				         							 "expected creditcard charge count to be 1 instead of #{@creditcardData.get_charge_count}"
			expect(@creditcardData.get_use_for_recurring).to eq('D'),  	 				         					 "expected creditcard use for recurring to be D instead of #{@creditcardData.get_use_for_recurring}"
			expect(@creditcardData.get_istoken).to eq('Y'),  	 				         					           "expected creditcard it token to be Y instead of #{@creditcardData.get_istoken}"
			expect(@creditcardData.get_mop).to eq('VI'),  	 				         					             "expected creditcard mop to be VI instead of #{@creditcardData.get_mop}"
			
			expect(@ordersData.get_orderid).to_not be_zero
			expect(@ordersData.get_inbound_orderid).to eq('0'),                                    "expected order in bound orderid to be '0' instead of #{@ordersData.get_inbound_orderid}"
			expect(@ordersData.get_accountid).to eq(@accountData.get_accountid),                   "expected order account id to be #{@accountData.get_accountid} instead of #{@ordersData.get_accountid}"
			expect(@ordersData.get_emailaddressid).to eq(@emailaddressData.get_emailaddressid),    "expected order email addressid to be #{@emailaddressData.get_emailaddressid} instead of #{@ordersData.get_emailaddressid}"
			expect(@ordersData.get_creditcardid).to eq(@creditcardData.get_creditcardid),          "expected order credit card id to be #{@creditcardData.get_creditcardid} instead of #{@ordersData.get_creditcardid}"
			expect(@ordersData.get_order_number).to_not eq('0' ),    															 "expected order order number to not be 0 instead of #{@ordersData.get_order_number}"
			expect(@ordersData.get_order_date).to eq(@currentDate),    														 "expected order date to be #{@currentDate} instead of #{@ordersData.get_order_date}"
			expect(@ordersData.get_order_time).to_not eq(''),    																   "expected order time id to not be blank instead of #{@ordersData.get_order_time}"
			expect(@ordersData.get_order_status).to eq('N'),																	     "expected order order status to be 'N' instead of #{@ordersData.get_order_status}"
			expect(@ordersData.get_statusdate).to eq(@currentDate),    														 "expected order status date to be 10 instead of #{@ordersData.get_subtotal}"
			expect(@ordersData.get_subtotal).to eq('10'),    																			 "expected order subtotal to be 10 instead of #{@ordersData.get_subtotal}"
			expect(@ordersData.get_shipping).to eq('0'),    																			 "expected order shipping to be 0 instead of #{@ordersData.get_shipping}"
			expect(@ordersData.get_taxes).to eq('0.78'),    																			 "expected order taxes to be 0.78 instead of #{@ordersData.get_taxes}"
			expect(@ordersData.get_total).to eq('10.78'),                                          "expected order total to be '0' instead of #{@ordersData.get_total}"
			expect(@ordersData.get_taxrateid).to eq('1012'),                                       "expected order tax rate id to be '1012' instead of #{@ordersData.get_taxrateid}"
			expect(@ordersData.get_taxrate).to eq('.078'),                                         "expected order tax rate to be '.078' instead of #{@ordersData.get_taxrate}"
			expect(@ordersData.get_adj_subtotal).to eq('0'),                                       "expected order adj subtotal to be 0 instead of #{@ordersData.get_adj_subtotal}"
			expect(@ordersData.get_adj_taxes).to eq('0'),                                          "expected order adj taxes to be 0 instead of #{@ordersData.get_adj_taxes}"
			expect(@ordersData.get_adj_shipping).to eq('0'),                                       "expected order adj shipping to be 0 instead of #{@ordersData.get_adj_shipping}"
			expect(@ordersData.get_adj_total).to eq('0'),                                          "expected order adj total to be 0 instead of #{@ordersData.get_adj_total}"
			expect(@ordersData.get_adj_sum_total).to eq('10.78'),                                  "expected order adj sum total to be 10.78 instead of #{@ordersData.get_adj_sum_total}"
			expect(@ordersData.get_unpaid).to eq('N'),                                             "expected order unpaid to be 0 instead of #{@ordersData.get_unpaid}"
			expect(@ordersData.get_payment).to eq('N'),                                            "expected order payment to be N instead of #{@ordersData.get_payment}"
			expect(@ordersData.get_result_code).to eq('0'),                                        "expected order result code to be 0 instead of #{@ordersData.get_result_code}"
			expect(@ordersData.get_last_auth_attempt).to eq(@currentDate),                         "expected order last auth attempt to be #{@currentDate} instead of #{@ordersData.get_last_auth_attempt}"
			expect(@ordersData.get_auth_attempts).to eq('1'),                         						 "expected order auth attempts to be 1 instead of #{@ordersData.get_auth_attempts}"
			expect(@ordersData.get_auth_total).to eq('10.78'),                         						 "expected order auth total to be 10.78 instead of #{@ordersData.get_auth_total}"
			expect(@ordersData.get_authmessage).to eq('Approved'),                                 "expected order auth message id to be 'Approved' instead of #{@ordersData.get_authmessage}"
			expect(@ordersData.get_sourceind).to eq('W'),                                          "expected order sourceind id to be 'W' instead of #{@ordersData.get_sourceind}"
			expect(@ordersData.get_nflags).to eq('0'),                                             "expected order nflags id to be '0' instead of #{@ordersData.get_nflags}"
			expect(@ordersData.get_campaignid).to eq('1'),                                         "expected order campaign id to be '1' instead of #{@ordersData.get_campaignid}"
			expect(@ordersData.get_cancelreason).to eq('0'),                                       "expected order cancel to be '0' instead of #{@ordersData.get_cancelreason}"
			#expect(@ordersData.get_total).to eql('0')

            # calling orderDetailData 1
			expect(@orderDetailRecords.to_s).to include("FREENUMBER"),                      				"expected order detail to contain FREENUMBER"
			expect(@orderDetailRecords.to_s).to include("NO2014MJREPLACEMENT"),                     "expected order detail to contain NO2014MJREPLACEMENT"
			expect(@orderDetailRecords.to_s).to include("2014MJSTDDIALPLAN"),                       "expected order detail to contain 2014MJSTDDIALPLAN"
			expect(@orderDetailRecords.to_s).to include("E911MJSUBSCRIPTION"),                      "expected order detail to contain E911MJSUBSCRIPTION"
			expect(@orderDetailRecords.to_s).to include("246"),                                     "expected order detail to contain 246" #product code for 2014MJSTDDIALPLAN
			expect(@orderDetailRecords.to_s).to include("213"),                                     "expected order detail to contain 223" #product code for NO2014MJREPLACEMENT
			expect(@orderDetailRecords.to_s).to include("46"),                                      "expected order detail to contain 223" #product code for FREENUMBER
			expect(@orderDetailRecords.to_s).to include("288"),                                     "expected order detail to contain 223" #product code for E911MJSUBSCRIPTION

    	end

     	rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        	puts e.message
       		puts @fname
       		exit(1)
     	end	    
	  	      
  	end

 
end
