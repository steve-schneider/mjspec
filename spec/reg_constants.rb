    CHKEMAIL  		 =  { id: 'chkEmail' }
    ALAUNCHER 		 =  { id: 'aLauncher' }
    JACKTYPE  		 =  { id: 'selSerialType' }
    SERIALNUM 		 =  { id: 'selSerial' } 
    
    CREATEACCOUNT 	 =  { css: '.col-b>button.btn' }
    
    #
    COUNTRY          =  { id: 'activation:account-country'}
    #
    FIRSTNAME    	 =  { id: 'first_name' }
    LASTNAME     	 =  { id: 'last_name' }
    STREETNAME   	 =  { id: 'street_name' }
    #
    LOCATION     	 =  { id: 'location' }
    #
    CITY         	 =  { id: 'msag_community_name'}
    STATE        	 =  { id: 'activation:location[-3].state'}
    ZIP          	 =  { id: 'zip_code'}
    EMAIL        	 =  { id: 'conf-email'}
    EMAILVERIFY 	 =  { id: 'email-verify'}
    PASSWORD     	 =  { id: 'password'}
    PASSWORDVERIFY   =  { id: 'password-verify'}
    CREATEACCOUNT2	 =	{ xpath: "(//button[@type='button'])[2]"}
    
    DEVICENAME		 =	{ id: 'device-name'}
    EMAILMETHOD		 =	{ id: 'emailMethod'}
    TXTMETHOD		 =  { id: 'smsMethod' }
    TXTPHONE         =  { id: 'activate_activation:phoneNumber_1' }
    SENDMYCODE		 =	{ css: 'button[type=submit]'}
    
    ACTCODE			 =	{ id: 'code1'}
    ACTIVATEMYDEVICE =	{ css: 'button[type=submit]'}
    
    TERMSACCEPT		 =	{ id: 'terms-accept'}
    CONTINUE 		 =	{ css: 'button[type=submit]'}
    
    CPNVANITY        =  { xpath: ".//*[@id='activation-number']/div/div[2]/a[3]/div/p"}
    CPNCANADIAN      =  { xpath: ".//*[@id='activation-number']/div/div[2]/a[2]/div/p"}
    CPNSEARCVAN      =  { id: 'search-vanity'}
    CPNCHOOSEDIGITS  =  { id: 'choose-digits'}
    CPNCUSTOMNUM     =  { id: 'freenumber'}
    CPNSTATE         =  { id: 'activation:cpn[-1].state'}
    CPNAREACODE      =  { id: 'activation:cpn[-1].areacode'}
    CPNAREAEXCHANGE  =  { id: 'activation:cpn[-1].exchange'}
    CPNCALLINGPN     =  { id: 'activation:cpn[-1].calling_party_no'}
    CPNSEARCHBTN     =  { id: 'btnSubmit'}
    CPNADDTOCART     =  { id: 'btnSubmit'}
    #
    RSUBSERVICETYPE  =  { id: 'service_type'}
    #
    
    E911POPUP        =  { id: 'modalPopup'}
    E911LOCATION     =  { id: 'activation:e911locationid'}
    E911LOCATIONADD  =  { id: 'activation:e911inttype'}
    #
    E911COUNTRY      =  { id: 'activation:e911-country'}
    #
    E911FIRSTNAME    =  { id: 'first_name'}
    E911LASTNAME     =  { id: 'last_name'}
    E911STREETNAME   =  { id: 'street_name'}
    E911ADDR2        =  { id: 'location'} #address 2 apt number etc
    E911CITY         =  { id: 'msag_community_name'}
    E911STATE        =  { id: 'activation:location[-99].state'} #types TN
    E911ZIPCODE      =  { id: 'zip_code'}
    E911LOCADDCONF   =  { xpath: "(//button[@type='button'])[2]"}
    E911CONFIRMLOC   =  { xpath: ".//*[@id='activation-911']/div/div[3]/button"}
    E911CONFIRM      =  { css: "input[name='confirm']"}
    E911CONFIRMNO    =  { xpath: "(//input[@name='confirm'])[2]"}
    E911INCITY       =  { css: "input[name='incity']"}
    E911INCITYNO     =  { xpath: "(//input[@name='incity'])[2]" }
    E911CONFIRMCITY  =  { xpath: ".//*[@id='modalPopup']/div/center/div[2]/button"}
    #
    E911NONUSCHECK   =  { id: 'non-us-check'}
    E911CONFIRMIT    =  { xpath: "(//button[@type='button'])[2]"}
    #
    
    FIVEYEAR         =  { id: '5year'}
    ONEYEAR          =  { id: '1year'}
    ADDORENEWALTOCART=  { id: 'years_submit_1'}
    AUTORENEWFIVEYEAR=  { id: 'years_activation:plan-option.autorenew_1'}
    AUTORENEWONEYEAR =  { id: 'years_activation:plan-option.autorenew_3'}
    SKIPFORNOW       =  { css: 'button[type=submit]'}
    TAKEIPP5         =  { id: 'sel-credit-a'}
    TAKEIPP10        =  { id: 'sel-credit-b'}
    TAKEIPP20        =  { id: 'sel-credit-c'}
    TAKEIPP40        =  { id: 'sel-credit-d'}
    PROCEEDTOCHECKOUT=  { xpath: "(//button[@type='submit'])[2]"}
    
    CCLOCATION       =  { id: 'activation:billinglocationid'}
    CARDNUMBER       =  { id: 'cardnumber'}
    CVV2             =  { id: 'cvv2'}
    CCEXPMONTH       =  { id: 'activation:creditcard[-1].month'}
    CCEXPYEAR        =  { id: 'year'}
    REVIEWORDER      =  { css: '.order-form-box > div:nth-child(2) > div:nth-child(2) > button:nth-child(1)'}

    PLACEORDER       =  { css: 'button[type=submit]'}
    
    #
    EXITBTNUPPER     =  { id: "btnClose"}
    #
    EXITBTNLOWER     =  { id: "btnExit"}
    CLOSEREG         =  { id: "aCloser"}
    
    SUCCESSMESSAGE   =  { class: "cartdetail-item"}
    
    CSRNAME      	 =  { id: 'inpUsr' }
    CSRPASSWORD  	 =  { id: 'inpPsw' }
    CSRLOGIN     	 =  { id: 'btnLogin' }
    ACCOUNTSTAB  	 =  { link: 'Accounts' }
    CSRLOOKUP    	 =  { id: 'inpLkp' }
    CSRSEARCH    	 =  { id: 'btnSearchAcct' }
    CSRDEVICETAB 	 =  { link: 'Devices' }
    
    #
    ADDNEWBILLLOC    =  { id: 'activation:billinginttype' }
    NEWBILLFIRSTNAME =  { id: 'first_name' }
    NEWBILLLASTNAME  =  { id: 'last_name' }
    NEWBILLSTREETNAME=  { id: 'street_name' }
    NEWBILLLOCATION  =  { id: 'location' }
    NEWBILLCITY      =  { id: 'msag_community_name'}
    NEWBILLSTATE     =  { id: 'activation:location[-1].state'}
    NEWBILLZIP       =  { id: 'zip_code'}
    #