require "json"
require "date"
require "rspec"
require_relative "reg_spec_helper.rb"
require_relative "../pages/reg.rb"
require_relative "../db/AccountDB.rb"
require_relative "../db/RsubscriberDB.rb"
require_relative "../db/CpnDB.rb"
require_relative "../db/EndPointDB.rb"
require_relative "../db/LocationDB.rb"
require_relative "../db/EmailAddressDB.rb"
require_relative "../db/CreditCardDB.rb"
require_relative "../db/IppDB.rb"
require_relative "../db/RegDB.rb"
require_relative "../db/OrdersDB.rb"
require_relative "../db/OrderDetailDB.rb"
include RSpec::Expectations

describe "Device Registration" do


  it "Registers a GO with a US number" do                 
	  @registerJack.register_with(@genGo,@fname,'EMAIL','US','BOTH','SKIP','inCityYes','SKIP', 'SKIP','NO')
	  
	  	@accountData.set_account_data(@fname)	
	    rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
		@rsubscriberData.set_rsubscriber_data(rsubscriberPK)
	    @emailaddressData.set_email_address_data(@accountData.get_emailaddressid)
	    @cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
	    @orderPK = @ordersData.get_order_pk(@accountData.get_accountid)
	    @ordersData.set_orders_data(@orderPK)
	   	   
		expect(@rsubscriberData.get_accountid).to_not be_zero
		expect(@accountData.get_locationid).to_not be_zero
		expect(@rsubscriberData.get_cpnid).to_not be_zero
		expect(@emailaddressData.get_emailaddressid).to_not be_zero
		expect(@ordersData.get_total).to eql "0"
		expect(@rsubscriberData.get_endservice).to eq @expDate
		expect(@cpnData.get_cpn_type).to eq 'S'
		expect(@accountData.get_customer_name).to eq @fname + ' SCHNEIDER'				
    
  end



  it "Registers a GO with a Vanity number" do                 
	  @registerJack.register_with(@genGo,@fname,'EMAIL','Vanity','BOTH','SKIP','inCityYes','SKIP', 'SKIP','NO')

	  	@accountData.set_account_data(@fname)	
	    rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
		@rsubscriberData.set_rsubscriber_data(rsubscriberPK)
	    @emailaddressData.set_email_address_data(@accountData.get_emailaddressid)
	    @cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
	    @IppData.set_ipp_data(@accountData.get_accountid)
	    @orderPK = @ordersData.get_order_pk(@accountData.get_accountid)
	    @ordersData.set_orders_data(@orderPK)         	  
	  					
		expect(@rsubscriberData.get_accountid).to_not be_zero
		expect(@accountData.get_locationid).to_not be_zero
		expect(@rsubscriberData.get_cpnid).to_not be_zero
		expect(@emailaddressData.get_emailaddressid).to_not be_zero
		expect(@ordersData.get_total).to eql "10.78"
		expect(@rsubscriberData.get_endservice).to eq @expDate
		expect(@cpnData.get_cpn_type).to eq 'V'
		expect(@accountData.get_customer_name).to eq @fname + ' SCHNEIDER'	
		
		
	     
	      
  end
  
=begin
    it "Registers a GO with a Custom number" do                 
	  	  @registerJack.register_with(@genGo,@fname,'EMAIL','Custom','BOTH','SKIP','inCityYes','SKIP', 'SKIP','NO')
	 
		@accountData.set_account_data(@fname)	
		@rsubscriberData.set_rsubscriber_data(@accountData.get_accountid)
		@emailaddressData.set_email_address_data(@accountData.get_emailaddressid)
		@cpnData.set_cpn_data(@rsubData.get_cpnid)
		@IppData.set_ipp_data(@accountData.get_accountid)
		@orderPK = @orderData.get_order_pk(@accountData.get_accountid)
		@orderData.set_orders_data(@orderPK)	        
	         	  

	   expect(@rsubData.get_accountid).to_not be_zero
	   expect(@accountData.get_locationid).to_not be_zero
	   expect(@rsubData.get_cpnid).to_not be_zero
	   expect(@emailaddressData.get_emailaddressid).to_not be_zero
	   expect(ordTot).to eql "3.23"
	   expect(@rsubData.get_endservice).to eq @expDate
	   expect(@cpnData.get_cpn_type).to eq 'K'
	   expect(@accountData.get_customer_name).to eq @fname + ' SCHNEIDER'				
	 	   		    
      
  end
 
  it "Registers a GO with a Canadian number" do                 
	  	  @registerJack.register_with(@genGo,@fname,'TXT','Canadian','BOTH','SKIP','inCityYes','SKIP', 'SKIP','NO')
	  
	  	@accountData.set_account_data(@fname)	
		@rsubscriberData.set_rsubscriber_data(@accountData.get_accountid)
		@emailaddressData.set_email_address_data(@accountData.get_emailaddressid)
		@cpnData.set_cpn_data(@rsubData.get_cpnid)
		@IppData.set_ipp_data(@accountData.get_accountid)
		@orderPK = @orderData.get_order_pk(@accountData.get_accountid)
		@orderData.set_orders_data(@orderPK)	        
	         	  

	   expect(@rsubData.get_accountid).to_not be_zero
	   expect(@accountData.get_locationid).to_not be_zero
	   expect(@rsubData.get_cpnid).to_not be_zero
	   expect(@emailaddressData.get_emailaddressid).to_not be_zero
	   expect(ordTot).to eql "10.78"
	   expect(@rsubData.get_endservice).to eq @expDate
	   expect(@cpnData.get_cpn_type).to eq 'C'
	   expect(@accountData.get_customer_name).to eq @fname + ' SCHNEIDER'	
	  	    
	      
  end

     
  it "Registers a GO with a US number and IPP10" do                 
	  @registerJack.register_with(@genGo,@fname,'TXT','US','BOTH','SKIP','inCityYes','SKIP', 'IPP10','NO')
	  
	  	  
		@accountData.set_account_data(@fname)	
		@rsubscriberData.set_rsubscriber_data(@accountData.get_accountid)
		@emailaddressData.set_email_address_data(@accountData.get_emailaddressid)
		@cpnData.set_cpn_data(@rsubData.get_cpnid)
		@IppData.set_ipp_data(@accountData.get_accountid)
		@orderPK = @orderData.get_order_pk(@accountData.get_accountid)
		@orderData.set_orders_data(@orderPK)	        
	
		  
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '10.78'
		  expect(@registerJack.expireDate).to eq @expDate
		  expect(@registerJack.cpnType).to eq 'S'
		  expect(@registerJack.ippAmt).to eq "1000"
	 	    
end

 
  it "Registers a GO with a US number and IPP20" do                 
	  @registerJack.register_with("Generate magicJackGO",@fname, 'US', 'SKIP', 'IPP20','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '21.56'
		  expect(@registerJack.expireDate).to eq @expDate
		  expect(@registerJack.cpnType).to eq 'S'
		  expect(@registerJack.ippAmt).to eq "2000"
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        ##exit(1)
     end		    
	      
  end


  it "Registers a GO with a US number and IPP40" do                 
	  @registerJack.register_with("Generate magicJackGO",@fname, 'US', 'SKIP', 'IPP40','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '43.12'
		  expect(@registerJack.expireDate).to eq @expDate
		  expect(@registerJack.cpnType).to eq 'S'
		  expect(@registerJack.ippAmt).to eq "4000"
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        ##exit(1)
     end		    		    
	      
  end


  it "Registers a GO with a US number and a 1 year renewal with auto renew" do                 
	  @registerJack.register_with("Generate magicJackGO",@fname, 'US', '1YR', 'SKIP','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '37.73'
		  expect(@registerJack.expireDate).to eq @expDate1yr
		  expect(@registerJack.cpnType).to eq 'S'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    		    
  end
  
 
  it "Registers a GO with a US number and a 5 year renewal with auto renew" do                 
	  @registerJack.register_with("Generate magicJackGO",@fname, 'US', '5YR', 'SKIP','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '107.53'
		  expect(@registerJack.expireDate).to eq @expDate5yr
		  expect(@registerJack.cpnType).to eq 'S'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
  
   it "Registers a GO with a US number, 5 year renewal with auto renew, and IPP 20" do                 
	  @registerJack.register_with("Generate magicJackGO",@fname, 'US', '5YR', 'IPP20','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '107.53'
		  expect(@registerJack.expireDate).to eq @expDate5yr
		  expect(@registerJack.cpnType).to eq 'S'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
  
   it "Registers a GO with a US number , 5 year renewal with auto renew, and IPP 40" do                 
	  @registerJack.register_with("Generate magicJackGO",@fname, 'US', '5YR', 'IPP40','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '107.53'
		  expect(@registerJack.expireDate).to eq @expDate5yr
		  expect(@registerJack.cpnType).to eq 'S'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
  
    it "Registers a GO with a US number , 1 year renewal with auto renew, and IPP 20" do                 
	  @registerJack.register_with("Generate magicJackGO",@fname, 'US', '1YR', 'IPP20','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '107.53'
		  expect(@registerJack.expireDate).to eq @expDate5yr
		  expect(@registerJack.cpnType).to eq 'S'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
  
   it "Registers a GO with a US number , 1 year renewal with auto renew, and IPP 40" do                 
	  @registerJack.register_with("Generate magicJackGO",@fname, 'US', '1YR', 'IPP40','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '107.53'
		  expect(@registerJack.expireDate).to eq @expDate5yr
		  expect(@registerJack.cpnType).to eq 'S'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
  
  it "Registers a GO with a Vanity number and a 1 year renewal with auto renew" do                 
	  @registerJack.register_with("Generate magicJackGO",@fname, 'Vanity', '1YR', 'SKIP','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '107.53'
		  expect(@registerJack.expireDate).to eq @expDate5yr
		  expect(@registerJack.cpnType).to eq 'V'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
  
  it "Registers a GO with a Custom number and a 1 year renewal with auto renew" do                 
	  @registerJack.register_with("Generate magicJackGO",@fname, 'Custom', '1YR', 'SKIP','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '107.53'
		  expect(@registerJack.expireDate).to eq @expDate5yr
		  expect(@registerJack.cpnType).to eq 'K'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
  
  it "Registers a GO with a Canadian number and a 1 year renewal with auto renew" do                 
	  @registerJack.register_with("Generate magicJackGO",@fname, 'Canadian', '1YR', 'SKIP','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '107.53'
		  expect(@registerJack.expireDate).to eq @expDate5yr
		  expect(@registerJack.cpnType).to eq 'C'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end

  it "Registers an EXPRESS with a US number" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'US', 'SKIP', 'SKIP','911addressCorrect','inCityYes','EMAIL')
	  begin
	  	  aggregate_failures "Registers an EXPRESS with a US number" do
	      expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '0'
		  expect(@registerJack.expireDate).to eq @expExpDate
		  expect(@registerJack.cpnType).to eq 'S'
     end
     rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
       #exit(1)
     end	    
	      
  end

  it "Registers an EXPRESS with a Vanity number" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'Vanity', 'SKIP', 'SKIP','911addressCorrect','inCityYes','EMAIL')
	  begin
	  	  aggregate_failures "Registers an EXPRESS with a Vanity number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '10.78'
		  expect(@registerJack.expireDate).to eq @expExpDate
		  expect(@registerJack.cpnType).to eq 'V'
	 end
	 rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
     end		    
	      
  end
  
    it "Registers an EXPRESS with a Custom number" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'Custom', 'SKIP', 'SKIP','911addressCorrect','inCityYes','EMAIL')	  
	  begin
		  aggregate_failures "Registers an EXPRESS with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '3.23'
		  expect(@registerJack.expireDate).to eq @expExpDate
		  expect(@registerJack.cpnType).to eq 'K'
	  end	
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
     end		   		    
      
  end
  
    it "Registers an EXPRESS with a Canadian number" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'Canadian', 'SKIP', 'SKIP','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers an EXPRESS with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '10.78'
		  expect(@registerJack.expireDate).to eq @expExpDate
		  expect(@registerJack.cpnType).to eq 'C'
	  end	
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
     end		    
	      
  end
     
  it "Registers an EXPRESS with a US number and IPP10" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'US', 'SKIP', 'IPP10','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers an EXPRESS with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '10.78'
		  expect(@registerJack.expireDate).to eq @expExpDate
		  expect(@registerJack.cpnType).to eq 'S'
		  expect(@registerJack.ippAmt).to eq "1000"
	  end		
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
     end		    
end

  
  it "Registers an EXPRESS with a US number and IPP20" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'US', 'SKIP', 'IPP20','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers an EXPRESS with a US number and IPP20" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '21.56'
		  expect(@registerJack.expireDate).to eq @expExpDate
		  expect(@registerJack.cpnType).to eq 'S'
		  expect(@registerJack.ippAmt).to eq "2000"
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
     end		    
	      
  end


  it "Registers an EXPRESS with a US number and IPP40" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'US', 'SKIP', 'IPP40','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers an EXPRESS with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '43.12'
		  expect(@registerJack.expireDate).to eq @expExpDate
		  expect(@registerJack.cpnType).to eq 'S'
		  expect(@registerJack.ippAmt).to eq 4000
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
     end		    		    
	      
  end


  it "Registers an EXPRESS with a US number and a 1 year renewal with auto renew" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'US', '1YR', 'SKIP','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers an EXPRESS with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '37.73'
		  expect(@registerJack.expireDate).to eq @expExpDate1yr
		  expect(@registerJack.cpnType).to eq 'S'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    		    
  end
  
 
  it "Registers an EXPRESS with a US number and a 5 year renewal with auto renew" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'US', '5YR', 'SKIP','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers an EXPRESS with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '107.53'
		  expect(@registerJack.expireDate).to eq @expExpDate5yr
		  expect(@registerJack.cpnType).to eq 'S'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
    it "Registers an Express with a US number, 5 year renewal with auto renew, and IPP 20" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'US', '5YR', 'IPP20','911addressCorrect','inCityYes','EMAIL')
	  begin
	  begin
	  	  aggregate_failures "Registers an Express with a US number, 5 year renewal with auto renew, and IPP 20" do
	      expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '129.09'
		  expect(@registerJack.expireDate).to eq @expExpDate5yr
		  expect(@registerJack.cpnType).to eq 'S'
		  expect(@registerJack.customerName).to eq @fname + ' SCHNEIDER'
		  expect(@registerJack.ippAmt).to eq "2000"
     end
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
  
   it "Registers an Express with a US number, 5 year renewal with auto renew, and IPP 40" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'US', '5YR', 'IPP40','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '150.65'
		  expect(@registerJack.expireDate).to eq @expDate5yr
		  expect(@registerJack.cpnType).to eq 'S'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
  
    it "Registers an Express with a US number, 1 year renewal with auto renew, and IPP 40" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'US', '1YR', 'IPP40','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '80.85'
		  expect(@registerJack.expireDate).to eq @expDate5yr
		  expect(@registerJack.cpnType).to eq 'S'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
  
   it "Registers an Express with a US number, 1 year renewal with auto renew, and IPP 20" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'US', '1YR', 'IPP20','911addressCorrect','inCityYes','EMAIL')
	  begin
		  aggregate_failures "Registers a GO with a US number" do
		  expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '59.29'
		  expect(@registerJack.expireDate).to eq @expDate5yr
		  expect(@registerJack.cpnType).to eq 'S'
	  end
	  rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
        #exit(1)
      end		    
	      
  end
  
    it "Registers an EXPRESS with a Vanity number and IPP 10" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'Vanity', 'SKIP', 'IPP10','911addressNew','inCityYes','EMAIL')
	  begin
	  	  aggregate_failures "Registers an EXPRESS with a US number" do
	      expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '21.56'
		  expect(@registerJack.expireDate).to eq @expExpDate
		  expect(@registerJack.cpnType).to eq 'V'
     end
     rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
       #exit(1)
     end	    
	      
  end
  
   it "Registers an EXPRESS with a Custom number and IPP 20" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'Custom', 'SKIP', 'IPP20','911addressNew','inCityNo','EMAIL')
	  begin
	  	  aggregate_failures "Registers an EXPRESS with a US number" do
	      expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '24.79'
		  expect(@registerJack.expireDate).to eq @expExpDate
		  expect(@registerJack.cpnType).to eq 'K'
     end
     rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
       #exit(1)
     end	    
	      
  end
  
  it "Registers an EXPRESS with a Canadian number and IPP 40" do                 
	  @registerJack.register_with("Generate magicJackEXPRESS",@fname, 'Canadian', 'SKIP', 'IPP 40','911addressNew','inCityYes','TXT')
	  begin
	  	  aggregate_failures "Registers an EXPRESS with a US number" do
	      expect(@registerJack.accountid).to_not be_zero
		  expect(@registerJack.locationid).to_not be_zero
		  expect(@registerJack.cpnid).to_not be_zero
		  expect(@registerJack.emailaddressid).to_not be_zero
		  expect(@registerJack.ordTot).to eql '53.90'
		  expect(@registerJack.expireDate).to eq @expExpDate
		  expect(@registerJack.cpnType).to eq 'C'
     end
     rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
        puts @fname
       #exit(1)
     end	    
	      
  end
=end    

end
