require "json"
require "date"
require "rspec"
require_relative "spec_helper.rb"
require_relative "../pages/reg.rb"
require_relative "../db/AccountDB.rb"
require_relative "../db/RsubscriberDB.rb"
require_relative "../db/CpnDB.rb"
require_relative "../db/EndPointDB.rb"
require_relative "../db/LocationDB.rb"
require_relative "../db/EmailAddressDB.rb"
require_relative "../db/CreditCardDB.rb"
require_relative "../db/IppDB.rb"
require_relative "../db/RegDB.rb"
require_relative "../db/OrdersDB.rb"
require_relative "../db/OrderDetailDB.rb"
require_relative "../db/OrderStatusDB.rb"
require_relative "../db/InvoiceDB.rb"
require_relative "../db/ActionDB.rb"
require_relative "../db/NotesDB.rb"
include RSpec::Expectations

describe "Device Registration" do

  before(:each) do

    @lname = "SCHNEIDER"
    @email ="STEVE.SCHNEIDER%"+@fname+"@MAGICJACK.COM"
    @orderStatusdata = OrderStatusDB.new
  end

  # OLD def register_with(jack_type, fname, numType, renewType, ippType, addr911, inCity, actCodeNotification)
  # NEW def register_with(jack_type, fname, actCodeNotification, numType, serviceType, addr911, inCity, renewType, ippType, newbillLocation)
 it "Registers an CA GO with a Canadian number, incity 911, no renewal 20 IPP, no auto renew" do
	  @registerJack.register_with(@genGo, @fname, 'EMAIL', 'Canadian', 'BOTH', '911ADD', 'inCityYes', '1YR', 'IPP20', 'NO')

	  begin
	      @accountData.set_account_data(@fname)
	      rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
	      @rsubscriberData.set_rsubscriber_data(rsubscriberPK)
	      @cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
	      @endpointData.set_endpoint_data(@rsubscriberData.get_endpointid)

	      @locationData.set_location_data(@accountData.get_locationid)
	      @locationRecords=@locationData.set_location_records(@accountData.get_accountid)

	      @emailaddressData.set_email_address_data(@accountData.get_emailaddressid)
	      creditcardidPK = @creditcardData.get_creditcardid_pk(@accountData.get_accountid)
	      @creditcardData.set_credit_card_data(creditcardidPK)

	      orderPK = @ordersData.get_order_pk(@accountData.get_accountid)
	      @ordersData.set_orders_data(orderPK)

	      @orderDetailRecords=@orderDetaildata.set_order_detail_records(orderPK)
	      @orderStatusRecords=@orderStatusdata.set_order_status_records(orderPK)

	      @IppData.set_ipp_data(@accountData.get_accountid)

	  	 aggregate_failures "Registers a CA GO" do
	      	expect(@accountData.get_active).to eq('Y'),											"expected account to be active"
			expect(@accountData.get_customer_name).to eq(@fname + " " + @lname),				"expected customer name to be #{@fname} #{@lname}"
			expect(@accountData.get_account_type).to eq('M'), 									"expected account type to be M"  # MagicJack
			expect(@accountData.get_busres).to eq('R'), 										"expected busred flag to be R" # Residential
			expect(@accountData.get_loyal).to eq('N'),											"expected loyal flag to be N" # Not Loyal
			expect(@accountData.get_locationid).to eq @locationData.get_locationid
			expect(@accountData.get_emailaddressid).to eq @emailaddressData.get_emailaddressid

			expect(@rsubscriberData.get_accountid).to_not be_zero
           expect(@rsubscriberData.get_rsubscriberid).to eq rsubscriberPK
           expect(@rsubscriberData.get_active).to eq('Y'),										"expected rsub device to be active"
           expect(@rsubscriberData.get_accountid).to eq @accountData.get_accountid
           expect(@rsubscriberData.get_cpnid).to eq @cpnData.get_cpnid
           expect(@rsubscriberData.get_startservice).to eq(@currentDate),					"rsub device startservice date was #{@rsubscriberData.get_startservice} expected it to be #{@currentDate}"
           expect(@rsubscriberData.get_endservice).to eq(@expDate1yr),					"rsub device endservice date was #{@rsubscriberData.get_endservice} expected it to be #{@expDate1yr}"
			expect(@rsubscriberData.get_device_type).to eq(@goDeviceType),  			    "expected rsub device type to be #{@gomxDeviceType} was #{@rsubscriberData.get_device_type} instead"
			expect(@rsubscriberData.get_e911serviceplan_productcodeid).to eq('288'),        "expected rsub 911 product code id to be 288 but was #{@rsubscriberData.get_e911serviceplan_productcodeid}"
			#'288' # MJ 911 Service Plan  '318' #No 911
			expect(@rsubscriberData.get_autorenew).to eq('Y'),								"expected rsub autorenew to be 'Y' but was #{@rsubscriberData.get_autorenew} instead"
			expect(@rsubscriberData.get_service_type).to eq('B'),                           "expected rsub service type to be 'B' but was #{@rsubscriberData.get_service_type} instead"
			#expect(@rsubscriberData.get_service_type).to eq('I'), 							"expected rsub service type to be 'I' but was #{@rsubscriberData.get_service_type} instead"
			expect(@rsubscriberData.get_pastdue).to eq('N'),                                "expected rsub past due to be 'N' but was #{@rsubscriberData.get_pastdue} instead"
			#expect(@rsubscriberData.get_insurance_productcodeid).to eq(360),        "expected rsub insurance product code id to be '360' but was #{@rsubscriberDataget_insurance_productcodeid} instead"
			expect(@rsubscriberData.get_lost).to eq('N'),                                  	"expected rsub lost to be 'N' but was #{@rsubscriberData.get_lost} instead"
			expect(@rsubscriberData.get_position).to eq(1),							  	"expected rsub position to be '1' but was #{@rsubscriberData.get_position} instead"
			expect(@rsubscriberData.get_devicename).to eq(@fname),                         	"expected rsub device name to be '1' #{@fname} but was #{@rsubscriberData.get_devicename} instead"
			expect(@rsubscriberData.get_endpointid).to_not be_zero
			expect(@rsubscriberData.get_subscriptionid).not_to eq(0),                       "expected rsub subscription id to not be '0'"
			expect(@rsubscriberData.get_serviceplan_productcodeid).to eq('342'), 	"expected rsub service plan product code id to be '342' but was #{@rsubscriberData.get_serviceplan_productcodeid} instead" # magicJackGO Mexico Standard Dialing Plan
			expect(@rsubscriberData.get_renewal_lag).to eq('0'),                            "expected rsub renewal lag to be '0' but was #{@rsubscriberData.get_renewal_lag} instead"
			expect(@rsubscriberData.get_suspended).to eq('N'),                              "expected rsub subscription suspended to be 'N'"
			expect(@rsubscriberData.get_subscriptionactive).to eq('Y'),                     "expected rsub subscription active to be 'Y'"
			expect(@rsubscriberData.get_treatments).to eq('0'),                             "expected rsub treatments active to be '0' but was #{@rsubscriberData.get_treatments} instead"
			expect(@rsubscriberData.get_vmtimer).to eq(25),                                 "expected rsub vmtimer active to be 25 but was #{@rsubscriberData.get_vmtimer} instead"
			expect(@rsubscriberData.get_e911service_locationid).to eq @locationData.get_locationid
			expect(@rsubscriberData.get_e911service_billdate).to eq @gomxExpDate

			expect(@cpnData.get_cpn_type).to eq(@cpnTypeS),                                 "expected cpn type to be #{@cpnTypeS} but was #{@cpnData.get_cpn_type} instead" # Standard
			expect(@cpnData.get_accountid).to eq(@accountData.get_accountid),               "expected cpn accountid to be #{@accountData.get_accountid} but was #{@cpnData.get_accountid} instead"
			expect(@cpnData.get_calling_party_no).to_not eq(0),                             "expected cpn calling party number to not be '0'"
			expect(@cpnData.get_class_of_service).to eq('V') ,                              "expected cpn type to be 'V' but was #{@cpnData.get_class_of_service} instead" # our magicjack Number
			expect(@cpnData.get_type_of_service).to eq('3'),                                "expected cpn type of service to be 3 but was #{@cpnData.get_type_of_service} instead"
			expect(@cpnData.get_dial_tone_company_id).to eq('YMX00'), 						"expected cpn dial tone company id to be 'YMX00' but was #{@cpnData.get_dial_tone_company_id} instead"
			expect(@cpnData.get_e911_status).to eq('PQ'), 									"expected cpn e911 status to be 'PQ' but was #{@cpnData.get_e911_status} instead"
			expect(@cpnData.get_e911_tcsi_code).to eq('PICQ'),                              "expected cpn e911 tcsi code to be 'PICQ' but was #{@cpnData.get_e911_tcsi_code} instead"
			expect(@cpnData.get_e911_tcsi_date).to eq(@currentDate),                        "expected cpn 911 tcsi date was #{@cpnData.get_e911_tcsi_date} instead of #{@currentDate}"
			expect(@cpnData.get_inservice).to eq(@currentDate),                             "expected cpn in service date to be #{@cpnData.get_inservice} instead of #{@currentDate}"
			expect(@cpnData.get_outservice).to eq(@standardExpDate),                        "expected cpn out service date to be #{@cpnData.get_outservice} instead of #{@standardExpDate}"
			expect(@cpnData.get_cpn_type).to eq(@cpnTypeS),                                 "expected cpn type to be #{@cpnData.get_cpn_type} instead of #{@cpnTypeS}"
			expect(@cpnData.get_number_productcodeid).to eq(46),                        "expected cpn product code id to be '46' instead of #{@cpnData.get_number_productcodeid}" # US Phone Number - Free
			expect(@cpnData.get_emailaddressid).to eq(@emailaddressData.get_emailaddressid), "expected cpn email address id to be #{@emailaddressData.get_emailaddressid} instead of #{@cpnData.get_emailaddressid}"
			expect(@cpnData.get_state).to eq('TN'),                                         "expected cpn state to be 'TN' instead of #{@cpnData.get_state}"
			expect(@cpnData.get_country).to eq('US'),                                       "expected cpn state to be 'US' instead of #{@cpnData.get_country}"
			expect(@cpnData.get_usagebits).to eq(1),                                        "expected cpn usagebits to be '1' instead of #{@cpnData.get_usagebits}" # Number of devices with number?
			expect(@cpnData.get_options).to eq(0),                                          "expected cpn options to be '0' instead of #{@cpnData.get_options}"
			expect(@cpnData.get_autorenewdate).to eq @expExpDate
			expect(@cpnData.get_autorenew).to eq 'N'

			expect(@endpointData.get_endpointid).to eq(@rsubscriberData.get_endpointid),	 "expected endpoint id to be #{@rsubscriberData.get_endpointid} instead of #{@endpointData.get_endpointid}"
			expect(@endpointData.get_accountid).to eq(@accountData.get_accountid),			 "expected endpoint accountid to be #{@accountData.get_accountid} instead of #{@endpointData.get_accountid}"
			expect(@endpointData.get_subscriberid).to eq(@rsubscriberData.get_rsubscriberid),	 "expected endpoint rsubscriber id to be #{@rsubscriberData.get_rsubscriberid} instead of #{@endpointData.get_subscriberid}"
			expect(@endpointData.get_cpnid).to eq(@cpnData.get_cpnid),			 				 "expected endpoint cpn id to be #{@cpnData.get_cpnid} instead of #{@endpointData.get_cpnid}"
			expect(@endpointData.get_enumber).to eq('E'+@cpnData.get_calling_party_no+'01'),	"expected endpoint enumber to be #{'E'+@cpnData.get_calling_party_no+'01'} instead of #{@endpointData.get_enumber}"
			expect(@endpointData.get_rgid).to eq('900'),			 							 "expected endpoint rgid to be 900 instead of #{@endpointData.get_rgid}"
			expect(@endpointData.get_subtype).to eq('R'),			 							 "expected endpoint subtype to be R instead of #{@endpointData.get_rgid}"  # Registered
			expect(@endpointData.get_locationid).to eq(@locationData.get_locationid),			 "expected endpoint location id to be #{@locationData.get_locationid} instead of #{@endpointData.get_locationid}"
			expect(@endpointData.get_serial_no).to eq(@rsubscriberData.get_serial_no),			 "expected endpoint serial number to be #{@rsubscriberData.get_serial_no} instead of #{@endpointData.get_serial_no}"
			expect(@endpointData.get_position).to eq(1),			 			 				 "expected endpoint position to be 1 instead of #{@endpointData.get_position}"
			expect(@endpointData.get_subscriptionid).to eq(@rsubscriberData.get_subscriptionid), "expected endpoint subscription id to be #{@rsubscriberData.get_subscriptionid} instead of #{@endpointData.get_subscriptionid}"
			expect(@endpointData.get_device_type).to eq(@goDeviceType),	 					 "expected endpoint device type to be #{@goDeviceType} instead of #{@endpointData.get_device_type}" # W
			expect(@endpointData.get_serviceplan_productcodeid).to eq(342),	 	"expected endpoint product code id to be 342 instead of #{@endpointData.get_serviceplan_productcodeid}"
			expect(@endpointData.get_treatments).to eq('0'),	 								 "expected endpoint treatments to be 0 instead of #{@endpointData.get_treatments}"
			expect(@endpointData.get_cpn_type).to eq(@cpnTypeS),	 							 "expected endpoint cpn type to be #{@cpnTypeS} instead of #{@endpointData.get_cpn_type}" # S
			expect(@endpointData.get_intrado_arg).to eq('TNI'),	 							 "expected endpoint intrado arg to be TNI instead of #{@endpointData.get_intrado_arg}"
			expect(@endpointData.get_e911_color).to eq('Y'),	 								 "expected endpoint e911 color to be Y instead of #{@endpointData.get_e911_color}"
			expect(@endpointData.get_e911_description).to eq('E911 Address Processing'),	 	 "expected endpoint e911 description to be E911 Address Processing instead of #{@endpointData.get_e911_description}"
			#expect(@endpointData.get_e911_verified).to eq(@currentDate),	 				 	 "expected endpoint e911 verified to be #{@currentDate} instead of #{@endpointData.get_e911_verified}"
			expect(@endpointData.get_e911_status).to eq('1'),	 				 				 "expected endpoint e911 status to be 2 instead of #{@endpointData.get_e911_status}"

			expect(@locationRecords.to_s).to include('S'),	 				 				 	 "expected location location type to be S." # Shipping
			expect(@locationRecords.to_s).to include(@accountData.get_accountid.to_s),	 		 "expected location account id to be #{@accountData.get_accountid}"
			expect(@locationRecords.to_s).to include(@fname),	 				 			 	 "expected location first name to be #{@fname}"
			expect(@locationRecords.to_s).to include(@lname),	 				 			 	 "expected location last name to be #{@lname}"
			expect(@locationRecords.to_s).to include(@fname + ' ' + @lname),	 				 "expected location dba to be #{@fname + ' ' + @lname}"
			expect(@locationRecords.to_s).to include('470'),	 	 "expected location house no to be 470 instead of #{@locationData.get_house_no}"
			expect(@locationRecords.to_s).to include('GLENRIDGE'),	         	 "expected location street name to be GLENRIDGE" #currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('AVE'),	 "expected location street name suffix to be AVE instead of #{@locationData.get_street_name_suffix}"
			#expect(@locationRecords.to_s).to include('NASHVILLE'),	 				 		 "expected location location to be NUEVO OBISPADO" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('ST CATHARINES'),	 				 			 "expected location msag community name to be ST CATHARINES" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('ON'),	 				 			 		 "expected location state to be ON" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('L2T4C3'),	 				 			 	 "expected location zipcode to be L2T4C3" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('Y'),	 				 			 		 "expected location active to be Y"
			expect(@locationRecords.to_s).to include('CA'),	 				 			 	     "expected location country to be CA" # currently hard coded in reg.rb
			#expect(@locationRecords.to_s).to include(@cpnData.get_cpnid.to_s),	 	 			 "expected location cpn id to be #{@cpnData.get_cpnid}" # instead of #{@locationData.get_phone}"
			expect(@locationData.get_geocode).to eq('4703752000'), "expected location geocode to be 4703752000 instead of #{@locationData.get_geocode}" # prepopuplated in db?  more test needed on geocode
			expect(@locationRecords.to_s).to include('I'),	 				 			 	     "expected location incity to be U" # Incity parameter passed into this test
			expect(@locationRecords.to_s).to include('O'),	 				 			 	     "expected location inlocal to be O" # default value

			expect(@emailaddressData.get_email_type).to eq('M'),	 				 			 "expected email email type to be M instead of #{@emailaddressData.get_email_type}"
			expect(@emailaddressData.get_accountid).to eq(@accountData.get_accountid),	 	 "expected email accountid to be #{@accountData.get_accountid} instead of #{@emailaddressData.get_accountid}"
			expect(@emailaddressData.get_emailaddr).to eq(@email),	 				 "expected email email address  to be #{@email} instead of #{@emailaddressData.get_emailaddr}" # defined within reg.rb
			expect(@emailaddressData.get_emailkey).to eq(@email),	 				 "expected email email key  to be #{@email} instead of #{@emailaddressData.get_emailkey}" # defined within reg.rb
			expect(@emailaddressData.get_password).to eq('VWVWVW12'),	 			 "expected email password  to be VWVWVW12 instead of #{@emailaddressData.get_password}" # defined within reg.rb
			expect(@emailaddressData.get_marketing).to eq('9'),	 	"expected email get marketing emails to be 9 instead of #{@emailaddressData.get_marketing}" # customer will receive marketing emails

			expect(@creditcardData.get_accountid).to eq(@accountData.get_accountid),	 "expected creditcard accountid to be #{@accountData.get_accountid} instead of #{@creditcardData.get_accountid}"
			expect(@creditcardData.get_full_name).to eq(@fname + '  ' +@lname),  	 		 "expected creditcard full name to be #{@fname + '  ' +@lname} instead of #{@creditcardData.get_full_name}"
			expect(@creditcardData.get_first_name).to eq(@fname),  	 				 	 	 "expected creditcard first name to be #{@fname} instead of #{@creditcardData.get_first_name}"
			expect(@creditcardData.get_last_name).to eq(@lname),  	 				 	 	 "expected creditcard last name to be #{@lname} instead of #{@creditcardData.get_last_name}"
			expect(@creditcardData.get_card_type).to eq('V'),  	 				 	 	     "expected creditcard last name to be V instead of #{@creditcardData.get_cardnumber}" # from reg.rb
			expect(@creditcardData.get_cardnumber).to_not eq('0'),  	 				 	 "expected creditcard card number to not be 0 instead of #{@creditcardData.get_cardnumber}"
			expect(@creditcardData.get_exp_date).to eq('042019'),  	 				 	 "expected creditcard expiration date to not be 122019 instead of #{@creditcardData.get_exp_date}" # set in reg.rb
			expect(@creditcardData.get_active).to eq('Y'),  	 				 	 	    "expected creditcard active to be Y instead of #{@creditcardData.get_active}" # set in reg.rb
			expect(@creditcardData.get_fraud_override).to eq('N'),  	 				 	 "expected creditcard active to be N instead of #{@creditcardData.get_fraud_override}"
			expect(@creditcardData.get_cvv2_match).to eq('Y'),  	 				 	     "expected creditcard cvv2 match to be Y instead of #{@creditcardData.get_cvv2_match}"
			expect(@creditcardData.get_processor_avs).to eq('F'),  	 				 	    "expected creditcard processor avs to be F instead of #{@creditcardData.get_processor_avs}"
			expect(@creditcardData.get_addr_match).to eq('-'),  	 					 "expected creditcard address match to be - instead of #{@creditcardData.get_addr_match}"
			expect(@creditcardData.get_zip_match).to eq('-'),  	 				 	     "expected creditcard zip match to be - instead of #{@creditcardData.get_zip_match}"
			expect(@creditcardData.get_debit).to eq('N'),  	 				 	 	     "expected creditcard debit to be N instead of #{@creditcardData.get_debit}"
			expect(@creditcardData.get_locationid).to eq(@accountData.get_locationid), "expected creditcard location id to be #{@accountData.get_locationid} instead of #{@creditcardData.get_locationid}"
			expect(@creditcardData.get_first_charge_date).to eq(@currentDate),  	     "expected creditcard first charge date to be #{@currentDate} instead of #{@creditcardData.get_first_charge_date}"
			expect(@creditcardData.get_charge_count).to eq('1'),  	 				  "expected creditcard charge count to be 1 instead of #{@creditcardData.get_charge_count}"
			expect(@creditcardData.get_use_for_recurring).to eq('D'),  	 				 "expected creditcard use for recurring to be D instead of #{@creditcardData.get_use_for_recurring}"
			expect(@creditcardData.get_istoken).to eq('Y'),  	 				         "expected creditcard it token to be Y instead of #{@creditcardData.get_istoken}"
			expect(@creditcardData.get_mop).to eq('VI'),  	 				         	 "expected creditcard mop to be VI instead of #{@creditcardData.get_mop}"

			expect(@ordersData.get_orderid).to_not be_zero
			expect(@ordersData.get_inbound_orderid).to eq('0'),                                  "expected order in bound orderid to be '0' instead of #{@ordersData.get_inbound_orderid}"
			expect(@ordersData.get_accountid).to eq(@accountData.get_accountid),                 "expected order account id to be #{@accountData.get_accountid} instead of #{@ordersData.get_accountid}"
			expect(@ordersData.get_emailaddressid).to eq(@emailaddressData.get_emailaddressid),  "expected order email addressid to be #{@emailaddressData.get_emailaddressid} instead of #{@ordersData.get_emailaddressid}"
			expect(@ordersData.get_creditcardid).to eq(@creditcardData.get_creditcardid),       "expected order credit card id to be #{@creditcardData.get_creditcardid} instead of #{@ordersData.get_creditcardid}"
			expect(@ordersData.get_order_number).to_not eq('0' ),    							 "expected order order number to not be 0 instead of #{@ordersData.get_order_number}"
			expect(@ordersData.get_order_date).to eq(@currentDate),    							 "expected order date to be #{@currentDate} instead of #{@ordersData.get_order_date}"
			expect(@ordersData.get_order_time).to_not eq(''),    								 "expected order time id to not be blank instead of #{@ordersData.get_order_time}"
			expect(@ordersData.get_order_status).to eq('N'),									 "expected order order status to be 'N' instead of #{@ordersData.get_order_status}"
			expect(@ordersData.get_statusdate).to eq(@currentDate),    							 "expected order date to be #{@currentDate} instead of #{@ordersData.get_statusdate}"
			expect(@ordersData.get_subtotal).to eq('55'),    									 "expected order subtotal to be 10 instead of #{@ordersData.get_subtotal}"
			expect(@ordersData.get_shipping).to eq('0'),    									 "expected order shipping to be 0 instead of #{@ordersData.get_shipping}"
			expect(@ordersData.get_taxes).to eq('4.29'),    										 "expected order taxes to be 4.29 instead of #{@ordersData.get_taxes}"
			expect(@ordersData.get_total).to eq('59.29'),                            				 "expected order total to be '59.29' instead of #{@ordersData.get_total}"
			expect(@ordersData.get_taxrateid).to eq('1012'),                                     "expected order tax rate id to be '1012' instead of #{@ordersData.get_taxrateid}"
			expect(@ordersData.get_taxrate).to eq('.078'),                                       "expected order tax rate to be '0.78' instead of #{@ordersData.get_taxrate}"
			expect(@ordersData.get_adj_subtotal).to eq('0'),                                     "expected order adj subtotal to be 0 instead of #{@ordersData.get_adj_subtotal}"
			expect(@ordersData.get_adj_taxes).to eq('0'),                                        "expected order adj taxes to be 0 instead of #{@ordersData.get_adj_taxes}"
			expect(@ordersData.get_adj_shipping).to eq('0'),                                     "expected order adj shipping to be 0 instead of #{@ordersData.get_adj_shipping}"
			expect(@ordersData.get_adj_total).to eq('0'),                                        "expected order adj total to be 0 instead of #{@ordersData.get_adj_total}"
			expect(@ordersData.get_adj_sum_total).to eq('59.29'),                                    "expected order adj sum total to be 59.29 instead of #{@ordersData.get_adj_sum_total}"
			expect(@ordersData.get_unpaid).to eq('N'),                                           "expected order unpaid to be 0 instead of #{@ordersData.get_unpaid}"
			expect(@ordersData.get_payment).to eq('N'),                                          "expected order payment to be N instead of #{@ordersData.get_payment}"
			expect(@ordersData.get_result_code).to eq('0'),                                      "expected order result code to be 0 instead of #{@ordersData.get_result_code}"
			expect(@ordersData.get_last_auth_attempt).to eq(@currentDate),                      "expected order last auth attempt to be #{@currentDate} instead of #{@ordersData.get_last_auth_attempt}"
			expect(@ordersData.get_auth_attempts).to eq('1'),                         			 "expected order auth attempts to be 1 instead of #{@ordersData.get_auth_attempts}"
			expect(@ordersData.get_auth_total).to eq(59.29),                         		 "expected order auth total to be 59.29 instead of #{@ordersData.get_auth_total}"
			expect(@ordersData.get_authmessage).to eq('Approved'),                              "expected order auth message id to be 'Approved' instead of #{@ordersData.get_authmessage}"
			expect(@ordersData.get_sourceind).to eq('W'),                                        "expected order sourceind id to be 'W' instead of #{@ordersData.get_sourceind}"
			expect(@ordersData.get_nflags).to eq(0),                                             "expected order nflags id to be '0' instead of #{@ordersData.get_nflags}"
			expect(@ordersData.get_campaignid).to eq('1'),                                       "expected order campaign id to be '1' instead of #{@ordersData.get_campaignid}"
			expect(@ordersData.get_cancelreason).to eq('0'),                                     "expected order cancel to be '0' instead of #{@ordersData.get_cancelreason}"

			expect(@orderDetailRecords.to_s).to include("CANADIANNUMBER1000"),               			"expected order detail to contain CANADIANNUMBER1000"
			expect(@orderDetailRecords.to_s).to include("MJGOSTDDIALPLAN"),   			  	"expected order detail to contain MJGOSTDDIALPLAN"
			expect(@orderDetailRecords.to_s).to include("FREE911CASERVICEPLAN"),     			"expected order detail to contain FREE911CASERVICEPLAN"
			expect(@orderDetailRecords.to_s).to include("NOMJGOREPLACEMENT"),     			"expected order detail to contain NOMJGOREPLACEMENT"
			expect(@orderDetailRecords.to_s).to include("49"),                       			"expected order detail to contain 49" #product code for CANADIANNUMBER1000
			expect(@orderDetailRecords.to_s).to include("359"),                      			"expected order detail to contain 581" #product code for MJGOSTDDIALPLAN
			expect(@orderDetailRecords.to_s).to include("286"),                      			"expected order detail to contain 286" #product code for FREE911CASERVICEPLAN
			expect(@orderDetailRecords.to_s).to include("363"),                      			"expected order detail to contain 363" #product code for NOMJGOREPLACEMENT
			expect(@orderDetailRecords.to_s).to include("1"),                        			"expected order detail to contain 1" #quantity, pieces,
			expect(@orderDetailRecords.to_s).to include("I"),                        			"expected order detail to contain I" #detail_type
			expect(@orderDetailRecords.to_s).to include("N"),                        			"expected order detail to contain N" #paid
			expect(@orderDetailRecords.to_s).to include("IPPMIN2000"),                        	"expected order detail to contain IPPMIN2000" #IPP
			expect(@orderDetailRecords.to_s).to include("28"),                        			"expected order detail to contain 28" #IPPMIN2000
			expect(@orderDetailRecords.to_s).to include(@cpnData.get_cpnid.to_s),		 		"expected order detail to contain cpnid" # cpnid for CANADIANNUMBER1000
			expect(@orderDetailRecords.to_s).to include(@locationData.get_locationid.to_s),		"expected order detail to contain locationid" # locationid for FREE911CASERVICEPLAN

			expect(@orderStatusRecords.to_s).to include("N"),                      	   			"expected order status to contain [N]ewPending" # New Pending status
			expect(@orderStatusRecords.to_s).to include(@ordersData.get_order_number), 			"expected order status description to contain [N]ewPending with order number"
	  end

    rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
       puts e.message
       puts @fname
    end
  end

  it "Registers an CA GO with a Canadian number, incity 911, no renewal 10 IPP, no auto renew" do
	  @registerJack.register_with(@genGo, @fname, 'EMAIL', 'Canadian', 'BOTH', '911ADD', 'inCityYes', '5YR', 'IPP10', 'NO')

	  begin
	      @accountData.set_account_data(@fname)
	      rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
	      @rsubscriberData.set_rsubscriber_data(rsubscriberPK)
	      @cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
	      @endpointData.set_endpoint_data(@rsubscriberData.get_endpointid)

	      #@locationData.set_location_data(@accountData.get_locationid)
	      @locationData.set_location_data(@rsubscriberData.get_e911service_locationid)
	      @locationRecords=@locationData.set_location_records(@accountData.get_accountid)
#puts @locationRecords
	      @emailaddressData.set_email_address_data(@accountData.get_emailaddressid)
	      creditcardidPK = @creditcardData.get_creditcardid_pk(@accountData.get_accountid)
	      @creditcardData.set_credit_card_data(creditcardidPK)

	      orderPK = @ordersData.get_order_pk(@accountData.get_accountid)
	      @ordersData.set_orders_data(orderPK)

	      @orderDetailRecords=@orderDetaildata.set_order_detail_records(orderPK)
	      @orderStatusRecords=@orderStatusdata.set_order_status_records(orderPK)

	      @IppData.set_ipp_data(@accountData.get_accountid)

	  	 aggregate_failures "Registers a CA GO" do
			expect(@locationRecords.to_s).to include('S'),	 				 				 	 "expected location location type to be S." # Shipping
			expect(@locationRecords.to_s).to include(@accountData.get_accountid.to_s),	 		 "expected location account id to be #{@accountData.get_accountid}"
			expect(@locationRecords.to_s).to include(@fname),	 				 			 	 "expected location first name to be #{@fname}"
			expect(@locationRecords.to_s).to include(@lname),	 				 			 	 "expected location last name to be #{@lname}"
			expect(@locationRecords.to_s).to include(@fname + ' ' + @lname),	 				 "expected location dba to be #{@fname + ' ' + @lname}"
			expect(@locationRecords.to_s).to include('470'),	 	 "expected location house no to be 470 instead of #{@locationData.get_house_no}"
			expect(@locationRecords.to_s).to include('GLENRIDGE'),	         	 "expected location street name to be GLENRIDGE" #currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('AVE'),	 "expected location street name suffix to be AVE instead of #{@locationData.get_street_name_suffix}"
			#expect(@locationRecords.to_s).to include('NASHVILLE'),	 				 		 "expected location location to be NUEVO OBISPADO" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('ST CATHARINES'),	 				 			 "expected location msag community name to be ST CATHARINES" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('ON'),	 				 			 		 "expected location state to be ON" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('L2T4C3'),	 				 			 	 "expected location zipcode to be L2T4C3" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('Y'),	 				 			 		 "expected location active to be Y"
			expect(@locationRecords.to_s).to include('CA'),	 				 			 	     "expected location country to be CA" # currently hard coded in reg.rb
			#expect(@locationRecords.to_s).to include(@cpnData.get_cpnid.to_s),	 	 			 "expected location cpn id to be #{@cpnData.get_cpnid}" # instead of #{@locationData.get_phone}"
			expect(@locationData.get_geocode).to eq('4703752000'), "expected location geocode to be 4703752000 instead of #{@locationData.get_geocode}" # prepopuplated in db?  more test needed on geocode
		  	#911 Address
			expect(@locationData.get_house_no).to eq('3880'),	 	 "expected location house no to be 3880 instead of #{@locationData.get_house_no}"
			expect(@locationRecords.to_s).to include('PRIEST LAKE'),	         	             "expected 911 location street name to be PRIEST LAKE" #currently hard coded in reg.rb
			expect(@locationData.get_street_name_suffix).to eq('DR'),	 "expected location street name suffix to be DR instead of #{@locationData.get_street_name_suffix}"
			expect(@locationRecords.to_s).to include('APT 96'),	 				 		 		 "expected 911 location location to be APT 96" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('NASHVILLE'),	 				 			 "expected 911 location msag community name to be NASHVILLE" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('TN'),	 				 			 		 "expected 911 location state to be TN" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('37210'),	 				 			 	 "expected 911 location zipcode to be 37210" # currently hard coded in reg.rb
			expect(@locationData.get_active).to eq('Y'),	 				 			 		 "expected location active to be Y instead of #{@locationData.get_active}"
			expect(@locationData.get_phone).to eq(@cpnData.get_calling_party_no),	 			 "expected location calling party no to be #{@cpnData.get_calling_party_no} instead of #{@locationData.get_phone}"
			#expect(@locationRecords.to_s).to include(@cpnData.get_cpnid.to_s),	 	 			 "expected location cpn id to be #{@cpnData.get_cpnid}" # instead of #{@locationData.get_phone}"
			expect(@locationData.get_geocode).to eq('4703752000'), "expected location geocode to be 4703752000 instead of #{@locationData.get_geocode}" # prepopuplated in db?  more test needed on geocode

			expect(@orderDetailRecords.to_s).to include("IPPMIN1000"),                        	"expected order detail to contain IPPMIN1000" #IPP
			expect(@orderDetailRecords.to_s).to include("26"),                        			"expected order detail to contain 26" #IPPMIN2000

			expect(@locationRecords.to_s).to include('I'),	 				 			 	     "expected location incity to be U" # Incity parameter passed into this test
			expect(@locationRecords.to_s).to include('O'),	 				 			 	     "expected location inlocal to be O" # default value
	  end

    rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
       puts e.message
       puts @fname

    end
  end

     it "Registers an CA GO with a Canadian number, incity 911, no renewal 40 IPP, no auto renew" do
	  @registerJack.register_with(@genGo, @fname, 'EMAIL', 'Canadian', 'BOTH', '911ADD', 'inCityYes', '1YR', 'IPP40', 'NO')

	  begin
	      @accountData.set_account_data(@fname)
	      rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
	      @rsubscriberData.set_rsubscriber_data(rsubscriberPK)
	      @cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
	      @endpointData.set_endpoint_data(@rsubscriberData.get_endpointid)

	      #@locationData.set_location_data(@accountData.get_locationid)
	      @locationData.set_location_data(@rsubscriberData.get_e911service_locationid)
	      @locationRecords=@locationData.set_location_records(@accountData.get_accountid)
#puts @locationRecords
	      @emailaddressData.set_email_address_data(@accountData.get_emailaddressid)
	      creditcardidPK = @creditcardData.get_creditcardid_pk(@accountData.get_accountid)
	      @creditcardData.set_credit_card_data(creditcardidPK)

	      orderPK = @ordersData.get_order_pk(@accountData.get_accountid)
	      @ordersData.set_orders_data(orderPK)

	      @orderDetailRecords=@orderDetaildata.set_order_detail_records(orderPK)
	      @orderStatusRecords=@orderStatusdata.set_order_status_records(orderPK)

	      @IppData.set_ipp_data(@accountData.get_accountid)

	  	 aggregate_failures "Registers a GO" do
			expect(@locationRecords.to_s).to include('S'),	 				 				 	 "expected location location type to be S." # Shipping
			expect(@locationRecords.to_s).to include(@accountData.get_accountid.to_s),	 		 "expected location account id to be #{@accountData.get_accountid}"
			expect(@locationRecords.to_s).to include(@fname),	 				 			 	 "expected location first name to be #{@fname}"
			expect(@locationRecords.to_s).to include(@lname),	 				 			 	 "expected location last name to be #{@lname}"
			expect(@locationRecords.to_s).to include(@fname + ' ' + @lname),	 				 "expected location dba to be #{@fname + ' ' + @lname}"
			expect(@locationRecords.to_s).to include('470'),	 	 "expected location house no to be 470 instead of #{@locationData.get_house_no}"
			expect(@locationRecords.to_s).to include('GLENRIDGE'),	         	 "expected location street name to be GLENRIDGE" #currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('AVE'),	 "expected location street name suffix to be AVE instead of #{@locationData.get_street_name_suffix}"
			#expect(@locationRecords.to_s).to include('NASHVILLE'),	 				 		 "expected location location to be NUEVO OBISPADO" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('ST CATHARINES'),	 				 			 "expected location msag community name to be ST CATHARINES" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('ON'),	 				 			 		 "expected location state to be ON" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('L2T4C3'),	 				 			 	 "expected location zipcode to be L2T4C3" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('Y'),	 				 			 		 "expected location active to be Y"
			expect(@locationRecords.to_s).to include('CA'),	 				 			 	     "expected location country to be CA" # currently hard coded in reg.rb
			#expect(@locationRecords.to_s).to include(@cpnData.get_cpnid.to_s),	 	 			 "expected location cpn id to be #{@cpnData.get_cpnid}" # instead of #{@locationData.get_phone}"
			expect(@locationData.get_geocode).to eq('4703752000'), "expected location geocode to be 4703752000 instead of #{@locationData.get_geocode}" # prepopuplated in db?  more test needed on geocode
		  	#911 Address
			expect(@locationData.get_house_no).to eq('3880'),	 	 "expected location house no to be 3880 instead of #{@locationData.get_house_no}"
			expect(@locationRecords.to_s).to include('PRIEST LAKE'),	         	             "expected 911 location street name to be PRIEST LAKE" #currently hard coded in reg.rb
			expect(@locationData.get_street_name_suffix).to eq('DR'),	 "expected location street name suffix to be DR instead of #{@locationData.get_street_name_suffix}"
			expect(@locationRecords.to_s).to include('APT 96'),	 				 		 		 "expected 911 location location to be APT 96" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('NASHVILLE'),	 				 			 "expected 911 location msag community name to be NASHVILLE" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('TN'),	 				 			 		 "expected 911 location state to be TN" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('37210'),	 				 			 	 "expected 911 location zipcode to be 37210" # currently hard coded in reg.rb
			expect(@locationData.get_active).to eq('Y'),	 				 			 		 "expected location active to be Y instead of #{@locationData.get_active}"
			expect(@locationData.get_phone).to eq(@cpnData.get_calling_party_no),	 			 "expected location calling party no to be #{@cpnData.get_calling_party_no} instead of #{@locationData.get_phone}"
			#expect(@locationRecords.to_s).to include(@cpnData.get_cpnid.to_s),	 	 			 "expected location cpn id to be #{@cpnData.get_cpnid}" # instead of #{@locationData.get_phone}"
			expect(@locationData.get_geocode).to eq('4703752000'), "expected location geocode to be 4703752000 instead of #{@locationData.get_geocode}" # prepopuplated in db?  more test needed on geocode
			expect(@orderDetailRecords.to_s).to include("IPPMIN4000"),                        	"expected order detail to contain IPPMIN4000" #IPP
			expect(@orderDetailRecords.to_s).to include("38"),                        			"expected order detail to contain 38" #IPPMIN2000

			expect(@locationRecords.to_s).to include('I'),	 				 			 	     "expected location incity to be U" # Incity parameter passed into this test
			expect(@locationRecords.to_s).to include('O'),	 				 			 	     "expected location inlocal to be O" # default value
	  end

    rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
       puts e.message
       puts @fname

    end
  end

   it "Registers an CA GO with a Canadian number, incity 911, no renewal 5 IPP, no auto renew" do
	  @registerJack.register_with(@genGo, @fname, 'EMAIL', 'Canadian', 'BOTH', '911ADD', 'inCityYes', '1YR', 'IPP5', 'NO')

	  begin
	      @accountData.set_account_data(@fname)
	      rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
	      @rsubscriberData.set_rsubscriber_data(rsubscriberPK)
	      @cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
	      @endpointData.set_endpoint_data(@rsubscriberData.get_endpointid)

	      #@locationData.set_location_data(@accountData.get_locationid)
	      @locationData.set_location_data(@rsubscriberData.get_e911service_locationid)
	      @locationRecords=@locationData.set_location_records(@accountData.get_accountid)
#puts @locationRecords
	      @emailaddressData.set_email_address_data(@accountData.get_emailaddressid)
	      creditcardidPK = @creditcardData.get_creditcardid_pk(@accountData.get_accountid)
	      @creditcardData.set_credit_card_data(creditcardidPK)

	      orderPK = @ordersData.get_order_pk(@accountData.get_accountid)
	      @ordersData.set_orders_data(orderPK)

	      @orderDetailRecords=@orderDetaildata.set_order_detail_records(orderPK)
	      @orderStatusRecords=@orderStatusdata.set_order_status_records(orderPK)

	      @IppData.set_ipp_data(@accountData.get_accountid)

	  	 aggregate_failures "Registers a GO" do
			expect(@locationRecords.to_s).to include('S'),	 				 				 	 "expected location location type to be S." # Shipping
			expect(@locationRecords.to_s).to include(@accountData.get_accountid.to_s),	 		 "expected location account id to be #{@accountData.get_accountid}"
			expect(@locationRecords.to_s).to include(@fname),	 				 			 	 "expected location first name to be #{@fname}"
			expect(@locationRecords.to_s).to include(@lname),	 				 			 	 "expected location last name to be #{@lname}"
			expect(@locationRecords.to_s).to include(@fname + ' ' + @lname),	 				 "expected location dba to be #{@fname + ' ' + @lname}"
			expect(@locationRecords.to_s).to include('470'),	 	 "expected location house no to be 470 instead of #{@locationData.get_house_no}"
			expect(@locationRecords.to_s).to include('GLENRIDGE'),	         	 "expected location street name to be GLENRIDGE" #currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('AVE'),	 "expected location street name suffix to be AVE instead of #{@locationData.get_street_name_suffix}"
			#expect(@locationRecords.to_s).to include('NASHVILLE'),	 				 		 "expected location location to be NUEVO OBISPADO" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('ST CATHARINES'),	 				 			 "expected location msag community name to be ST CATHARINES" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('ON'),	 				 			 		 "expected location state to be ON" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('L2T4C3'),	 				 			 	 "expected location zipcode to be L2T4C3" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('Y'),	 				 			 		 "expected location active to be Y"
			expect(@locationRecords.to_s).to include('CA'),	 				 			 	     "expected location country to be CA" # currently hard coded in reg.rb
			#expect(@locationRecords.to_s).to include(@cpnData.get_cpnid.to_s),	 	 			 "expected location cpn id to be #{@cpnData.get_cpnid}" # instead of #{@locationData.get_phone}"
			expect(@locationData.get_geocode).to eq('4703752000'), "expected location geocode to be 4703752000 instead of #{@locationData.get_geocode}" # prepopuplated in db?  more test needed on geocode
		  	#911 Address
			expect(@locationData.get_house_no).to eq('3880'),	 	 "expected location house no to be 3880 instead of #{@locationData.get_house_no}"
			expect(@locationRecords.to_s).to include('PRIEST LAKE'),	         	             "expected 911 location street name to be PRIEST LAKE" #currently hard coded in reg.rb
			expect(@locationData.get_street_name_suffix).to eq('DR'),	 "expected location street name suffix to be DR instead of #{@locationData.get_street_name_suffix}"
			expect(@locationRecords.to_s).to include('APT 96'),	 				 		 		 "expected 911 location location to be APT 96" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('NASHVILLE'),	 				 			 "expected 911 location msag community name to be NASHVILLE" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('TN'),	 				 			 		 "expected 911 location state to be TN" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('37210'),	 				 			 	 "expected 911 location zipcode to be 37210" # currently hard coded in reg.rb
			expect(@locationData.get_active).to eq('Y'),	 				 			 		 "expected location active to be Y instead of #{@locationData.get_active}"
			expect(@locationData.get_phone).to eq(@cpnData.get_calling_party_no),	 			 "expected location calling party no to be #{@cpnData.get_calling_party_no} instead of #{@locationData.get_phone}"
			#expect(@locationRecords.to_s).to include(@cpnData.get_cpnid.to_s),	 	 			 "expected location cpn id to be #{@cpnData.get_cpnid}" # instead of #{@locationData.get_phone}"
			expect(@locationData.get_geocode).to eq('4703752000'), "expected location geocode to be 4703752000 instead of #{@locationData.get_geocode}" # prepopuplated in db?  more test needed on geocode

			expect(@orderDetailRecords.to_s).to include("IPPMIN500"),                        	"expected order detail to contain IPPMIN500" #IPP
			expect(@orderDetailRecords.to_s).to include("25"),                        			"expected order detail to contain 25" #IPPMIN2000

			expect(@locationRecords.to_s).to include('I'),	 				 			 	     "expected location incity to be U" # Incity parameter passed into this test
			expect(@locationRecords.to_s).to include('O'),	 				 			 	     "expected location inlocal to be O" # default value
	  end

    rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
       puts e.message
       puts @fname

    end
  end

    it "Registers an CA GO with a Canadian number, incity 911, no renewal 5 IPP, no auto renew, new bill" do
	  @registerJack.register_with(@genGo, @fname, 'EMAIL', 'Canadian', 'BOTH', '911ADD', 'inCityYes', '1YR', 'IPP5', 'YES')

	  begin
	      @accountData.set_account_data(@fname)
	      rsubscriberPK = @rsubscriberData.get_rsubscriber_pk(@accountData.get_accountid)
	      @rsubscriberData.set_rsubscriber_data(rsubscriberPK)
	      @cpnData.set_cpn_data(@rsubscriberData.get_cpnid)
	      @endpointData.set_endpoint_data(@rsubscriberData.get_endpointid)

	      #@locationData.set_location_data(@accountData.get_locationid)
	      @locationData.set_location_data(@rsubscriberData.get_e911service_locationid)
	      @locationRecords=@locationData.set_location_records(@accountData.get_accountid)
puts @locationRecords
	      @emailaddressData.set_email_address_data(@accountData.get_emailaddressid)
	      creditcardidPK = @creditcardData.get_creditcardid_pk(@accountData.get_accountid)
	      @creditcardData.set_credit_card_data(creditcardidPK)

	      orderPK = @ordersData.get_order_pk(@accountData.get_accountid)
	      @ordersData.set_orders_data(orderPK)

	      @orderDetailRecords=@orderDetaildata.set_order_detail_records(orderPK)
	      @orderStatusRecords=@orderStatusdata.set_order_status_records(orderPK)

	      @IppData.set_ipp_data(@accountData.get_accountid)

	  	 aggregate_failures "Registers a CA GO" do
			expect(@locationRecords.to_s).to include('S'),	 				 				 	 "expected location location type to be S." # Shipping
			expect(@locationRecords.to_s).to include(@accountData.get_accountid.to_s),	 		 "expected location account id to be #{@accountData.get_accountid}"
			expect(@locationRecords.to_s).to include(@fname),	 				 			 	 "expected location first name to be #{@fname}"
			expect(@locationRecords.to_s).to include(@lname),	 				 			 	 "expected location last name to be #{@lname}"
			expect(@locationRecords.to_s).to include(@fname + ' ' + @lname),	 				 "expected location dba to be #{@fname + ' ' + @lname}"
			expect(@locationRecords.to_s).to include('470'),	 	 "expected location house no to be 470 instead of #{@locationData.get_house_no}"
			expect(@locationRecords.to_s).to include('GLENRIDGE'),	         	 "expected location street name to be GLENRIDGE" #currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('AVE'),	 "expected location street name suffix to be AVE instead of #{@locationData.get_street_name_suffix}"
			#expect(@locationRecords.to_s).to include('NASHVILLE'),	 				 		 "expected location location to be NUEVO OBISPADO" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('ST CATHARINES'),	 				 			 "expected location msag community name to be ST CATHARINES" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('ON'),	 				 			 		 "expected location state to be ON" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('L2T4C3'),	 				 			 	 "expected location zipcode to be L2T4C3" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('Y'),	 				 			 		 "expected location active to be Y"
			expect(@locationRecords.to_s).to include('CA'),	 				 			 	     "expected location country to be CA" # currently hard coded in reg.rb
			#expect(@locationRecords.to_s).to include(@cpnData.get_cpnid.to_s),	 	 			 "expected location cpn id to be #{@cpnData.get_cpnid}" # instead of #{@locationData.get_phone}"
			expect(@locationData.get_geocode).to eq('4703752000'), "expected location geocode to be 4703752000 instead of #{@locationData.get_geocode}" # prepopuplated in db?  more test needed on geocode
		  	#911 Address
			expect(@locationData.get_house_no).to eq('3880'),	 	 "expected location house no to be 3880 instead of #{@locationData.get_house_no}"
			expect(@locationRecords.to_s).to include('PRIEST LAKE'),	         	             "expected 911 location street name to be PRIEST LAKE" #currently hard coded in reg.rb
			expect(@locationData.get_street_name_suffix).to eq('DR'),	 "expected location street name suffix to be DR instead of #{@locationData.get_street_name_suffix}"
			expect(@locationRecords.to_s).to include('APT 96'),	 				 		 		 "expected 911 location location to be APT 96" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('NASHVILLE'),	 				 			 "expected 911 location msag community name to be NASHVILLE" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('TN'),	 				 			 		 "expected 911 location state to be TN" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('37210'),	 				 			 	 "expected 911 location zipcode to be 37210" # currently hard coded in reg.rb
			expect(@locationData.get_active).to eq('Y'),	 				 			 		 "expected location active to be Y instead of #{@locationData.get_active}"
			expect(@locationData.get_phone).to eq(@cpnData.get_calling_party_no),	 			 "expected location calling party no to be #{@cpnData.get_calling_party_no} instead of #{@locationData.get_phone}"
			#expect(@locationRecords.to_s).to include(@cpnData.get_cpnid.to_s),	 	 			 "expected location cpn id to be #{@cpnData.get_cpnid}" # instead of #{@locationData.get_phone}"
			expect(@locationData.get_geocode).to eq('4703752000'), "expected location geocode to be 4703752000 instead of #{@locationData.get_geocode}" # prepopuplated in db?  more test needed on geocode
			#Bill Location
			expect(@locationRecords.to_s).to include('TestBill'),	 				 			 	 "expected location first name to be #{@fname}"
			expect(@locationRecords.to_s).to include('TestLoc'),	 				 			 	 "expected location last name to be #{@lname}"
			expect(@locationRecords.to_s).to include('318'),	 	 							 "expected location house to be 3880"
			expect(@locationRecords.to_s).to include('SEABOARD'),	         	 "expected location street name to be 2ND" #currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('LN'),	 									 "expected location street name suffix to be AVE"
			expect(@locationRecords.to_s).to include('DR'),	 "expected location street name suffix to be DR instead of #{@locationData.get_street_name_suffix}"
			#expect(@locationRecords.to_s).to include('LOCATION'),	 				 		 "expected location location to be NUEVO OBISPADO" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('FRANKLIN'),	 				 			 "expected location msag community name to be NASHVILLE" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('TN'),	 				 			 		 "expected location state to be TN" # currently hard coded in reg.rb
			expect(@locationRecords.to_s).to include('37067'),	 				 			 	 "expected location zipcode to be 37210" # currently hard coded in reg.rb
			expect(@orderDetailRecords.to_s).to include("IPPMIN500"),                        	"expected order detail to contain IPPMIN500" #IPP
			expect(@orderDetailRecords.to_s).to include("25"),                        			"expected order detail to contain 25" #IPPMIN2000

			expect(@locationRecords.to_s).to include('I'),	 				 			 	     "expected location incity to be U" # Incity parameter passed into this test
			expect(@locationRecords.to_s).to include('O'),	 				 			 	     "expected location inlocal to be O" # default value
	  end

     rescue RSpec::Expectations::MultipleExpectationsNotMetError => e
        puts e.message
     end
   end
end
