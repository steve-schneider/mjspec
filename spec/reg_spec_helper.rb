require 'selenium-webdriver'


RSpec.configure do |config|
  # ...
  public
   
  config.before(:each) do 
		

		default_profile = Selenium::WebDriver::Firefox::Profile.from_name "default"
		default_profile.native_events = true
		client = Selenium::WebDriver::Remote::Http::Default.new
		client.timeout = 240 # seconds	
		@driver = Selenium::WebDriver.for(:firefox, :profile => default_profile, :http_client => client)    
		@registerJack = Reg.new(@driver)
			
	  d = DateTime.now
	  @fname = "AUT-"+d.strftime('%F')+"-"+d.strftime('%H-%M-%S')
    @lname = "SCHNEIDER"
    @email ="STEVE.SCHNEIDER%"+@fname+"@MAGICJACK.COM"
    @fname = "AUT-"+d.strftime('%F')+"-"+d.strftime('%H-%M-%S')
    
    @rsubscriberData = RsubscriberDB.new
    @emailaddressData = EmailAddressDB.new
    @accountData = AccountDB.new
    @locationData = LocationDB.new
    @endpointData = EndPointDB.new
    @creditcardData = CreditCardDB.new
    @IppData = IppDB.new
    @cpnData = CpnDB.new
    @regData = RegDB.new
    @ordersData = OrdersDB.new
    @orderDetaildata = OrderDetailDB.new
	 
			d = DateTime.now 
		 # Registration screen Device Type selection options.
		 # "Generate magicJackEXPRESS gets 3 months"
		 @genExpress = "Generate magicJackEXPRESS"
		 @expExpDate = d.next_month(3).strftime('%m/%d/%Y')
		 @expDevicetype = "W"
	 
			 # "Generate magicJackGO gets 12 months"    
		 @genGo = "Generate magicJackGO"
		 @goExpDate = d.next_month(12).strftime('%m/%d/%Y')
		 @goDeviceType = "B"
	 
			 # "Generate magicJackGO Mexico gets 12 months"
		 @genGoMx = "Generate magicJackGO Mexico"
		 @gomxExpDate = d.next_month(12).strftime('%m/%d/%Y')
		 @gomxDeviceType = "E"
	 
			 # "Generate 2014 magicJack PLUS gets 6 months"
			 @genfourteen = "Generate 2014 magicJack PLUS"
		 @fourteenExpDate = d.next_month(6).strftime('%m/%d/%Y')
		 @fourteenDeviceType = "F"
	 
			 # "Generate magicJack PLUS"
		 @genPlus = "Generate magicJack PLUS"
		 @plusExpDate = d.next_month(12).strftime('%m/%d/%Y') 
		 @plusDeviceType = "P"   
	 
			 # "Generate magicJack should generate a message no longer able to register"
			 @genJack = "Generate magicJack"
		 @jackExpDate = d.next_month(12).strftime('%m/%d/%Y')     
		 @jackDeviceType = "M"

    # Renewal upsell date increases
    @addOneYrRenewal = d.next_month(12).strftime('%m/%d/%Y')
    @addFiveYrRenewal = d.next_month(60).strftime('%m/%d/%Y')    

    # CPN type
    @cpnTypeI = "I" # International Speed Dial Number
    @cpnTypeM = "M" # MagicNumber
    @cpnTypeP = "P" # Port-in Number
    @poerExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeS = "S" # Standard Telephone Number
    @standardExpDate = "12/31/2100"
    @standardCPNAutoRenew = d.next_month(3).strftime('%m/%d/%Y')
    @cpnTypeV = "V" # Vanity Number
    @vanityExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeK = "K" # Kustom Numer
    @kustomExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeA = "A" # Alternate contact Number
    @cpnTypeC = "C" # Canadian
    @canadaExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')    
    @cpnTypeG = "G" # Group Call
    @cpnTypeF = "F" # Park Call Forwarding
    
    d = DateTime.now
    @currentDate = d.strftime('%m/%d/%Y') 

    @expDate = d.next_year(1).strftime('%m/%d/%Y')
    @expDate1yr = d.next_year(2).strftime('%m/%d/%Y')
    @VanityExpDate1yr = d.next_year(1).strftime('%m/%d/%Y')
    @expDate5yr = d.next_year(6).strftime('%m/%d/%Y')
        
    d = DateTime.now
#	  log_file_name = 'reg_log_' + d.strftime('%S') + '.txt'
#		$stderr = File.new(File.join(File.dirname(__FILE__), log_file_name), 'w')
#    $stdout = File.new(File.join(File.dirname(__FILE__), log_file_name), 'w')
	
	
end
  
   
  config.after(:each) do
	
    @driver.quit
	
  end

  config.mock_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
  
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end

end