require 'oci8'
require_relative 'DBConn.rb'

class PromoSaleDB
    
	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end
	
	def set_promo_sale_data(promosaleid)

    promosaleQuery = "select regprice_productcodeid, "+
							"updated_ip, "+
							"updated_csr, "+
							"to_char(updated,'mm/dd/yyyy'), "+
							"added_ip, "+
							"added_csr, "+
							"to_char(added,'mm/dd/yyyy'), "+
							"active, "+
							"offer_type, "+
							"to_char(promo_enddate,'mm/dd/yyyy'), "+
							"to_char(promo_startdate,'mm/dd/yyyy'), "+
							"productcodeid, "+
							"promosale, "+
							"promosaleid, "+
							"to_char(display_enddate,'mm/dd/yyyy'), "+
							"to_char(display_startdate,'mm/dd/yyyy') "+
                    "from ymax.promosale where promosaleid = #{promosaleid}"
        
        
        r = @conn.exec(promosaleQuery)
		
		@promosaleData = r.fetch()
		
		return @promosaleData
                    
	end
                    
	def get_regprice_productcodeid
	  return @promosaleData[0]
	end
			
	def get_updated_ip
	  return @promosaleData[1]
	end
			
	def get_updated_csr
	  return @promosaleData[2]
	end
			
	def get_updated
	  return @promosaleData[3]
	end
			
	def get_added_ip
	  return @promosaleData[4]
	end
			
	def get_added_csr
	  return @promosaleData[5]
	end
			
	def get_added
	  return @promosaleData[6]
	end
			
	def get_active
	  return @promosaleData[7]
	end
			
	def get_offer_type
	  return @promosaleData[8]
	end
			
	def get_promo_enddate
	  return @promosaleData[9]
	end
			
	def get_promo_startdate
	  return @promosaleData[10]
	end
			
	def get_productcodeid
	  return @promosaleData[11]
	end
			
	def get_promosale
	  return @promosaleData[12]
	end
			
	def get_promosaleid
	  return @promosaleData[13]
	end
			
	def get_display_enddate
	  return @promosaleData[14]
	end
			
	def get_display_startdate
	  return @promosaleData[15]
	end
			
	def closeDBConnection		
		@conn.logoff
	end
end
    