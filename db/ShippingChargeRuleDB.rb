require 'oci8'
require_relative 'DBConn.rb'

class ShippingChargeRuleDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end	

	def set_shipping_charge_rule(shippingcategory)
						  
		shippingchargeruleQuery ="select shippingcategory, "+
								 "maxshippingquantitythreshold, "+
						 		 "freeshippingquantitythreshold, "+
								 "freerushquantitythreshold "+  
								 "from ymax.shippingchargerule "    +
								 "where shippingcategory = '" + shippingcategory + "'"
				 
		r = @conn.exec(shippingchargeruleQuery)
		
		@shippingchargeruleData = r.fetch()
		
		return @shippingchargeruleData
	end
				
	def get_shippingcategory
	  return @shippingchargeruleData[0]
	end
			
	def get_maxshippingquantitythreshold
	  return @shippingchargeruleData[1]
	end
			
	def get_freeshippingquantitythreshold
	  return @shippingchargeruleData[2]
	end
			
	def get_freerushquantitythreshold
	  return @shippingchargeruleData[3]
	end

	def closeDBConnection		
		@conn.logoff
	end
	
end