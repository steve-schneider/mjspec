require 'oci8'
require_relative 'DBConn.rb'

class ActionDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)
	end	

	def set_action_records(accountid)
		 
		@actionRecords = Array.new
		 
		actionRecsQuery =  "select actionid," +
			  			" accountid," +
			  			" cpnid," +            
			  			" calling_party_no," + 
			  			" act," +              
			  			" arg," +              
			  			" to_char(send_date,'mm/dd/yyyy')," +        
			  			" emailaddr," +        
			  			" orderid," +          
			  			" intarg2," +          
			  			" intarg3," +          
			  			" strarg1," +         
			  			" strarg2," +         
			  			" strarg3," +         
			  			" strarg4," +         
			  			" strarg5," +         
			  			" amount1," +         
			  			" amount2," +         
			  			" error_type," +      
			  			" suberror_type," + 
			  			" to_char(added,'mm/dd/yyyy')," + 
			  			" added_csr," +   
			  			" to_char(updated,'mm/dd/yyyy')," +     
			  			" updated_csr," +  
			  			" active," +       
			  			" added_ip," +     
			  			" updated_ip," +   
			  			" bulk," +         
			  			" language" +
				" from ymax.action" +
				" where accountid = #{accountid}"
				
 				 
		 @conn.exec(actionRecsQuery) do |r| @actionRecords.push(r.join(',')) end		  	    
		
	   return @actionRecords		 
		 
	end	

	def set_action_data(actionid)
		
		actionQuery ="select actionid," +
			  			" accountid," +
			  			" cpnid," +            
			  			" calling_party_no," + 
			  			" act," +              
			  			" arg," +              
			  			" to_char(send_date,'mm/dd/yyyy')," +        
			  			" emailaddr," +        
			  			" orderid," +          
			  			" intarg2," +          
			  			" intarg3," +          
			  			" strarg1," +         
			  			" strarg2," +         
			  			" strarg3," +         
			  			" strarg4," +         
			  			" strarg5," +         
			  			" amount1," +         
			  			" amount2," +         
			  			" error_type," +      
			  			" suberror_type," + 
			  			" to_char(added,'mm/dd/yyyy')," + 
			  			" added_csr," +   
			  			" to_char(updated,'mm/dd/yyyy')," +     
			  			" updated_csr," +  
			  			" active," +       
			  			" added_ip," +     
			  			" updated_ip," +   
			  			" bulk," +         
			  			" language" +
				" from ymax.action" +
				" where actionid = #{actionid}"
				
		a = @conn.exec(actionQuery)
		
		@actionData = a.fetch()
		
		return @actionData
	end
	
	def get_actionid
		return @actionData[0]
	end

	def get_accountid
		return @actionData[1]
	end

	def get_cpnid           
		return @actionData[2]
	end

	def get_calling_party_no 
		return @actionData[3]
	end

	def get_act             
		return @actionData[4]
	end

	def get_arg            
		return @actionData[5]
	end

	def get_send_date      
		return @actionData[6]
	end

	def get_emailaddr       
		return @actionData[7]
	end

	def get_orderid         
		return @actionData[8]
	end

	def get_intarg2          
		return @actionData[9]
	end

	def get_intarg3          
		return @actionData[10]
	end

	def get_strarg1        
		return @actionData[11]
	end

	def get_strarg2        
		return @actionData[12]
	end

	def get_strarg3        
		return @actionData[13]
	end

	def get_strarg4        
		return @actionData[14]
	end

	def get_strarg5        
		return @actionData[15]
	end

	def get_amount1        
		return @actionData[16]
	end

	def get_amount2        
		return @actionData[17]
	end

	def get_error_type     
		return @actionData[18]
	end

	def get_suberror_type 
		return @actionData[19]
	end

	def get_added        
		return @actionData[20]
	end

	def get_added_csr    
		return @actionData[21]
	end

	def get_updated    
		return @actionData[22]
	end

	def get_updated_csr 
		return @actionData[23]
	end

	def get_active       
		return @actionData[24]
	end

	def get_added_ip     
		return @actionData[25]
	end

	def get_updated_ip  
		return @actionData[26]
	end

	def get_bulk      
		return @actionData[27]
	end

	def get_language
		return @actionData[28]
	end
  
    def closeDBConnection		
		@conn.logoff
	end
	
end