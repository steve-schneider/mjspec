require 'oci8'
require_relative 'DBConn.rb'

class NumberDidsDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)
	end	

	def set_numberdids_data(numberdidsid)
		
		numberdidsQuery ="select numberdidsid," + 
			  			" did_number," +           
			  			" numberblocksid," +        
			  			" vanity," +                
			  			" to_char(reserved,'mm/dd/yyyy')," +              
			  			" to_char(added,'mm/dd/yyyy')," +                 
			  			" added_csr," +             
			  			" to_char(updated,'mm/dd/yyyy')" +               
			  			" updated_csr," +           
			  			" status," +                
			  			" reserved_serial," +       
			  			" market," +                
			  			" added_ip," +              
			  			" updated_ip," +            
			  			" accountid" +             
				" from ymax.numberdids" +
				" where numberdidsid = #{numberdidsid}"
				
		n = @conn.exec(numberdidsQuery)
		
		@numberdidsData = n.fetch()
		
		return @numberdidsData
	end

	def get_numberdidsid
		return @numberdidsData[0]
	end

	def get_did_number           
		return @numberdidsData[1]
	end

	def get_numberblocksid        
		return @numberdidsData[2]
	end

	def get_vanity                
		return @numberdidsData[3]
	end

	def get_reserved              
		return @numberdidsData[4]
	end

	def get_added                 
		return @numberdidsData[5]
	end

	def get_added_csr             
		return @numberdidsData[6]
	end

	def get_updated               
		return @numberdidsData[7]
	end

	def get_updated_csr           
		return @numberdidsData[8]
	end

	def get_status                
		return @numberdidsData[9]
	end

	def get_reserved_serial       
		return @numberdidsData[10]
	end

	def get_market                
		return @numberdidsData[11]
	end

	def get_added_ip              
		return @numberdidsData[12]
	end

	def get_updated_ip            
		return @numberdidsData[13]
	end

	def get_accountid             
		return @numberdidsData[14]
	end

    def closeDBConnection		
		@conn.logoff
	end
	
end
