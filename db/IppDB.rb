require 'oci8'
require_relative 'DBConn.rb'

class IppDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end	

	def set_ipp_data(accountId)
						  
		ippQuery ="select ippid," +
				  " accountid," +            
				  " orderid," +             
				  " orderdetailid," +       
				  " productcodeid," +       
				  " emailaddressid," +      
				  " quantity," +            
				  " claimed," +             
				  " claim_code," +          
				  " claim_accountid," +     
				  " claim_rsubscriberid," + 
				  " to_char(claim_date,'mm/dd/yyyy')," +          
				  " to_char(email_sent,'mm/dd/yyyy')," +          
				  " error_type," +           
				  " csr," +                  
				  " to_char(added,'mm/dd/yyyy')," +                
				  " added_csr," +            
				  " to_char(updated,'mm/dd/yyyy')," +              
				  " updated_csr," +          
				  " to_char(startservice,'mm/dd/yyyy')," +         
				  " invoiceid," +            
				  " to_char(invoice_date,'mm/dd/yyyy')," +         
				  " adjustmentid," +         
				  " to_char(adjustment_date,'mm/dd/yyyy')," +     
				  " added_ip," +           
				  " updated_ip," +          
				  " called_area" +         
				" from ymax.ipp" +
				" where accountid = #{accountId}"
		r = @conn.exec(ippQuery)
		
		@ippData = r.fetch()
		
		return @ippData
	end
	
	def get_ippid
		return @ippData[0]
	end

	def get_accountid 
		return @ippData[1]
	end

	def get_orderid           
		return @ippData[2]
	end

	def get_orderdetailid             
		return @ippData[3]
	end

	def get_productcodeid       
		return @ippData[4]
	end

	def get_emailaddressid      
		return @ippData[5]
	end

	def get_quantity            
		return @ippData[6]
	end

	def get_claimed             
		return @ippData[7]
	end

	def get_claim_code          
		return @ippData[8]
	end

	def get_claim_accountid     
		return @ippData[9]
	end

	def get_claim_rsubscriberid 
		return @ippData[10]
	end

	def get_claim_date          
		return @ippData[11]
	end

	def get_email_sent          
		return @ippData[12]
	end

	def get_error_type           
		return @ippData[13]
	end

	def get_csr                  
		return @ippData[14]
	end

	def get_added                
		return @ippData[15]
	end

	def get_added_csr            
		return @ippData[16]
	end

	def get_updated              
		return @ippData[17]
	end

	def get_updated_csr          
		return @ippData[18]
	end

	def get_startservice         
		return @ippData[19]
	end

	def get_invoiceid            
		return @ippData[20]
	end

	def get_invoice_date         
		return @ippData[21]
	end

	def get_adjustmentid         
		return @ippData[22]
	end

	def get_adjustment_date      
		return @ippData[23]
	end

	def get_added_ip            
		return @ippData[24]
	end

	def get_updated_ip          
		return @ippData[25]
	end

	def get_called_area         
		return @ippData[26]
	end
  
    def closeDBConnection		
		@conn.logoff
	end
	
end