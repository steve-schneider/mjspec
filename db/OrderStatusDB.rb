require 'oci8'
require_relative 'DBConn.rb'

class OrderStatusDB

	def initialize
	     @conn = OCI8.new(CONNECTSTRING)  		
	end	

	def get_order_statusids(orderid)
		 
		@orderStatusIds = Array.new		 
		orderStatusIdQuery = "select orderstatusid from ymax.orderstatus where orderid = #{orderid}"		 
		@conn.exec(orderStatusIdQuery) do |r| @orderStatusIds.push(r.join(',')) end		  	    
		
	   return @orderStatisIds		 
		 
	end

	def set_order_status_records(orderid)
		 
		 @orderStatusRecords = Array.new
		 
		 orderStatusRecsQuery = "select orderstatusid," +
								" accountid," +
								" orderid," +
								" order_status," + 
								" to_char(trunc(statusdate), 'mm/dd/yyyy')," +
								" description," +
								" csr," +
								" to_char(trunc(added), 'mm/dd/yyyy')," + 
								" added_csr," +
								" to_char(trunc(updated), 'mm/dd/yyyy')," +
								" updated_csr," +
								" added_ip," +
								" updated_ip" +
							" from ymax.orderstatus" +
							" where orderid = #{orderid}"

 				 
		 @conn.exec(orderStatusRecsQuery) do |r| @orderStatusRecords.push(r.join(',')) end		  	    
		
	   return @orderStatusRecords		 
		 
	end


	def set_order_status_data(orderstatusid)

		orderstatusQuery = "select orderstatusid," +
								" accountid," +
								" orderid," +
								" order_status," + 
								" to_char(trunc(statusdate), 'mm/dd/yyyy')," +
								" description," +
								" csr," +
								" to_char(trunc(added), 'mm/dd/yyyy')," + 
								" added_csr," +
								" to_char(trunc(updated), 'mm/dd/yyyy')," +
								" updated_csr," +
								" added_ip," +
								" updated_ip" +
							" from ymax.orderstatus" +
							" where orderstatusid = #{orderstatusid}"

 		r = @conn.exec(orderstatusQuery)
		
		@orderstatusData = r.fetch()
		
		return @orderstatusData
	 end

	 def get_orderstatusid
	 	return @orderstatusData[0]
	 end
	 
	 def get_accountid
	 	return @orderstatusData[1]
	 end
	 
	 def get_orderid
	 	return @orderstatusData[2]
	 end

 	 def get_order_status
	 	return @orderstatusData[3]
	 end

	 def get_statusdate
	 	return @orderstatusData[4]
	 end
	 
	 def get_description
	 	return @orderstatusData[5]
	 end

	 def get_csr
	 	return @orderstatusData[6]
	 end
	 
	 def get_added
	 	return @orderstatusData[7]
	 end

	 def get_added_csr
	 	return @orderstatusData[8]
	 end

	 def get_updated
	 	return @orderstatusData[9]
	 end
 
	 def get_updated_csr
	 	return @orderstatusData[10]
	 end
	 
	 def get_added_ip
	 	return @orderstatusData[11]
	 end
	 
	 def get_updated_ip
	 	return @orderstatusData[12]
	 end
	 
    def closeDBConnection		
		@conn.logoff
	end
end
