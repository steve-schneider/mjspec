require 'oci8'
require_relative 'DBConn.rb'

class PinlessDB
    
	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end
	
	def set_pinless_data(pinlessid)

    pinlessQuery = "select calling_party_no, "+
								"updated_ip, "+
								"updated_csr, "+
								"to_char(updated,'mm/dd/yyyy'), "+
								"added_ip, "+
								"added_csr, "+
								"to_char(added,'mm/dd/yyyy'), "+
								"active, "+
								"to_char(outservice,'mm/dd/yyyy'), "+
								"to_char(inservice,'mm/dd/yyyy'), "+
								"members, "+
								"maxusers, "+
								"recordingformat, "+
								"recordingfilename, "+
								"adminopts, "+
								"opts, "+
								"adminpin, "+
								"pin, "+
								"cpnid, "+
								"accountid, "+
								"pinlessid "+
							"from ymax.pinless where pinlessid = #{pinlessid}"        
        
        r = @conn.exec(pinlessQuery)
		
		@pinlessData = r.fetch()
		
		return @pinlessData
                    
	end
                    
			def get_calling_party_no
			  return @pinlessData[0]
			end
			
			def get_updated_ip
			  return @pinlessData[1]
			end
			
			def get_updated_csr
			  return @pinlessData[2]
			end
			
			def get_updated
			  return @pinlessData[3]
			end
			
			def get_added_ip
			  return @pinlessData[4]
			end
			
			def get_added_csr
			  return @pinlessData[5]
			end
			
			def get_added
			  return @pinlessData[6]
			end
			
			def get_active
			  return @pinlessData[7]
			end
			
			def get_outservice
			  return @pinlessData[8]
			end
			
			def get_inservice
			  return @pinlessData[9]
			end
			
			def get_members
			  return @pinlessData[10]
			end
			
			def get_maxusers
			  return @pinlessData[11]
			end
			
			def get_recordingformat
			  return @pinlessData[12]
			end
			
			def get_recordingfilename
			  return @pinlessData[13]
			end
			
			def get_adminopts
			  return @pinlessData[14]
			end
			
			def get_opts
			  return @pinlessData[15]
			end
			
			def get_adminpin
			  return @pinlessData[16]
			end
			
			def get_pin
			  return @pinlessData[17]
			end
			
			def get_cpnid
			  return @pinlessData[18]
			end
			
			def get_accountid
			  return @pinlessData[19]
			end
			
			def get_pinlessid
			  return @pinlessData[20]
			end


			
	def closeDBConnection		
		@conn.logoff
	end
end
    