require 'oci8'
require_relative 'DBConn.rb'

class LettersDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end	

	def set_letters_data(lettersId)
						  
	lettersQuery = "select lettersid, "+
					"name, "+
					"printer, "+
					"DBMS_LOB.substr(document, 3000), "+
					"text, "+
					"subject, "+
					"\"FROM\" "+
					"pg_cols, "+
					"pg_rows, "+
					"tmargin, "+
					"bmargin, "+
					"lmargin "+
					"rmargin, "+
					"justified, "+
					"html, "+
					"envelope, "+
					"active, "+
					"to_char(added,'mm/dd/yyyy'), "+
					"added_csr, "+
					"to_char(updated,'mm/dd/yyyy'), "+
					"updated_csr, "+
					"added_ip, "+
					"updated_ip, "+
					"add_note, "+
					"marketing, "+
					"language "+        
					" from ymax.letters " +
					" where lettersid = #{lettersId}"
				
		r = @conn.exec(lettersQuery)
		
		@lettersData = r.fetch()
		
		return @lettersData
	end
	
	def get_lettersid
		return @lettersData[0]
	end
			
	def get_name
		return @lettersData[1]
	end
			
	def get_printer
		return @lettersData[2]
	end
			
	def get_document
		return @lettersData[3]
	end
			
	def get_text
		return @lettersData[4]
	end
			
	def get_subject
		return @lettersData[5]
	end
			
	def get_from
		return @lettersData[6]
	end
			
	def get_pg_cols
		return @lettersData[7]
	end
			
	def get_pg_rows
		return @lettersData[8]
	end
			
	def get_tmargin
		return @lettersData[9]
	end
			
	def get_bmargin
		return @lettersData[10]
	end
			
	def get_lmargin
		return @lettersData[11]
	end
			
	def get_rmargin
		return @lettersData[12]
	end
			
	def get_justified
		return @lettersData[13]
	end
			
	def get_html
		return @lettersData[14]
	end
			
	def get_envelope
		return @lettersData[15]
	end
			
	def get_active
		return @lettersData[16]
	end
			
	def get_added
		return @lettersData[17]
	end
			
	def get_added_csr
		return @lettersData[18]
	end
			
	def get_updated
		return @lettersData[19]
	end
			
	def get_updated_csr
		return @lettersData[20]
	end
			
	def get_added_ip
		return @lettersData[21]
	end
			
	def get_updated_ip
		return @lettersData[22]
	end
			
	def get_add_note
		return @lettersData[23]
	end
			
	def get_marketing
		return @lettersData[24]
	end
			
	def get_language
		return @lettersData[25]
	end
  
    def closeDBConnection		
		@conn.logoff
	end
	
end