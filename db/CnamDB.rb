require 'oci8'
require_relative 'DBConn.rb'

class CnamDB
    
	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end
	
	def set_cnam_data(cnamid)

    cnamQuery = "select cnamid, "+
				"callername, "+
				"to_char(added,'mm/dd/yyyy'), "+
				"added_csr, "+
				"added_ip, "+
				"to_char(updated,'mm/dd/yyyy'), "+
				"updated_csr, "+
				"updated_ip, "+
				"active, "+
				"accountid, "+
				"calling_party_no "+
			    "from ymax.cnam where cnamid = #{cnamid}"
        
        r = @conn.exec(cnamQuery)
		
		@cnamData = r.fetch()
		
		return @cnamData
                    
	end
	
	def get_cnamid
	  return @cnamData[0]
	end
				
	def get_callername
		return @cnamData[1]
	end
				
	def get_added
		return @cnamData[2]
	end
				
	def get_added_csr
		return @cnamData[3]
	end
				
	def get_added_ip
		return @cnamData[4]
	end
				
	def get_updated
		return @cnamData[5]
	end
				
	def get_updated_csr
		return @cnamData[6]
	end
				
	def get_updated_ip
		return @cnamData[7]
	end
				
	def get_active
		return @cnamData[8]
	end
				
	def get_accountid
		return @cnamData[9]
	end
				
	def get_calling_party_no
		return @cnamData[10]
	end
	
	def closeDBConnection		
		@conn.logoff
	end

end
    