require 'oci8'
require_relative 'DBConn.rb'

class DeliveryCostContainerDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)
	end	

	def set_delivery_cost_container_data(deliverycostcontainerid)
		
		deliverycostcontainerQuery ="select deliverycostcontainerid," + 
			  						" active," +
			  						" to_char(added,'mm/dd/yyyy')," +
			  						" added_csr," +
			  						" added_ip," +
			  						" carrier," +
			  						" country," +
			  						" cubicindicator," +           
			  						" dirname," +
			  						" labelname," +     
			  						" mailclass," +        
			  						" maxpackagequantity," +
			  						" minpackagequantity," +
			  						" parcel," +
			  						" pkgdepth," +         
			  						" pkglength," +      
			  						" pkgwidth," +        
			  						" to_char(updated,'mm/dd/yyyy')," +           
			  						" updated_csr," + 
			  						" updated_ip," +     
			  						" usecubic" +       
				" from ymax.deliverycostcontainer" +
				" where deliverycostcontainerid = #{deliverycostcontainerid}"
				
		dcc = @conn.exec(deliverycostcontainerQuery)
		
		@deliverycostcontainerData = dcc.fetch()
		
		return @deliverycostcontainerData
	end
	
	def get_numberblocksid
		return @deliverycostcontainerData[0]
	end

	def get_deliverycostcontainerid
		return @deliverycostcontainerData[1]
	end

	def get_active
		return @deliverycostcontainerData[2]
	end

	def get_added
		return @deliverycostcontainerData[3]
	end

	def get_added_csr
		return @deliverycostcontainerData[4]
	end

	def get_added_ip
		return @deliverycostcontainerData[5]
	end

	def get_carrier
		return @deliverycostcontainerData[6]
	end

	def get_country
		return @deliverycostcontainerData[7]
	end

	def get_cubicindicator           
		return @deliverycostcontainerData[8]
	end

	def get_dirname
		return @deliverycostcontainerData[9]
	end

	def get_labelname      
		return @deliverycostcontainerData[10]
	end

	def get_mailclass        
		return @deliverycostcontainerData[11]
	end

	def get_maxpackagequantity
		return @deliverycostcontainerData[12]
	end

	def get_minpackagequantity
		return @deliverycostcontainerData[13]
	end

	def get_parcel
		return @deliverycostcontainerData[14]
	end

	def get_pkgdepth         
		return @deliverycostcontainerData[15]
	end

	def get_pkglength      
		return @deliverycostcontainerData[16]
	end

	def get_pkgwidth        
		return @deliverycostcontainerData[17]
	end

	def get_updated           
		return @deliverycostcontainerData[18]
	end

	def get_updated_csr 
		return @deliverycostcontainerData[19]
	end

	def get_updated_ip     
		return @deliverycostcontainerData[20]
	end

	def get_usecubic  
		return @deliverycostcontainerData[21]
	end
	
    def closeDBConnection		
		@conn.logoff
	end
		
end
