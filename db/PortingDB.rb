require 'oci8'
require_relative 'DBConn.rb'

class PortingDB
    
	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end
	
	def set_porting_data(portingid)

    portingQuery = "select to_char(letter_id), "+
							"order_returned, "+
							"updated_ip, "+
							"added_ip, "+
							"to_char(added,'mm/dd/yyyy'), "+
							"added_csr, "+
							"to_char(updated,'mm/dd/yyyy'), "+
							"updated_csr, "+
							"to_char(process_date,'mm/dd/yyyy'), "+
							"error_message, "+
							"to_char(error_type), "+
							"to_char(status_date,'mm/dd/yyyy'), "+
							"to_char(status_type), "+
							"to_char(transmit_date,'mm/dd/yyyy'), "+
							"to_char(port_type), "+
							"reject_message, "+
							"reject_code, "+
							"status, "+
							"calling_party_no, "+
							"to_char(due_date,'mm/dd/yyyy'), "+
							"ver, "+
							"ccna, "+
							"pon, "+
							"to_char(payload), "+
							"to_char(id), "+
							"new_spid, "+
							"portbits, "+
							"old_spid, "+
							"soa_action, "+
							"to_char(sup_type), "+
							"to_char(retry_count) "+
							"from ymax.porting where id = #{portingid}"
        
        r = @conn.exec(portingQuery)
		
		@portingData = r.fetch()
		
		return @portingData
                    
	end
                    
	def get_letter_id
		return @portingData[0]
	end
				
	def get_order_returned
		return @portingData[1]
	end
				
	def get_updated_ip
		return @portingData[2]
	end
				
	def get_added_ip
		return @portingData[3]
	end
				
	def get_added
		return @portingData[4]
	end
				
	def get_added_csr
		return @portingData[5]
	end
				
	def get_updated
		return @portingData[6]
	end
				
	def get_updated_csr
		return @portingData[7]
	end
				
	def get_process_date
		return @portingData[8]
	end
				
	def get_error_message
		return @portingData[9]
	end
				
	def get_error_type
		return @portingData[10]
	end
				
	def get_status_date
		return @portingData[11]
	end
				
	def get_status_type
		return @portingData[12]
	end
				
	def get_transmit_date
		return @portingData[13]
	end
				
	def get_port_type
		return @portingData[14]
	end
				
	def get_reject_message
		return @portingData[15]
	end
				
	def get_reject_code
		return @portingData[16]
	end
				
	def get_status
		return @portingData[17]
	end
				
	def get_calling_party_no
		return @portingData[18]
	end
				
	def get_due_date
		return @portingData[19]
	end
				
	def get_ver
		return @portingData[20]
	end
				
	def get_ccna
		return @portingData[21]
	end
				
	def get_pon
		return @portingData[22]
	end
				
	def get_payload
		return @portingData[23]
	end
				
	def get_id
		return @portingData[24]
	end
				
	def get_new_spid
		return @portingData[25]
	end
				
	def get_portbits
		return @portingData[26]
	end
				
	def get_old_spid
		return @portingData[27]
	end
				
	def get_soa_action
		return @portingData[28]
	end
				
	def get_sup_type
		return @portingData[29]
	end
				
	def get_retry_count
		return @portingData[30]
	end
			
	def closeDBConnection		
		@conn.logoff
	end
end
    