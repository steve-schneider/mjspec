require 'oci8'
require_relative 'DBConn.rb'

class PremiumDB

	def initialize		
	     @conn = OCI8.new(CONNECTSTRING)  
#		@conn = OCI8.new('winman/winnat0309@//develop.talk4free.com:1521/ddb01')	
	end	

	def set_premium_data(premiumid)

		premiumQuery = "select premiumid," +
		                 " accountid," +
                        " accountid," +
                        " orderid," +
                        " orderdetailid," +
                        " productcodeid," +
                        " emailaddressid," +
                        " quantity," +
                        " error_type," +
                        " claimed," +
                        " claim_code," +
                        " claim_accountid," +                   
                        " claim_cpnid," +
                        " to_char(trunc(claim_date), 'mm/dd/yyyy')," +
                        " to_char(trunc(inservice), 'mm/dd/yyyy')," +
                        " invoiceid," +
                        " to_char(trunc(invoice_date), 'mm/dd/yyyy')," +
                        " adjustmentid," + 
                        " to_char(trunc(adjustment_date), 'mm/dd/yyyy')," +
                        " to_char(trunc(added), 'mm/dd/yyyy')," +
                        " added_csr," +
                        " added_ip," + 
                        " to_char(trunc(updated), 'mm/dd/yyyy')," +
                        " updated_csr," + 
                        " updated_ip" +
					" from  ymax.premium" +
					" where premiumid = #{premiumid}"

		r = @conn.exec(premiumQuery)

		@premiumData = r.fetch()
		
		return @premiumData
	 end

 	def get_premiumid
	   return @premiumData[0]
	end
	
	def get_accountid
	   return @premiumData[1]
	end
	
	def get_orderid
	   return @premiumData[2]
	end
	
	def get_orderdetailid
	   return @premiumData[3]
	end

	def get_productcodeid
	   return @premiumData[4]
	end
	
	def get_emailaddressid
	   return @premiumData[5]
	end

	def get_quantity
	   return @premiumData[6]
	end
	
	def get_error_type
	   return @premiumData[7]
	end

	def get_claimed
	   return @premiumData[8]
	end
	
	def get_claim_code
	   return @premiumData[9]
	end

	def get_claim_accountid
	   return @premiumData[10]
	end
	
	def get_claim_cpnid
	   return @premiumData[11]
	end

	def get_claim_date
	   return @premiumData[12]
	end

	def get_inservice
	   return @premiumData[13]
	end
	
	def get_invoiceid
	   return @premiumData[14]
	end
	
	def get_invoice_date
	   return @premiumData[15]
	end
	
	def get_adjustmentid
	   return @premiumData[16]
	end
	
	def get_adjustment_date
	   return @premiumData[17]
	end
	
	def get_added
	   return @premiumData[18]
	end
	
	def get_added_csr
	   return @premiumData[19]
	end
	
	def get_added_ip
	   return @premiumData[20]
	end
	
	def get_updated
	   return @premiumData[21]
	end
	
	def get_updated_csr
	   return @premiumData[22]
	end
	
	def get_updated_ip
	   return @premiumData[23]
	end
	
    def closeDBConnection		
		@conn.logoff
	end
end
