require 'oci8'
require_relative 'DBConn.rb'

class RmaDB
    
	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end
	
	def set_rma_data(rmaId)
        
                    rmaQuery = " select ordernumber, "+
                                "rmaid, "+
                                "orderid, "+
                                "to_char(updated,'mm/dd/yyyy'), "+
                                "updated_ip, "+
                                "added_ip, "+
                                "credit, "+
                                "processed, "+
                                "requested, "+                               
                                "reason, "+
                                "to_char(added,'mm/dd/yyyy'), "+
                                "rma_type, "+
                                "updated_csr, "+
                                "added_csr, "+                                
                                "notes, "+
                                "zip, "+
                                "state, "+
                                "city, "+
                                "address, "+
                                "name, "+
                                "emailaddress "+
                                "from ymax.rma where rmaid = #{rmaId}"
                                				
		r = @conn.exec(rmaQuery)
		
		@rmaData = r.fetch()
		
		return @rmaData
	end
            
    def get_ordernumber
     return @rmaData[0]
    end
        
    def get_rmaid
     return @rmaData[1]
    end
        
    def get_orderid
     return @rmaData[2]
    end
        
    def get_updated
     return @rmaData[3]
    end
        
    def get_updated_ip
     return @rmaData[4]
    end
        
    def get_added_ip
     return @rmaData[5]
    end
        
    def get_credit
     return @rmaData[6]
    end
        
    def get_processed
     return @rmaData[7]
    end
        
    def get_requested
     return @rmaData[8]
    end
        
    def get_reason
     return @rmaData[9]
    end
        
    def get_added
     return @rmaData[10]
    end
        
    def get_rma_type
     return @rmaData[11]
    end
        
    def get_updated_csr
     return @rmaData[12]
    end
        
    def get_added_csr
     return @rmaData[13]
    end
                       
    def get_notes
     return @rmaData[14]
    end
        
    def get_zip
     return @rmaData[15]
    end
        
    def get_state
     return @rmaData[16]
    end
        
    def get_city
     return @rmaData[17]
    end
        
    def get_address
     return @rmaData[18]
    end
        
    def get_name
     return @rmaData[19]
    end
        
    def get_emailaddress
     return @rmaData[20]
    end
         
    def closeDBConnection		
		@conn.logoff
	end
end

