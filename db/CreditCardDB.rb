require 'oci8'
require_relative 'DBConn.rb'

class CreditCardDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end	

	def set_credit_card_data(creditcardid)

		creditcardQuery   = "select creditcardid," +
							" accountid," +
							" full_name," + 
							" first_name," +
							" last_name," +
							" card_type," +
							" to_char(exp_date)," +
							" active," + 
							" csr," +							
							" to_char(trunc(added), 'mm/dd/yyyy')," +
							" added_csr," +
							" to_char(trunc(updated), 'mm/dd/yyyy')," +
							" updated_csr," +
							" added_ip," + 
							" updated_ip," +
							" fraud_override," +
							" fraud_csr," +
							" cvv2_match," +
							" processor_avs," +
							" addr_match," +
							" zip_match," +
							" debit," +
							" locationid," +
							" fixed," +
							" to_char(first_charge_date, 'mm/dd/yyyy')," +
							" to_char(charge_count)," +
							" use_for_recurring," +
							" route_no," +
							" istoken," +
							" mop," +
							" to_char(cardnumber)" +
					" from ymax.creditcard" +
					" where creditcardid = #{creditcardid}"
						
		c = @conn.exec(creditcardQuery)
		
		@creditcardData = c.fetch()
		
		return @creditcardData
	 end
	
	def get_creditcardid
		return @creditcardData[0]
	end
	 
	def get_accountid
		return @creditcardData[1]
	end
	 
	def get_full_name
		return @creditcardData[2]
	end
	 
	def get_first_name
		return @creditcardData[3]
	end
	 
	def get_last_name
		return @creditcardData[4]
	end
	 
	def get_card_type
		return @creditcardData[5]
	end
	 
	def get_exp_date
		return @creditcardData[6]
	end
	 
	def get_active
		return @creditcardData[7]
	end
	 
	def get_csr
		return @creditcardData[8]
	end
	 
	def get_added
		return @creditcardData[9]
	end
	 
	def get_added_csr
		return @creditcardData[10]
	end
	 
	def get_updated
		return @creditcardData[11]
	end
	 
	def get_updated_csr
		return @creditcardData[12]
	end
	 
	def get_added_ip
		return @creditcardData[13]
	end
	 
	def get_updated_ip
		return @creditcardData[14]
	end
	 
	def get_fraud_override
		return @creditcardData[15]
	end
	 
	def get_fraud_csr
		return @creditcardData[16]
	end
	 
	def get_cvv2_match
		return @creditcardData[17]
	end
	 
	def get_processor_avs
		return @creditcardData[18]
	end
	 
	def get_addr_match
		return @creditcardData[19]
	end
	 
	def get_zip_match
		return @creditcardData[20]
	end
	 
	def get_debit
		return @creditcardData[21]
	end
	 
	def get_locationid
		return @creditcardData[22]
	end
	 
	def get_fixed
		return @creditcardData[23]
	end
	 
	def get_first_charge_date
		return @creditcardData[24]
	end
	 
	def get_charge_count
		return @creditcardData[25]
	end
	 
	def get_use_for_recurring
		return @creditcardData[26]
	end
	 
	def get_route_no
		return @creditcardData[27]
	end
	 
	def get_istoken
		return @creditcardData[28]
	end
	 
	def get_mop
		return @creditcardData[29]
	end

	def get_cardnumber
		return @creditcardData[30]
	end

	def get_creditcardid_pk(accountid)
			creditcardpkQuery = "select max(creditcardid) from ymax.creditcard where accountid = #{accountid}"		
			c = @conn.exec(creditcardpkQuery)		
			@creditcardPK = c.fetch()		
		return @creditcardPK[0]
    end
	 
    def closeDBConnection		
		@conn.logoff
	end
	
end