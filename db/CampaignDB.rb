require 'oci8'
require_relative 'DBConn.rb'

class CampaignDB

	def initialize		
	     @conn = OCI8.new(CONNECTSTRING)  	
	end	

	def set_campaign_data(campaignid)

		campaignQuery = "select campaignid," +
		                " campaigncode," +
                        " category," +
                        " description," +
                        " promosale," +
                        " pieces_out," +
                        " orders_in," +
                        " active," +
                        " to_char(trunc(added), 'mm/dd/yyyy')," +
                        " added_csr," +
                        " added_ip," + 
                        " to_char(trunc(updated), 'mm/dd/yyyy')," +
                        " updated_csr," + 
                        " updated_ip," +
                        " campaign_url," +
                        " give_magicdollars," +
                        " landing_page" +
					" from  ymax.campaign" +
					" where campaignid = #{campaignid}"

		r = @conn.exec(campaignQuery)

		@campaignData = r.fetch()
		
		return @campaignData
	 end

	def get_campaignid
	   return @campaignData[0]
	end
	
	def get_campaigncode
	   return @campaignData[1]
	end
	
	def get_category
	   return @campaignData[2]
	end
	
	def get_description
	   return @campaignData[3]
	end

	def get_promosale
	   return @campaignData[4]
	end
	
	def get_pieces_out
	   return @campaignData[5]
	end

	def get_orders_in
	   return @campaignData[6]
	end
	
	def get_active
	   return @campaignData[7]
	end

	def get_added
	   return @campaignData[8]
	end
	
	def get_added_csr
	   return @campaignData[9]
	end

	def get_added_ip
	   return @campaignData[10]
	end
	
	def get_updated
	   return @campaignData[11]
	end
	
	def get_updated_csr
	   return @campaignData[12]
	end

	def get_updated_ip
	   return @campaignData[13]
	end
	
	def get_campaign_url
	   return @campaignData[14]
	end
	
	def get_give_magicdollars
	   return @campaignData[15]
	end
	
	def get_landing_page
	   return @campaignData[16]
	end
	
    def closeDBConnection		
		@conn.logoff
	end
end
