require 'oci8'
require_relative 'DBConn.rb'

class PrePaidAccessDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end	

	def set_prepaid_access_data(prepaidaccessid)
						  
		prepaidaccessQuery ="select prepaidaccessid, "+					
									"accountid, "+
									"subscriptionid, "+
									"fromnumber, "+
									"accessnumber, "+
									"forwardnumber, "+
									"type, "+
									"to_char(inservice,'mm/dd/yyyy'), "+
									"to_char(outservice,'mm/dd/yyyy'), "+
									"name, "+
									"active, "+
									"cpnid, "+
									"to_char(added,'mm/dd/yyyy'), "+
									"added_csr, "+
									"added_ip, "+
									"to_char(updated,'mm/dd/yyyy'), "+
									"updated_csr, "+
									"updated_ip, "+
									"rsubscriberid "+        
							"from ymax.prepaidaccess "  +
							"where prepaidaccessid = #{prepaidaccessid}"
				 
		r = @conn.exec(prepaidaccessQuery)
		
		@prepaidaccessData = r.fetch()
		
		return @prepaidaccessData
	end
				
	def get_prepaidaccessid
	  return @prepaidaccessData[0]
	end
					
	def get_accountid
	 return @prepaidaccessData[1]
	end
					
	def get_subscriptionid
	 return @prepaidaccessData[2]
	end
					
	def get_fromnumber
	  return @prepaidaccessData[3]
	end
					
	def get_accessnumber
		return @prepaidaccessData[4]
	end
					
	def get_forwardnumber
		return @prepaidaccessData[5]
	end
					
	def get_type
		return @prepaidaccessData[6]
	end
					
	def get_inservice
		return @prepaidaccessData[7]
	end
					
	def get_outservice
		return @prepaidaccessData[8]
	end
					
	def get_name
		return @prepaidaccessData[9]
	end
					
	def get_active
		return @prepaidaccessData[10]
	end
					
	def get_cpnid
		return @prepaidaccessData[11]
	end
					
	def get_added
		return @prepaidaccessData[12]
	end
					
	def get_added_csr
		return @prepaidaccessData[13]
	end
					
	def get_added_ip
		return @prepaidaccessData[14]
	end
					
	def get_updated
		return @prepaidaccessData[15]
	end
					
	def get_updated_csr
		return @prepaidaccessData[16]
	end
					
	def get_updated_ip
		return @prepaidaccessData[17]
	end
					
	def get_rsubscriberid
		return @prepaidaccessData[18]
	end
  
    def closeDBConnection		
		@conn.logoff
	end
	
end