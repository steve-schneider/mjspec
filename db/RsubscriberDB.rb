require 'oci8'
require_relative 'DBConn.rb'

class RsubscriberDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)			
	end	

	def set_rsubscriber_data(rsubscriberid)
	
		rsubscriberQuery =  "select rsubscriberid," +                  
							" accountid," +
					  		" cpnid," +                          
				  			" registration_status," +            
					  		" to_char(registration_date,'mm/dd/yyyy')," +              
					  		" active," +                         
					  		" to_char(added,'mm/dd/yyyy')," +                          
					  		" added_csr," +                      
					  		" to_char(updated,'mm/dd/yyyy')," +                        
					  		" updated_csr," +                    
					  		" serial_no," +                      
					  		" overlimit_warnings," +             
					  		" to_char(startservice,'mm/dd/yyyy')," +                   
					  		" to_char(endservice,'mm/dd/yyyy')," +                     
					  		" autorenew," +                      
					  		" ippflag," +                        
					  		" to_char(ippflag_date,'mm/dd/yyyy')," +                   
					  		" routing," +                        
					  		" service_type," +                   
					  		" pastdue," +                        
					  		" abuse_flag," +                     
					  		" timezone," +                       
					  		" register_include," +               
					  		" insurance_productcodeid," +        
					  		" to_char(unregistered_ipp_minutes)," +       
					  		" to_char(registered_ipp_minutes)," +         
					  		" added_ip," +                       
					  		" updated_ip," +                     
					  		" lost," +                           
					  		" to_char(lastnotificationdate,'mm/dd/yyyy')," +           
					  		" position," +                       
					  		" endpointid," +                     
					  		" device_type," +                    
					  		" devicename," +                     
					  		" to_char(subscriptionid)," +                 
					  		" to_char(serviceplan_productcodeid)," +      
					  		" to_char(e911serviceplan_productcodeid)," +  
					  		" push_profile," +                   
					  		" to_char(renewal_lag)," +                    
					  		" suspended," +                      
					  		" subscriptionactive," +             
					  		" to_char(treatments)," +                     
					  		" to_char(referrer_accountid)," +             
					  		" inbound_referrerdetailid," +       
					  		" vmtimer," +                        
					  		" to_char(versiontime,'mm/dd/yyyy')," +                    
					  		" to_char(e911service_enddate,'mm/dd/yyyy')," +            
					  		" e911service_locationid," +         
					  		" attr_upgrade_from," +              
					  		" attr_upgrade_to," +                
					  		" attr_upgrade_extension," +         
					  		" attr_downgrade_from," +            
					  		" attr_downgrade_to," +             
					  		" attr_replace_from," +             
					  		" attr_replace_to," +                
					  		" attr_renewed," +                   
					  		" attr_reactivated, " +             
					  		" to_char(e911service_billdate,'mm/dd/yyyy') " +      
					  		" from ymax.rsubscriber" +
					  		" where rsubscriberid = #{rsubscriberid}"
	
		r = @conn.exec(rsubscriberQuery)
		
		@rsubscriberData = r.fetch()
		
		return @rsubscriberData
	 end
	
    def get_rsubscriber_pk(accountid)
		
			rsubPkQuery = "select max(rsubscriberid) from ymax.rsubscriber where accountid = #{accountid}"		
			r = @conn.exec(rsubPkQuery)		
			@rsubscribePK = r.fetch()		
			return @rsubscribePK[0]
    end
	
	 def get_rsubscriberid
	 	return @rsubscriberData[0]
	 end
	 
	 def get_accountid
	 	return @rsubscriberData[1]
	 end
	 
	 def get_cpnid
	 	return @rsubscriberData[2]
	 end
	 
	 def get_registration_status
	 	return @rsubscriberData[3]
	 end

	 def get_registration_date
	 	return @rsubscriberData[4]
	 end

	 def get_active
	 	return @rsubscriberData[5]
	 end
	 
	 def get_added
	 	return @rsubscriberData[6]
	 end
	 
	 def get_added_csr
	 	return @rsubscriberData[7]
	 end

	 def get_updated
	 	return @rsubscriberData[8]
	 end
	 
	 def get_updated_csr
	 	return @rsubscriberData[9]
	 end

	 def get_serial_no
	 	return @rsubscriberData[10]
	 end
	 
	 def get_overlimit_warnings
	 	return @rsubscriberData[11]
	 end
	 
	 def get_startservice
	 	return @rsubscriberData[12]
	 end
	 
	 def get_endservice
	 	return @rsubscriberData[13]
	 end
	 
	 def get_autorenew
	 	return @rsubscriberData[14]
	 end
	 
	 def get_ippflag
	 	return @rsubscriberData[15]
	 end
	 
	 def get_ippflag_date
	 	return @rsubscriberData[16]
	 end
	 
	 def get_routing
	 	return @rsubscriberData[17]
	 end
	 
	 def get_service_type
	 	return @rsubscriberData[18]
	 end
	 
	 def get_pastdue
	 	return @rsubscriberData[19]
	 end
	 
	 def get_abuse_flag
	 	return @rsubscriberData[20]
	 end
	 
	 def get_timezone
	 	return @rsubscriberData[21]
	 end
	 
	 def get_register_include
	 	return @rsubscriberData[22]
	 end
	 
	 def get_insurance_productcodeid
	 	return @rsubscriberData[23]
	 end
	 
	 def get_unregistered_ipp_minutes
	 	return @rsubscriberData[24]
	 end
	 
	 def get_registered_ipp_minutes
	 	return @rsubscriberData[25]
	 end
	 
	 def get_added_ip
	 	return @rsubscriberData[26]
	 end
	 
	 def get_updated_ip
	 	return @rsubscriberData[27]
	 end

	 def get_lost
	 	return @rsubscriberData[28]
	 end
	 
	 def get_lastnotificationdate
	 	return @rsubscriberData[29]
	 end
	 
	 def get_position
	 	return @rsubscriberData[30]
	 end
	 
	 def get_endpointid
	 	return @rsubscriberData[31]
	 end
	 
	 def get_device_type
	 	return @rsubscriberData[32]
	 end
	 
	 def get_devicename
	 	return @rsubscriberData[33]
	 end
	 
	 def get_subscriptionid
	 	return @rsubscriberData[34]
	 end
	 
	 def get_serviceplan_productcodeid
	 	return @rsubscriberData[35]
	 end
	 
	 def get_e911serviceplan_productcodeid
	 	return @rsubscriberData[36]
	 end
	 
	 def get_push_profile
	 	return @rsubscriberData[37]
	 end
	 
	 def get_renewal_lag
	 	return @rsubscriberData[38]
	 end
	 
	 def get_suspended
	 	return @rsubscriberData[39]
	 end
	 
	 def get_subscriptionactive
	 	return @rsubscriberData[40]
	 end
	 
	 def get_treatments
	 	return @rsubscriberData[41]
	 end
	 
	 def get_referrer_accountid
	 	return @rsubscriberData[42]
	 end
	 
	 def get_inbound_referrerdetailid
	 	return @rsubscriberData[43]
	 end
	 
	 def get_vmtimer
	 	return @rsubscriberData[44]
	 end
	 
	 def get_versiontime
	 	return @rsubscriberData[45]
	 end
	 
	 def get_e911service_enddate
	 	return @rsubscriberData[46]
	 end
	 
	 def get_e911service_locationid
	 	return @rsubscriberData[47]
	 end
	 
	 def get_attr_upgrade_from
	 	return @rsubscriberData[48]
	 end
	 
	 def get_attr_upgrade_to
	 	return @rsubscriberData[49]
	 end
	 
	 def get_attr_upgrade_extension
	 	return @rsubscriberData[50]
	 end
	 
	 def get_attr_downgrade_from
	 	return @rsubscriberData[51]
	 end
	 
	 def get_attr_downgrade_to
	 	return @rsubscriberData[52]
	 end
	 
	 def get_attr_replace_from
	 	return @rsubscriberData[53]
	 end
	 
	 def get_attr_replace_to
	 	return @rsubscriberData[54]
	 end
	 
	 def get_attr_renewed
	 	return @rsubscriberData[55]
	 end
	 
	 def get_attr_reactivated
	 	return @rsubscriberData[56]
	 end
	 
	 def get_e911service_billdate
	 	return @rsubscriberData[57]
	 end
	 
    def closeDBConnection		
		@conn.logoff
	end
	
end