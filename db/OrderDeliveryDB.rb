require 'oci8'
require_relative 'DBConn.rb'

class OrderDeliveryDB

	def initialize
	     @conn = OCI8.new(CONNECTSTRING)  
#		@conn = OCI8.new('winman/winnat0309@//develop.talk4free.com:1521/ddb01')		
	end	

	def set_orders_delivery_data(orderdeliveryid)

		orderdeliveryQuery =    "select orderdeliveryid," +
                            " orderid," +
                            " accountid," +
                            " locationid," + 
                            " batch_no," +
                            " shipstatus," +
                            " to_char(trunc(shipdate), 'mm/dd/yyyy')," + 
                            " total_qty," + 
                            " to_char(total_weight)," +
                            " mailclass," +
                            " toreturncode," +
                            " status," +
                            " usps_trackingno," +
                            " to_char(final_postage)," +
                            " transactionid," +
                            " transaction_datetime," +
                            " postmark_date," +
                            " to_char(trunc(deliverdate), 'mm/dd/yyyy')," + 
                            " print_count," + 
                            " csr," +
                            " to_char(trunc(added), 'mm/dd/yyyy')," +
                            " added_csr," +
                            " to_char(trunc(updated), 'mm/dd/yyyy')," + 
                            " updated_csr," + 
                            " reshipped," +
                            " barcode1," +
                            " added_ip," +
                            " updated_ip," +
                            " deliverycategory," +
                            " to_char(package_count)," +
                            " to_char(deliverycostcontainerid)," + 
                            " to_char(scanned)," + 
                            " to_char(reship_orderdeliveryid)," +
                            " rush," +
                            " to_char(trunc(order_date), 'mm/dd/yyyy')" + 
                        " from ymax.orderdelivery" +
                        " where orderdeliveryid = #{orderdeliveryid}"

 		r = @conn.exec(orderdeliveryQuery)
		
		@ordersdeliveryData = r.fetch()
		
		return @ordersdeliveryData
	 end

	 def get_orderdeliveryid
	 	return @ordersdeliveryData[0]
	 end
	 
	 def get_orderid
	 	return @ordersdeliveryData[1]
	 end
	 
	 def get_accountid
	 	return @ordersdeliveryData[2]
	 end

 	 def get_locationid
	 	return @ordersdeliveryData[3]
	 end

	 def get_batch_no
	 	return @ordersdeliveryData[4]
	 end
	 
	 def get_shipstatus
	 	return @ordersdeliveryData[5]
	 end

	 def get_shipdate
	 	return @ordersdeliveryData[6]
	 end
	 
	 def get_total_qty
	 	return @ordersdeliveryData[7]
	 end

	 def get_total_weight
	 	return @ordersdeliveryData[8]
	 end

	 def get_mailclass
	 	return @ordersdeliveryData[9]
	 end
 
	 def get_toreturncode
	 	return @ordersdeliveryData[10]
	 end
	 
	 def get_status
	 	return @ordersdeliveryData[11]
	 end
	 
	 def get_usps_trackingno
	 	return @ordersdeliveryData[12]
	 end
 
    def get_final_postage
       return @ordersdeliveryData[13]
    end
    
    def get_transactionid
       return @ordersdeliveryData[14]
    end
    
    def get_transaction_datetime
       return @ordersdeliveryData[15]
    end

    def get_postmark_date
       return @ordersdeliveryData[16]
    end

    def get_deliverdate
       return @ordersdeliveryData[17]
    end
    
    def get_print_count
       return @ordersdeliveryData[18]
    end

    def get_csr
       return @ordersdeliveryData[19]
    end
    
    def get_added
       return @ordersdeliveryData[20]
    end

    def get_added_csr
       return @ordersdeliveryData[21]
    end

    def get_updated
       return @ordersdeliveryData[22]
    end

    def get_updated_csr
       return @ordersdeliveryData[23]
    end
    
    def get_reshipped
       return @ordersdeliveryData[24]
    end
    
    def get_barcode1
       return @ordersdeliveryData[25]
    end
    
    def get_added_ip
       return @ordersdeliveryData[26]
    end
    
    def get_updated_ip
       return @ordersdeliveryData[27]
    end
    
    def get_deliverycategory
       return @ordersdeliveryData[28]
    end

    def get_package_count
       return @ordersdeliveryData[29]
    end

    def get_deliverycostcontainerid
       return @ordersdeliveryData[30]
    end
    
    def get_scanned
       return @ordersdeliveryData[31]
    end

    def get_reship_orderdeliveryid
       return @ordersdeliveryData[32]
    end
    
    def get_rush
       return @ordersdeliveryData[33]
    end

    def get_order_date
       return @ordersdeliveryData[34]
    end

    def closeDBConnection		
		@conn.logoff
	end
end
