require 'oci8'
require_relative 'DBConn.rb'

class NsubscriberDB

	def initialize		
	     @conn = OCI8.new(CONNECTSTRING)  
#		@conn = OCI8.new('winman/winnat0309@//develop.talk4free.com:1521/ddb01')	
	end	

	def set_nsubscriber_data(nsubscriberid)

		nsubscriberQuery = "select NSUBSCRIBERID," +
		                 " ACTIVE," +
                        " DEVICE_TYPE," +
                        " SERIAL_NO," +
                        " ADDED_SEQ," +
                        " ENDPOINTID," +
                        " to_char(trunc(STARTSERVICE), 'mm/dd/yyyy')," +
                        " to_char(trunc(ENDSERVICE), 'mm/dd/yyyy')," +                    
                        " to_char(trunc(ADDED), 'mm/dd/yyyy')," +
                        " ADDED_CSR," +
                        " ADDED_IP," + 
                        " to_char(trunc(UPDATED), 'mm/dd/yyyy')," +
                        " UPDATED_CSR," + 
                        " UPDATED_IP," +
                        " X_ACCOUNTID," +
                        " DEVICENAME," +
                        " to_char(PROVISIONINGDELAY)" +
					" from  YMAX.NSUBSCRIBER" +
					" where NSUBSCRIBERID = #{nsubscriberid}" 			

		r = @conn.exec(nsubscriberQuery)

		@nsubscriberData = r.fetch()
		
		return @nsubscriberData
	end

	def get_nsubscriberid
	   return @nsubscriberData[0]
	end
	
	def get_active
	   return @nsubscriberData[1]
	end
	
	def get_device_type
	   return @nsubscriberData[2]
	end
	
	def get_serial_no
	   return @nsubscriberData[3]
	end

	def get_added_seq
	   return @nsubscriberData[4]
	end
	
	def get_endpointid
	   return @nsubscriberData[5]
	end

	def get_startservice
	   return @nsubscriberData[6]
	end
	
	def get_endservice
	   return @nsubscriberData[7]
	end

	def get_added
	   return @nsubscriberData[8]
	end
	
	def get_added_csr
	   return @nsubscriberData[9]
	end

	def get_added_ip
	   return @nsubscriberData[10]
	end
	
	def get_updated
	   return @nsubscriberData[11]
	end
	
	def get_updated_csr
	   return @nsubscriberData[12]
	end

	def get_updated_ip
	   return @nsubscriberData[13]
	end
	
	def get_x_accountid
	   return @nsubscriberData[14]
	end
	
	def get_devicename
	   return @nsubscriberData[15]
	end
	
	def get_provisioningdelay
	   return @nsubscriberData[16]
	end
	
    def closeDBConnection		
		@conn.logoff
	end
end
