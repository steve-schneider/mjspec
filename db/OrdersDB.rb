require 'oci8'
require_relative 'DBConn.rb'

class OrdersDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)			
	end	

	def set_orders_data(orderid)
		ordersQuery ="select orderid," +
						" to_char(inbound_orderid)," +      
						" accountid," +      
						" emailaddressid," +           
						" creditcardid," + 
						" to_char(order_number)," +            
						" order_date," +    
						" to_char(order_time)," +    
						" order_status," +               
						" to_char(statusdate,'mm/dd/yyyy')," +     
						" onhold_reason," +         
						" promo_code," + 
						" authcode," +        
						" to_char(subtotal)," +          
						" to_char(shipping)," +           
						" to_char(taxes)," +  
						" to_char(total)," +  
						" to_char(taxrateid)," +        
						" to_char(taxrate)," +             
						" csr," +       
						" to_char(added,'mm/dd/yyyy')," + 
						" added_csr," +      
						" to_char(updated,'mm/dd/yyyy')," +           
						" updated_csr," + 
						" to_char(adj_subtotal)," +               
						" to_char(adj_taxes)," +        
						" to_char(adj_shipping)," + 
						" to_char(adj_total)," +        
						" to_char(adj_sum_total)," +           
						" to_char(minpaydate,'mm/dd/yyyy')," +   
						" unpaid," +              
						" payment," +           
						" to_char(result_code)," +  
						" to_char(last_auth_attempt,'mm/dd/yyyy')," + 
						" to_char(auth_attempts)," +           
						" auth_total," +    
						" authmessage," +               
						" added_ip," +          
						" updated_ip," +     
						" sourceind," +       
						" pflags," +               
						" nflags," +               
						" to_char(campaignid)," +   
						" to_char(orderdate,'mm/dd/yyyy')," +      
						" to_char(cancelreason)" + 		
				   " from ymax.orders" +
				   " where orderid = #{orderid}"
		o = @conn.exec(ordersQuery)
		
		@ordersData = o.fetch()
		
		return @ordersData
	end

	def get_order_pk(accountId)
		
		orderIdQuery = "select max(orderid) from ymax.orders where accountid = #{accountId}"
		
		od = @conn.exec(orderIdQuery)
		
		@orderPK = od.fetch()
		
		return @orderPK[0]
	end

	def get_orderid
		return @ordersData[0]
	end
	
	def get_inbound_orderid      
		return @ordersData[1]
	end

	def get_accountid      
		return @ordersData[2]
	end

	def get_emailaddressid           
		return @ordersData[3]
	end

	def get_creditcardid 
		return @ordersData[4]
	end

	def get_order_number            
		return @ordersData[5]
	end

	def get_order_date    
		return @ordersData[6]
	end

	def get_order_time    
		return @ordersData[7]
	end

	def get_order_status               
		return @ordersData[8]
	end

	def get_statusdate     
		return @ordersData[9]
	end

	def get_onhold_reason         
		return @ordersData[10]
	end

	def get_promo_code 
		return @ordersData[11]
	end

	def get_authcode        
		return @ordersData[12]
	end

	def get_subtotal          
		return @ordersData[13]
	end

	def get_shipping           
		return @ordersData[14]
	end

	def get_taxes  
		return @ordersData[15]
	end

	def get_total  
		return @ordersData[16]
	end

	def get_taxrateid        
		return @ordersData[17]
	end

	def get_taxrate             
		return @ordersData[18]
	end

	def get_csr       
		return @ordersData[19]
	end

	def get_added 
		return @ordersData[20]
	end

	def get_added_csr      
		return @ordersData[21]
	end

	def get_updated           
		return @ordersData[22]
	end

	def get_updated_csr 
		return @ordersData[23]
	end

	def get_adj_subtotal               
		return @ordersData[24]
	end

	def get_adj_taxes        
		return @ordersData[25]
	end

	def get_adj_shipping 
		return @ordersData[26]
	end

	def get_adj_total        
		return @ordersData[27]
	end

	def get_adj_sum_total           
		return @ordersData[28]
	end

	def get_minpaydate   
		return @ordersData[29]
	end

	def get_unpaid              
		return @ordersData[30]
	end

	def get_payment           
		return @ordersData[31]
	end

	def get_result_code  
		return @ordersData[32]
	end

	def get_last_auth_attempt 
		return @ordersData[33]
	end

	def get_auth_attempts           
		return @ordersData[34]
	end

	def get_auth_total    
		return @ordersData[35]
	end

	def get_authmessage               
		return @ordersData[36]
	end

	def get_added_ip          
		return @ordersData[37]
	end

	def get_updated_ip     
		return @ordersData[38]
	end

	def get_sourceind       
		return @ordersData[39]
	end

	def get_pflags               
		return @ordersData[40]
	end

	def get_nflags               
		return @ordersData[41]
	end

	def get_campaignid   
		return @ordersData[42]
	end

	def get_orderdate      
		return @ordersData[43]
	end

	def get_cancelreason 	
		return @ordersData[44]
	end

    def closeDBConnection		
		@conn.logoff
	end
	
end