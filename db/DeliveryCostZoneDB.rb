require 'oci8'
require_relative 'DBConn.rb'

class DeliveryCostZoneDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)
	end	

	def set_delivery_cost_zone_data(deliverycostzoneid)
		
		deliverycostzoneQuery ="select deliverycostzoneid ," + 
				  			" active," +
				  			" to_char(added,'mm/dd/yyyy')," + 
				  			" added_csr," +      
				  			" added_ip," +          
				  			" country," +           
				  			" deliverycategory," +
				  			" deliverycostcontainerid," +
				  			" fromzone," +       
				  			" tozone," +              
				  			" to_char(updated,'mm/dd/yyyy')," +           
				  			" updated_csr," + 
				  			" updated_ip" + 
				" from ymax.deliverycostzone" +
				" where deliverycostzoneid = #{deliverycostzoneid}"
				
		dcz = @conn.exec(deliverycostzoneQuery)
		
		@deliverycostzoneData = dcz.fetch()
		
		return @deliverycostzoneData
	end
	
	def get_deliverycostzoneid
		return @deliverycostzoneData[0]
	end

	def get_active
		return @deliverycostzoneData[1]
	end

	def get_added 
		return @deliverycostzoneData[2]
	end

	def get_added_csr      
		return @deliverycostzoneData[3]
	end

	def get_added_ip          
		return @deliverycostzoneData[4]
	end

	def get_country           
		return @deliverycostzoneData[5]
	end

	def get_deliverycategory
		return @deliverycostzoneData[6]
	end

	def get_deliverycostcontainerid
		return @deliverycostzoneData[7]
	end

	def get_fromzone       
		return @deliverycostzoneData[8]
	end

	def get_tozone              
		return @deliverycostzoneData[9]
	end

	def get_updated           
		return @deliverycostzoneData[10]
	end

	def get_updated_csr 
		return @deliverycostzoneData[11]
	end

	def get_updated_ip 
		return @deliverycostzoneData[12]
	end

    def closeDBConnection		
		@conn.logoff
	end
	
end
