require 'oci8'
require_relative 'DBConn.rb'

class SipEndpointDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)			
	end	

	def set_sip_endpoint_data(enumber)
	
		sipendpointQuery =	"select sip_alias,"+          
						" admin_state,"+ 
						" alerttimer,"+      
						" ann_id,"+              
						" app_token,"+      
						" app_type,"+          
						" app_ver,"+            
						" cap,"+       
						" data_status,"+ 
						" data_version,"+               
						" dev_type,"+          
						" entity_id,"+          
						" feature_set,"+   
						" iid,"+         
						" info,"+     
						" last_modified,"+              
						" last_state_updated,"+
						" last_transact,"+             
						" last_updated,"+               
						" mac_address,"+
						" to_char(modified_date,'mm/dd/yyyy')," +             
						" nortpbp,"+           
						" op_state,"+         
						" orig_sip_alias,"+             
						" orig_sip_ip,"+     
						" orig_sip_port,"+              
						" private_partition,"+     
						" pserver_id,"+      
						" quota,"+
						" read_level,"+      
						" reserved,"+          
						" rg_id,"+  
						" sip_encryption,"+            
						" sip_extension,"+               
						" sip_ip,"+  
						" sip_port,"+
						" sip_token,"+        
						" sip_ttl,"+               
						" s_desc,"+
						" telecode,"+           
						" user_domain,"+               
						" vendor_id,"+      
						" visit_instance,"+             
						" write_level"+      
					" from sip_endpoint@entice" +
					" where sip_alias ='"+enumber+"'"
	
		e = @conn.exec(sipendpointQuery)
		
		@sipendpointData = e.fetch()
		
		return @sipendpointData
	 end	 
	 
	 def get_sip_alias
	 	return @sipendpointData[0]
	 end
	 	 
	 def get_admin_state 
	 	return @sipendpointData[1]
	 end
	 
	 def get_alerttimer      
	 	return @sipendpointData[2]
	 end
	 
	 def get_ann_id              
	 	return @sipendpointData[3]
	 end
	 
	 def get_app_token      
	 	return @sipendpointData[4]
	 end
	 
	 def get_app_type          
	 	return @sipendpointData[5]
	 end
	 
	 def get_app_ver            
	 	return @sipendpointData[6]
	 end
	 
	 def get_cap       
	 	return @sipendpointData[7]
	 end
	 
	 def get_data_status 
	 	return @sipendpointData[8]
	 end
	 
	 def get_data_version               
	 	return @sipendpointData[9]
	 end
	 
	 def get_dev_type          
	 	return @sipendpointData[10]
	 end
	 
	 def get_entity_id          
	 	return @sipendpointData[11]
	 end
	 
	 def get_feature_set   
	 	return @sipendpointData[12]
	 end
	 
	 def get_iid         
	 	return @sipendpointData[13]
	 end
	 
	 def get_info     
	 	return @sipendpointData[14]
	 end
	 
	 def get_last_modified              
	 	return @sipendpointData[15]
	 end
	 
	 def get_last_state_updated
	 	return @sipendpointData[16]
	 end
	 
	 def get_last_transact             
	 	return @sipendpointData[17]
	 end
	 
	 def get_last_updated               
	 	return @sipendpointData[18]
	 end
	 
	 def get_mac_address
	 	return @sipendpointData[19]
	 end
	 
	 def get_modified_date             
	 	return @sipendpointData[20]
	 end
	 
	 def get_nortpbp           
	 	return @sipendpointData[21]
	 end
	 
	 def get_op_state          
	 	return @sipendpointData[22]
	 end
	 
	 def get_orig_sip_alias             
	 	return @sipendpointData[23]
	 end
	 
	 def get_orig_sip_ip     
	 	return @sipendpointData[24]
	 end
	 
	 def get_orig_sip_port              
	 	return @sipendpointData[25]
	 end
	 
	 def get_private_partition     
	 	return @sipendpointData[26]
	 end
	 
	 def get_pserver_id      
	 	return @sipendpointData[27]
	 end
	 
	 def get_quota
	 	return @sipendpointData[28]
	 end
	 
	 def get_read_level      
	 	return @sipendpointData[29]
	 end
	 
	 def get_reserved          
	 	return @sipendpointData[30]
	 end
	 
	 def get_rg_id  
	 	return @sipendpointData[31]
	 end
	 
	 def get_sip_encryption            
	 	return @sipendpointData[32]
	 end
	 
	 def get_sip_extension               
	 	return @sipendpointData[33]
	 end
	 
	 def get_sip_ip  
	 	return @sipendpointData[34]
	 end
	 
	 def get_sip_port
	 	return @sipendpointData[35]
	 end
	 
	 def get_sip_token        
	 	return @sipendpointData[36]
	 end
	 
	 def get_sip_ttl               
	 	return @sipendpointData[37]
	 end
	 
	 def get_s_desc
	 	return @sipendpointData[38]
	 end
	 
	 def get_telecode           
	 	return @sipendpointData[39]
	 end
	 
	 def get_user_domain               
	 	return @sipendpointData[40]
	 end
	 
	 def get_vendor_id      
	 	return @sipendpointData[41]
	 end
	 
	 def get_visit_instance             
	 	return @sipendpointData[42]
	 end
	 
	 def get_write_level    
	 	return @sipendpointData[43]
	 end
	  
    def closeDBConnection		
		@conn.logoff
	end
	
end