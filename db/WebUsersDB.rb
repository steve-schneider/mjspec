require 'oci8'
require_relative 'DBConn.rb'

class WebUsersDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end	

	def set_webusers_data(userId)
						  
	webusersQuery = "select last_login, "+
					"inactive_lock_days, "+
					"lock_after, "+
					"userid, "+
					"username, "+
					"email, "+
					"password, "+
					"name, "+
					"active, "+
					"timeout, "+
					"internal, "+
					"to_char(added_dt,'mm/dd/yyyy'), "+
					"added_usr, "+
					"to_char(updated_dt,'mm/dd/yyyy'), "+
					"updated_usr, "+
					"subnet, "+
					"to_char(localid), "+
					"to_char(updated_pswd,'mm/dd/yyyy'), "+
					"to_char(pswd_exp), "+
					"lp_nickname, "+
					"to_char(added,'mm/dd/yyyy'), "+
					"added_csr, "+
					"added_ip, "+
					"to_char(updated,'mm/dd/yyyy'), "+
					"updated_csr, "+
					"updated_ip, "+
					"to_char(historylength) "+        
					"from ymax.webusers "+
					"where userid = #{userId}"
				
		r = @conn.exec(webusersQuery)
		
		@webusersData = r.fetch()
		
		return @webusersData
	end
	
	def get_last_login
	  return @webusersData[0]
	end
				
	def get_inactive_lock_days
	  return @webusersData[1]
	end
				
	def get_lock_after
	  return @webusersData[2]
	end
				
	def get_userid
	  return @webusersData[3]
	end
				
	def get_username
	  return @webusersData[4]
	end
				
	def get_email
	  return @webusersData[5]
	end
				
	def get_password
	  return @webusersData[6]
	end
				
	def get_name
	  return @webusersData[7]
	end
				
	def get_active
	  return @webusersData[8]
	end
				
	def get_timeout
	  return @webusersData[9]
	end
				
	def get_internal
	  return @webusersData[10]
	end
				
	def get_added_dt
	  return @webusersData[11]
	end
				
	def get_added_usr
	  return @webusersData[12]
	end
				
	def get_updated_dt
	  return @webusersData[13]
	end
				
	def get_updated_usr
	  return @webusersData[14]
	end
				
	def get_subnet
	  return @webusersData[15]
	end
				
	def get_localid
	  return @webusersData[16]
	end
				
	def get_updated_pswd
	  return @webusersData[17]
	end
				
	def get_pswd_exp
	  return @webusersData[18]
	end
				
	def get_lp_nickname
	  return @webusersData[19]
	end
				
	def get_added
	  return @webusersData[20]
	end
				
	def get_added_csr
	  return @webusersData[21]
	end
				
	def get_added_ip
	  return @webusersData[22]
	end
				
	def get_updated
	  return @webusersData[23]
	end
				
	def get_updated_csr
	  return @webusersData[24]
	end
				
	def get_updated_ip
	  return @webusersData[25]
	end
				
	def get_historylength
	  return @webusersData[26]
	end
  
	def closeDBConnection		
		@conn.logoff
	end
	
end