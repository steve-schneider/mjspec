require 'oci8'
require_relative 'DBConn.rb'

class InvoiceDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)			
	end	

	def set_invoice_records(orderid)
		 
		@invoiceRecords = Array.new
		 
		invoiceRecsQuery =  "select invoiceid,"+              
			 				" accountid,"+      
							" orderid,"+             
							" paymentsid,"+    
							" invoice_type,"+ 
							" to_char(invoice_date,'mm/dd/yyyy'),"+
							" to_char(amount),"+           
							" description,"+    
							" to_char(balance),"+            
					  		" to_char(invoices),"+           
					  		" to_char(payments),"+        
					  		" to_char(refunds),"+            
					  		" to_char(debits),"+ 
					  		" to_char(credits),"+
					  		" to_char(due_date,'mm/dd/yyyy'),"+
					  		" csr,"+       
					  		" to_char(added,'mm/dd/yyyy'),"+ 
					  		" added_csr,"+
					  		" to_char(updated,'mm/dd/yyyy'),"+           
					  		" updated_csr,"+ 
					  		" active,"+
					  		" added_ip,"+
					  		" updated_ip"+    
							" from ymax.invoice" +
							" where orderid = #{orderid}"
	
 				 
		 @conn.exec(invoiceRecsQuery) do |r| @invoiceRecords.push(r.join(',')) end		  	    
		
	   return @invoiceRecords		 
		 
	end	

	def set_invoice_data(invoiceid)
	
		invoiceQuery ="select invoiceid,"+              
					  " accountid,"+      
					  " orderid,"+             
					  " paymentsid,"+    
					  " invoice_type,"+ 
					  " to_char(invoice_date,'mm/dd/yyyy'),"+
					  " to_char(amount),"+           
					  " description,"+    
					  " to_char(balance),"+            
					  " to_char(invoices),"+           
					  " to_char(payments),"+        
					  " to_char(refunds),"+            
					  " to_char(debits),"+ 
					  " to_char(credits),"+
					  " to_char(due_date,'mm/dd/yyyy'),"+
					  " csr,"+       
					  " to_char(added,'mm/dd/yyyy'),"+ 
					  " added_csr,"+
					  " to_char(updated,'mm/dd/yyyy'),"+           
					  " updated_csr,"+ 
					  " active,"+
					  " added_ip,"+
					  " updated_ip"+    
					" from ymax.invoice" +
					" where invoiceid ='"+invoiceid+"'"
	
		i = @conn.exec(invoiceQuery)
		
		@invoiceData = i.fetch()
		
		return @invoiceData
	 end	 
	 
	 def get_invoiceid
	 	return @invoiceData[0]
	 end
        
	 def get_accountid      
	 	return @invoiceData[1]
	 end
        
	 def get_orderid             
	 	return @invoiceData[2]
	 end
        
	 def get_paymentsid    
	 	return @invoiceData[3]
	 end
        
	 def get_invoice_type 
	 	return @invoiceData[4]
	 end
        
	 def get_invoice_date
	 	return @invoiceData[5]
	 end
        
	 def get_amount           
	 	return @invoiceData[6]
	 end
        
	 def get_description    
	 	return @invoiceData[7]
	 end
        
	 def get_balance            
	 	return @invoiceData[8]
	 end
        
	 def get_invoices           
	 	return @invoiceData[9]
	 end
        
	 def get_payments        
	 	return @invoiceData[10]
	 end
        
	 def get_refunds            
	 	return @invoiceData[11]
	 end
        
	 def get_debits 
	 	return @invoiceData[12]
	 end
        
	 def get_credits
	 	return @invoiceData[13]
	 end
        
	 def get_due_date
	 	return @invoiceData[14]
	 end
        
	 def get_csr       
	 	return @invoiceData[15]
	 end
        
	 def get_added 
	 	return @invoiceData[16]
	 end
        
	 def get_added_csr
	 	return @invoiceData[17]
	 end
        
	 def get_updated           
	 	return @invoiceData[18]
	 end
        
	 def get_updated_csr 
	 	return @invoiceData[19]
	 end
        
	 def get_active
	 	return @invoiceData[20]
	 end
        
	 def get_added_ip
	 	return @invoiceData[21]
	 end
        
	 def get_updated_ip
	 	return @invoiceData[22]
	 end
        
    def closeDBConnection		
		@conn.logoff
	end
	
end