require 'oci8'
require_relative 'DBConn.rb'

class PortedDidsDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end	

	def set_porteddids_data(porteddidsid)
						  
		porteddidsQuery ="select to_char(portout_reason), "+
								"ccna, "+
								"to_char(id), "+
								"ported_number, "+
								"active_port, "+
								"to_char(ponid), "+
								"to_char(accountid), "+
								"portedfrom_spid, "+
								"portedto_spid, "+
								"to_char(portin_date,'mm/dd/yyyy'), "+
								"to_char(portout_date,'mm/dd/yyyy'), "+
								"state, "+
								"lata, "+
								"to_char(added,'mm/dd/yyyy'), "+
								"added_csr, "+
								"added_ip, "+
								"to_char(updated,'mm/dd/yyyy'), "+
								"updated_csr, "+
								"updated_ip "+         
						 "from ymax.porteddids " +
						 "where id = #{porteddidsid}"
				 
		r = @conn.exec(porteddidsQuery)
		
		@porteddidsData = r.fetch()
		
		return @porteddidsData
	end
	
	def get_portout_reason
	  return @porteddidsData[0]
	end
				
	def get_ccna
	  return @porteddidsData[1]
	end
				
	def get_id
	  return @porteddidsData[2]
	end
				
	def get_ported_number
	  return @porteddidsData[3]
	end
				
	def get_active_port
	  return @porteddidsData[4]
	end
				
	def get_ponid
	  return @porteddidsData[5]
	end
				
	def get_accountid
	  return @porteddidsData[6]
	end
				
	def get_portedfrom_spid
	  return @porteddidsData[7]
	end
				
	def get_portedto_spid
	  return @porteddidsData[8]
	end
				
	def get_portin_date
	  return @porteddidsData[9]
	end
				
	def get_portout_date
	  return @porteddidsData[10]
	end
				
	def get_state
	  return @porteddidsData[11]
	end
				
	def get_lata
	  return @porteddidsData[12]
	end
				
	def get_added
	  return @porteddidsData[13]
	end
				
	def get_added_csr
	  return @porteddidsData[14]
	end
				
	def get_added_ip
	  return @porteddidsData[15]
	end
				
	def get_updated
	  return @porteddidsData[16]
	end
				
	def get_updated_csr
	  return @porteddidsData[17]
	end
				
	def get_updated_ip
	  return @porteddidsData[18]
	end
  
    def closeDBConnection		
		@conn.logoff
	end
	
end