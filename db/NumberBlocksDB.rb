require 'oci8'
require_relative 'DBConn.rb'

class NumberBlocksDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)
	end	

	def set_numberblocks_data(numberblocksid)
		
		numberblocksQuery ="select numberblocksid," + 
			  			" country," +        
			  			" state," +          
			  			" area," +           
			  			" block," +          
			  			" total," +          
			  			" free," +           
			  			" to_char(added,'mm/dd/yyyy')," +          
			  			" added_csr," +      
			  			" to_char(updated,'mm/dd/yyyy')," +        
			  			" updated_csr," +    
			  			" v_total," +        
			  			" v_free," +         
			  			" status," +         
			  			" siteid," +          
			  			" v_coord," +        
			  			" h_coord," +        
			  			" lon," +            
			  			" lat," +           
			  			" lata," +           
			  			" retail_n_cnt," +   
			  			" retail_f_cnt," +   
			  			" retail_y_cnt," +   
			  			" retail_p_cnt," +   
			  			" retail_c_cnt," +   
			  			" retail_t_cnt," +   
			  			" retail_v_cnt," +   
			  			" retail_a_cnt," +   
			  			" switchid," +       
			  			" ratecenter," +     
			  			" wholesale_n_cnt," +
			  			" wholesale_f_cnt," +
			  			" wholesale_y_cnt," +
			  			" wholesale_p_cnt," +
			  			" wholesale_c_cnt," +
			  			" wholesale_t_cnt," +
			  			" wholesale_v_cnt," +
			  			" wholesale_a_cnt," +
			  			" added_ip," +       
			  			" updated_ip," +     
			  			" retail_o_cnt," +   
			  			" retail_s_cnt," +   
			  			" retail_h_cnt," +   
			  			" retail_x_cnt," +   
			  			" retail_i_cnt" +   
				" from ymax.numberblocks" +
				" where numberblocksid = #{numberblocksid}"
				
		n = @conn.exec(numberblocksQuery)
		
		@numberblocksData = n.fetch()
		
		return @numberblocksData
	end
	
	def get_numberblocksid
		return @numberblocksData[0]
	end

	def get_country        
		return @numberblocksData[1]
	end

	def get_state          
		return @numberblocksData[2]
	end

	def get_area           
		return @numberblocksData[3]
	end

	def get_block         
		return @numberblocksData[4]
	end

	def get_total          
		return @numberblocksData[5]
	end

	def get_free           
		return @numberblocksData[6]
	end

	def get_added          
		return @numberblocksData[7]
	end

	def get_added_csr      
		return @numberblocksData[8]
	end

	def get_updated        
		return @numberblocksData[9]
	end

	def get_updated_csr    
		return @numberblocksData[10]
	end

	def get_v_total        
		return @numberblocksData[11]
	end

	def get_v_free         
		return @numberblocksData[12]
	end

	def get_status         
		return @numberblocksData[13]
	end

	def get_siteid          
		return @numberblocksData[14]
	end

	def get_v_coord        
		return @numberblocksData[15]
	end

	def get_h_coord        
		return @numberblocksData[16]
	end

	def get_lon            
		return @numberblocksData[17]
	end

	def get_lat            
		return @numberblocksData[18]
	end

	def get_lata           
		return @numberblocksData[19]
	end

	def get_retail_n_cnt   
		return @numberblocksData[20]
	end

	def get_retail_f_cnt   
		return @numberblocksData[21]
	end

	def get_retail_y_cnt   
		return @numberblocksData[22]
	end

	def get_retail_p_cnt   
		return @numberblocksData[23]
	end

	def get_retail_c_cnt   
		return @numberblocksData[24]
	end

	def get_retail_t_cnt   
		return @numberblocksData[25]
	end

	def get_retail_v_cnt   
		return @numberblocksData[26]
	end

	def get_retail_a_cnt   
		return @numberblocksData[27]
	end

	def get_switchid       
		return @numberblocksData[28]
	end

	def get_ratecenter     
		return @numberblocksData[29]
	end

	def get_wholesale_n_cnt
		return @numberblocksData[30]
	end

	def get_wholesale_f_cnt
		return @numberblocksData[31]
	end

	def get_wholesale_y_cnt
		return @numberblocksData[32]
	end

	def get_wholesale_p_cnt
		return @numberblocksData[33]
	end

	def get_wholesale_c_cnt
		return @numberblocksData[34]
	end

	def get_wholesale_t_cnt
		return @numberblocksData[35]
	end

	def get_wholesale_v_cnt
		return @numberblocksData[36]
	end

	def get_wholesale_a_cnt
		return @numberblocksData[37]
	end

	def get_added_ip       
		return @numberblocksData[38]
	end

	def get_updated_ip     
		return @numberblocksData[39]
	end

	def get_retail_o_cnt   
		return @numberblocksData[40]
	end

	def get_retail_s_cnt   
		return @numberblocksData[41]
	end

	def get_retail_h_cnt   
		return @numberblocksData[42]
	end

	def get_retail_x_cnt   
		return @numberblocksData[43]
	end

	def get_retail_i_cnt    
		return @numberblocksData[44]
	end

    def closeDBConnection		
		@conn.logoff
	end
	
end
