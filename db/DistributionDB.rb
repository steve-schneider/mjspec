require 'oci8'
require_relative 'DBConn.rb'

class DistributionDB

	def initialize		
	     @conn = OCI8.new(CONNECTSTRING)  
	end	

	def set_distribution_data(distributionid)

		distributionQuery = "select distributionid," +
		             	    " invoiceid," +
                       		" accountid," +
                        	" orderid," +
                        	" referenceid," +
                        	" to_char(trunc(invoice_date), 'mm/dd/yyyy')," +
                        	" category" +
                        	" amount," +                    
                        	" glaccount" +
                        	" csr," +
                        	" to_char(trunc(added), 'mm/dd/yyyy')," +
                        	" added_csr," + 
                        	" updated_csr," +
                        	" active," +
                        	" added_ip," +
                        	" to_char(trunc(updated), 'mm/dd/yyyy')," +
                        	" updated_ip" +
					" from  ymax.distribution" +
					" where distributionid = #{distributionid}" 			

		d = @conn.exec(distributionQuery)

		@distributionData = d.fetch()
		
		return @distributionData
	end

	def get_distributionid
		return @distributionData[0]
	end
	
	def get_invoiceid
	   return @distributionData[1]
	end
	
	def get_accountid
	   return @distributionData[2]
	end
	
	def get_orderid
	   return @distributionData[3]
	end

	def get_referenceid
	   return @distributionData[4]
	end
	
	def get_invoice_date
	   return @distributionData[5]
	end

	def get_category
	   return @distributionData[6]
	end
	
	def get_amount
	   return @distributionData[7]
	end

	def get_glaccount
	   return @distributionData[8]
	end
	
	def get_csr
	   return @distributionData[9]
	end

	def get_added
	   return @distributionData[10]
	end
	
	def get_added_csr
	   return @distributionData[11]
	end
	
	def get_updated_csr
	   return @distributionData[12]
	end

	def get_active
	   return @distributionData[13]
	end
	
	def get_added_ip
	   return @distributionData[14]
	end
	
	def get_updated
	   return @distributionData[15]
	end
	
	def get_updated_ip
	   return @distributionData[16]
	end
	
    def closeDBConnection		
		@conn.logoff
	end
end
