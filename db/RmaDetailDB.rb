require 'oci8'
require_relative 'DBConn.rb'

class RmaDetailDB
    
	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end
	
	def set_rma_detail_data(rmadetailid)

    rmadetailQuery = "select to_char(Processed_date,'mm/dd/yyyy'), "+
                     "updated_ip, "+
					 "added_ip, "+
					 "updated_csr, "+
					 "to_char(updated,'mm/dd/yyyy'), "+
					 "added_csr, "+
					 "to_char(added,'mm/dd/yyyy'), "+                        
					 "override, "+
					 "processed, "+
					 "requested, "+
					 "productcodeid, "+
					 "orderid, "+
					 "accountid, "+
					 "rmaid, "+
					 "rmadetailid "+
                     " from ymax.rmadetail where rmadetailid = #{rmadetailid}"
        
        
        r = @conn.exec(rmadetailQuery)
		
		@rmadetailData = r.fetch()
		
		return @rmadetailData
                    
    end
                    
	def get_processed_date
	  return @rmadetailData[0]
	end
	 
	def get_updated_ip
	  return @rmadetailData[1]
	end
	 
	def get_added_ip
	  return @rmadetailData[2]
	end
	
	def get_updated_csr
	  return @rmadetailData[3]
	end
	
	def get_updated
	  return @rmadetailData[4]
	end
	
	def get_added_csr
	  return @rmadetailData[5]
	 end
	 
	def get_added
	  return @rmadetailData[6]
	end
	 
	def get_override
	  return @rmadetailData[7]
	end
	
	def get_processed
	  return @rmadetailData[8]
	end
	 
	def get_requested
	  return @rmadetailData[9]
	end
	
	def get_productcodeid
	  return @rmadetailData[10]
	end
	 
	def get_orderid
	  return @rmadetailData[11]
	end
	 
	def get_accountid
	  return @rmadetailData[12]
	end
	
	def get_rmaid
	  return @rmadetailData[13]
	end
	
	def get_rmadetailid
	  return @rmadetailData[14]
	end

    def closeDBConnection		
		@conn.logoff
	end
end
    