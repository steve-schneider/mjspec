require 'oci8'
require_relative 'DBConn.rb'

class EmailAddressDB

	def initialize		
		@conn = OCI8.new(CONNECTSTRING)		
	end	

	def set_email_address_data(emailaddressid)

		emailaddressQuery = "select emailaddressid," +
							" email_type," + 
							" accountid," +
							" emailaddr," + 
							" emailkey," +
							" password," +
							" marketing," +
							" active," + 
							" to_char(trunc(order_date), 'mm/dd/yyyy')," + 
							" order_csr," +
							" order_ipaddr," +
							" order_tracking_no," +
							" to_char(trunc(added), 'mm/dd/yyyy')," +
							" added_csr," +
							" to_char(trunc(updated), 'mm/dd/yyyy')," +
							" updated_csr," +
							" to_char(trunc(verified), 'mm/dd/yyyy')," +
							" verified_flag," +
							" added_ip," + 
							" updated_ip" +
					" from ymax.emailaddress" +
					" where emailaddressid = #{emailaddressid}"
						
		e = @conn.exec(emailaddressQuery)
		
		@emailaddressData = e.fetch()
		
		return @emailaddressData
	 end
	
	 def get_emailaddressid
	 	return @emailaddressData[0]
	 end
	 
	 def get_email_type
	 	return @emailaddressData[1]
	 end
	 
	 def get_accountid
	 	return @emailaddressData[2]
	 end
	 
	 def get_emailaddr
	 	return @emailaddressData[3]
	 end

	 def get_emailkey
	 	return @emailaddressData[4]
	 end
	 
	 def get_password
	 	return @emailaddressData[5]
	 end

	 def get_marketing
	 	return @emailaddressData[6]
	 end
	 
	 def get_active
	 	return @emailaddressData[7]
	 end

	 def get_order_date
	 	return @emailaddressData[8]
	 end
	 
	 def get_order_csr
	 	return @emailaddressData[9]
	 end
	 
	 def get_order_ipaddr
	 	return @emailaddressData[10]
	 end
	 
	 def get_order_tracking_no
	 	return @emailaddressData[11]
	 end
	 
	 def get_added
	 	return @emailaddressData[12]
	 end
	 
	 def get_added_csr
	 	return @emailaddressData[13]
	 end
	 
	 def get_updated
	 	return @emailaddressData[14]
	 end
	 
	 def get_updated_csr
	 	return @emailaddressData[15]
	 end
	 
	 def get_verified
	 	return @emailaddressData[16]
	 end
	 
	 def get_verified_flag
	 	return @emailaddressData[17]
	 end
	 
	 def get_added_ip
	 	return @emailaddressData[18]
	 end
	 
	 def get_updated_ip
	 	return @emailaddressData[19]
	 end
	 
    def closeDBConnection		
		@conn.logoff
	end
	
end