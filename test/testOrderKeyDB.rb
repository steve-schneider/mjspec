require_relative '../db/OrderKeyDB.rb'
require "rspec"
require "date"
include RSpec::Expectations

valDB = OrderKeyDB.new
valDB.set_order_key_data('34459')

p 'ORDERKEYID' 
p valDB.get_orderkeyid 

p 'ORDERID'
p valDB.get_orderid

p 'ORDERKEY' 
p valDB.get_orderkey

p 'ORDER_NUMBER' 
p valDB.get_order_number

p 'SHIPDATE'
p valDB.get_shipdate

p 'ACTIONID'
p valDB.get_actionid

p 'EMAILDATE' 
p valDB.get_emaildate

p 'EXPIREDATE' 
p valDB.get_expiredate

p 'CLAIMED'
p valDB.get_claimed

p 'CLAIM_DATE'
p valDB.get_claim_date

p 'INBOUND_ORDERID'
p valDB.get_inbound_orderid

p 'CSR'
p valDB.get_csr

p 'ADDED'
p valDB.get_added

p 'ADDED_CSR'
p valDB.get_added_csr

p 'UPDATED'  
p valDB.get_updated

p 'UPDATED_CSR'
p valDB.get_updated_csr

p 'ADDED_IP'
p valDB.get_added_ip

p 'UPDATED_IP'
p valDB.get_updated_ip

p 'GIFTJACK_ORDERID'
p valDB.get_giftjack_orderid

p 'PRODUCTCODEID'
p valDB.get_productcodeid


valDB.closeDBConnection

