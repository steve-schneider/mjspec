require_relative '../db/InvoiceDB.rb'
require "rspec"
require "date"


valDB = InvoiceDB.new
valDB.set_invoice_data('5984871')

p 'get_invoiceid'
p valDB.get_invoiceid
	 
p 'get_accountid'      
p valDB.get_accountid

p 'get_orderid'             
p valDB.get_orderid

p 'get_paymentsid'    
p valDB.get_paymentsid

p 'get_invoice_type' 
p valDB.get_invoice_type

p 'get_invoice_date'
p valDB.get_invoice_date

p 'get_amount'           
p valDB.get_amount

p 'get_description'    
p valDB.get_description

p 'get_balance'            
p valDB.get_balance

p 'get_invoices'           
p valDB.get_invoices

p 'get_payments'        
p valDB.get_payments

p 'get_refunds'            
p valDB.get_refunds

p 'get_debits' 
p valDB.get_debits

p 'get_credits'
p valDB.get_credits

p 'get_due_date'
p valDB.get_due_date

p 'get_csr'       
p valDB.get_csr

p 'get_added' 
p valDB.get_added

p 'get_added_csr'
p valDB.get_added_csr

p 'get_updated'           
p valDB.get_updated

p 'get_updated_csr' 
p valDB.get_updated_csr

p 'get_active'
p valDB.get_active

p 'get_added_ip'
p valDB.get_added_ip

p 'get_updated_ip'
p valDB.get_updated_ip

valDB.closeDBConnection

