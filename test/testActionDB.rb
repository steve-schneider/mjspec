require_relative '../db/ActionDB.rb'
require "rspec"
require "date"

valDB = ActionDB.new
valDB.set_action_data('17181438')

p 'get_actionid'
p valDB.get_actionid

p 'get_accountid'
p valDB.get_accountid

p 'get_cpnid'           
p valDB.get_cpnid

p 'get_calling_party_no' 
p valDB.get_calling_party_no

p 'get_act'             
p valDB.get_act

p 'get_arg'            
p valDB.get_arg

p 'get_send_date'      
p valDB.get_send_date

p 'get_emailaddr'      
p valDB.get_emailaddr

p 'get_orderid'         
p valDB.get_orderid

p 'get_intarg2'          
p valDB.get_intarg2

p 'get_intarg3'          
p valDB.get_intarg3

p 'get_strarg1'        
p valDB.get_strarg1

p 'get_strarg2'        
p valDB.get_strarg2

p 'get_strarg3'        
p valDB.get_strarg3

p 'get_strarg4'        
p valDB.get_strarg4

p 'get_strarg5'        
p valDB.get_strarg5

p 'get_amount1'        
p valDB.get_amount1

p 'get_amount2'        
p valDB.get_amount2

p 'get_error_type'     
p valDB.get_error_type

p 'get_suberror_type' 
p valDB.get_suberror_type

p 'get_added'        
p valDB.get_added

p 'get_added_csr'    
p valDB.get_added_csr

p 'get_updated'    
p valDB.get_updated

p 'get_updated_csr' 
p valDB.get_updated_csr

p 'get_active'       
p valDB.get_active

p 'get_added_ip'     
p valDB.get_added_ip

p 'get_updated_ip'  
p valDB.get_updated_ip

p 'get_bulk'      
p valDB.get_bulk

p 'get_language'	 
p valDB.get_language

valDB.closeDBConnection
