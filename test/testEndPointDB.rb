require_relative '../db/EndPointDB.rb'
require "rspec"
require "date"



valDB = EndPointDB.new
valDB.set_endpoint_data('2294015')

p 'get_endpointid'
p valDB.get_endpointid
p 'get_accountid'
p valDB.get_accountid
p 'get_subscriberid'
p valDB.get_subscriberid
p 'get_cpnid'
p valDB.get_cpnid
p 'get_enumber'
p valDB.get_enumber
p 'get_rgid'
p valDB.get_rgid
p 'get_admin_state'
p valDB.get_admin_state
p 'get_added'
p valDB.get_added
p 'get_password'
p valDB.get_password
p 'get_added_csr'
p valDB.get_added_csr
p 'get_updated'
p valDB.get_updated
p 'get_updated_csr'
p valDB.get_updated_csr
p 'get_added_ip'
p valDB.get_added_ip
p 'get_updated_ip'
p valDB.get_updated_ip

p 'get_subtype'
p valDB.get_subtype
p 'get_sip_proxy_host'
p valDB.get_sip_proxy_host
p 'get_sip_proxy_port'
p valDB.get_sip_proxy_port
p 'get_spoof'
p valDB.get_spoof
p 'get_locationid'
p valDB.get_locationid
p 'get_serial_no'
p valDB.get_serial_no
p 'get_position'
p valDB.get_position
p 'get_subscriptionid'
p valDB.get_subscriptionid

p 'get_device_type'
p valDB.get_device_type
p 'get_serviceplan_productcodeid'
p valDB.get_serviceplan_productcodeid
p 'get_routing'
p valDB.get_routing
p 'get_last_password_reset'
p valDB.get_last_password_reset
p 'get_treatments'
p valDB.get_treatments
p 'get_vmtimer'
p valDB.get_vmtimer
p 'get_calling_party_no'
p valDB.get_calling_party_no

p 'get_usagebits'
p valDB.get_usagebits
p 'get_cpn_type'
p valDB.get_cpn_type
p 'get_intrado_arg'
p valDB.get_intrado_arg
p 'get_e911_color'
p valDB.get_e911_color
p 'get_e911_description'
p valDB.get_e911_description
p 'get_e911_verified'
p valDB.get_e911_verified
p 'get_e911_status'
p valDB.get_e911_status

valDB.closeDBConnection
