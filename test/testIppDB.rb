require_relative '../db/IppDB.rb'


valDB = IppDB.new
valDB.set_ipp_data(11879040)

p 'get_ippid'
p valDB.get_ippid
p 'get_accountid' 
p valDB.get_accountid           
p 'get_orderid'
p valDB.get_orderid             
p 'get_orderdetailid'
p valDB.get_orderdetailid
p 'get_productcodeid'
p valDB.get_productcodeid
p 'get_emailaddressid'
p valDB.get_emailaddressid
p 'get_quantity'
p valDB.get_quantity
p 'get_claimed'
p valDB.get_claimed 
p 'get_claim_code'
p valDB.get_claim_code
p 'get_claim_accountid'
p valDB.get_claim_accountid
p 'get_claim_rsubscriberid'
p valDB.get_claim_rsubscriberid
p 'get_claim_date'
p valDB.get_claim_date
p 'get_email_sent'
p valDB.get_email_sent
p 'get_error_type'
p valDB.get_error_type
p 'get_csr'
p valDB.get_csr
p 'get_added'
p valDB.get_added
p 'get_added_csr'
p valDB.get_added_csr
p 'get_updated'
p valDB.get_updated
p 'get_updated_csr'
p valDB.get_updated_csr
p 'get_startservice'
p valDB.get_startservice
p 'get_invoiceid'
p valDB.get_invoiceid
p 'get_invoice_date'
p valDB.get_invoice_date
p 'get_adjustmentid'
p valDB.get_adjustmentid
p 'get_adjustment_date'
p valDB.get_adjustment_date
p 'get_added_ip'
p valDB.get_added_ip
p 'get_updated_ip'
p valDB.get_updated_ip
p 'get_called_area'
p valDB.get_called_area

valDB.closeDBConnection

