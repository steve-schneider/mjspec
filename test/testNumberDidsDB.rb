require_relative '../db/NumberDidsDB.rb'
require "rspec"
require "date"

valDB = NumberDidsDB.new
valDB.set_numberdids_data('5402752')


p 'get_numberdidsid'
p valDB.get_numberdidsid

p 'get_did_number'           
p valDB.get_did_number

p 'get_numberblocksid'        
p valDB.get_numberblocksid

p 'get_vanity'                
p valDB.get_vanity

p 'get_reserved'              
p valDB.get_reserved

p 'get_added'                 
p valDB.get_added

p 'get_added_csr'             
p valDB.get_added_csr

p 'get_updated'               
p valDB.get_updated

p 'get_updated_csr'           
p valDB.get_updated_csr

p 'get_status'                
p valDB.get_status

p 'get_reserved_serial'
p valDB.get_reserved_serial

p 'get_market'                
p valDB.get_market

p 'get_added_ip'              
p valDB.get_added_ip

p 'get_updated_ip'            
p valDB.get_updated_ip

p 'get_accountid'
p valDB.get_accountid

valDB.closeDBConnection




   