require_relative '../db/OrdersDB.rb'
require "rspec"
require "date"

valDB = OrdersDB.new
orderPK=valDB.get_order_pk(11894953)

puts "order PK is #{orderPK.to_i}"

valDB.setOrdersData(orderPK)

p 'orderid'
p valDB.get_orderid

p 'inbound_orderid'      
p valDB.get_inbound_orderid

p 'accountid'      
p valDB.get_accountid

p 'emailaddressid'           
p valDB.get_emailaddressid

p 'creditcardid' 
p valDB.get_creditcardid

p 'order_number'            
p valDB.get_order_number

p 'order_date'    
p valDB.get_order_date

p 'order_time'    
p valDB.get_order_time

p 'order_status'               
p valDB.get_order_status

p 'statusdate'     
p valDB.get_statusdate

p 'onhold_reason'         
p valDB.get_onhold_reason

p 'promo_code' 
p valDB.get_promo_code

p 'authcode'        
p valDB.get_authcode

p 'subtotal'          
p valDB.get_subtotal

p 'shipping'          
p valDB.get_shipping

p 'taxes'  
p valDB.get_taxes

p 'total'  
p valDB.get_total

p 'taxrateid'        
p valDB.get_taxrateid

p 'taxrate'             
p valDB.get_taxrate

p 'csr'       
p valDB.get_csr

p 'added' 
p valDB.get_added

p 'added_csr'      
p valDB.get_added_csr

p 'updated'           
p valDB.get_updated

p 'updated_csr' 
p valDB.get_updated_csr

p 'adj_subtotal'               
p valDB.get_adj_subtotal

p 'adj_taxes'        
p valDB.get_adj_taxes

p 'adj_shipping' 
p valDB.get_adj_shipping

p 'adj_total'        
p valDB.get_adj_total

p 'adj_sum_total'           
p valDB.get_adj_sum_total

p 'minpaydate'   
p valDB.get_minpaydate

p 'unpaid'              
p valDB.get_unpaid

p 'payment'           
p valDB.get_payment

p 'result_code'  
p valDB.get_result_code

p 'last_auth_attempt' 
p valDB.get_last_auth_attempt

p 'auth_attempts'           
p valDB.get_auth_attempts

p 'auth_total'   
p valDB.get_auth_total

p 'authmessage'               
p valDB.get_authmessage

p 'added_ip'          
p valDB.get_added_ip

p 'updated_ip'     
p valDB.get_updated_ip

p 'sourceind'       
p valDB.get_sourceind

p 'pflags'               
p valDB.get_pflags

p 'nflags'               
p valDB.get_nflags

p 'campaignid'   
p valDB.get_campaignid

p 'orderdate'      
p valDB.get_orderdate

p 'cancelreason' 
p valDB.get_cancelreason

valDB.closeDBConnection
