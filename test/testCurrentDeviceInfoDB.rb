require_relative '../db/CurrentDeviceInfoDB.rb'
require "rspec"
require "date"
include RSpec::Expectations

valDB = CurrentDeviceInfoDB.new
valDB.set_current_device_info_data('57906')

p 'CURRENTDEVICEINFOID'
p valDB.get_currentdeviceinfoid

p 'SERIAL_NO'
p valDB.get_serial_no

p 'USAGE_TYPE'
p valDB.get_usage_type

p 'DEVICE_TYPE'
p valDB.get_device_type

p 'DEVICE_HW'
p valDB.get_device_hw

p 'DEVICE_MFG'
p valDB.get_device_mfg

p 'DEVICE_BRAND'
p valDB.get_device_brand

p 'DEVICE_OSVERSION'
p valDB.get_device_osversion

p 'OSNAME'
p valDB.get_osname

p 'MINIMUM_SOFTWARE_VERSION'
p valDB.get_minimum_software_version

p 'CURRENT_SOFTWARE_VERSION'
p valDB.get_current_software_version

p 'MODULE_VERSION'
p valDB.get_module_version

p 'LAST_PROVISIONING' 
p valDB.get_last_provisioning

p 'LAST_PROVISIONING_IP'
p valDB.get_last_provisioning_ip

p 'LAST_ACCOUNTID'
p valDB.get_last_accountid

p 'LAST_UPGRADE'
p valDB.get_last_upgrade

p 'PREV_PROVISIONING'
p valDB.get_prev_provisioning

p 'PREV_PROVISIONING_IP'
p valDB.get_prev_provisioning_ip

p 'PREV_ACCOUNTID'
p valDB.get_prev_accountid

p 'PREV_UPGRADE'
p valDB.get_prev_upgrade

p 'ADDED'
p valDB.get_added

p 'ADDED_CSR'
p valDB.get_added_csr

p 'ADDED_IP'
p valDB.get_added_ip

p 'UPDATED'
p valDB.get_updated

p 'UPDATED_CSR'
p valDB.get_updated_csr

p 'UPDATED_IP'
p valDB.get_updated_ip

valDB.closeDBConnection

