require_relative '../db/EmailAddressDB.rb'

valdb = EmailAddressDB.new
valdb.set_email_address_data(12100964)

p 'emailaddressid'
p valdb.get_emailaddressid
p 'email_type'
p valdb.get_email_type
p 'accountid'
p valdb.get_accountid
p 'emailaddr'
p valdb.get_emailaddr
p 'emailkey'
p valdb.get_emailkey
p 'password'
p valdb.get_password
p 'marketing'                         
p valdb.get_marketing
p 'active'
p valdb.get_active
p 'order_date'                      
p valdb.get_order_date
p 'order_csr'
p valdb.get_order_csr
p 'order_ipaddr'                    
p valdb.get_order_ipaddr
p 'order_tracking_no'                      
p valdb.get_order_tracking_no
p 'added'             
p valdb.get_added
p 'added_csr'                   
p valdb.get_added_csr
p 'updated'                     
p valdb.get_updated
p 'updated_csr'                      
p valdb.get_updated_csr
p 'verified'                        
p valdb.get_verified
p 'verified_flag'                   
p valdb.get_verified_flag
p 'added_ip'       
p valdb.get_added_ip
p 'updated_ip'                       
p valdb.get_updated_ip

valdb.closeDBConnection

