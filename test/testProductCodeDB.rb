require_relative '../db/ProductCodeDB.rb'


valDB = ProductCodeDB.new
valDB.set_product_code_data(400)

p 'shortdesc'
p valDB.get_shortdesc

p 'upccode'
p valDB.get_upccode

p 'subcategory'
p valDB.get_subcategory

p 'comments'
p valDB.get_comments

p 'initial_term'
p valDB.get_initial_term

p 'multiplier'
p valDB.get_multiplier

p 'cust_adjustable'
p valDB.get_cust_adjustable

p 'class4'
p valDB.get_class4

p 'gift_productcodeid'
p valDB.get_gift_productcodeid

p 'paynow_productcodeid'
p valDB.get_paynow_productcodeid

p 'suppress'
p valDB.get_suppress

p 'shippingcategory'
p valDB.get_shippingcategory

p 'deliverycategory'
p valDB.get_deliverycategory

p 'weight'
p valDB.get_weight

p 'max_companion_quantity'
p valDB.get_max_companion_quantity

p 'companion_productcodeid'
p valDB.get_companion_productcodeid

p 'exceed_device'
p valDB.get_exceed_device

p 'max_order_quantity'
p valDB.get_max_order_quantity

p 'e911'
p valDB.get_e911

p 'class3'
p valDB.get_class3

p 'class2'
p valDB.get_class2

p 'tangible'
p valDB.get_tangible

p 'authgroup'
p valDB.get_authgroup

p 'class1'
p valDB.get_class1

p 'referral_levels'
p valDB.get_referral_levels

p 'on_hold'
p valDB.get_on_hold

p 'installment_amount'
p valDB.get_installment_amount

p 'no_installments'
p valDB.get_no_installments

p 'downpayment'
p valDB.get_downpayment

p 'renewal_productcodeid'
p valDB.get_renewal_productcodeid

p 'feeproductcodeid'
p valDB.get_feeproductcodeid

p 'serviceplan_type'
p valDB.get_serviceplan_type

p 'renewal_price'
p valDB.get_renewal_price

p 'vanity'
p valDB.get_vanity

p 'lettername'
p valDB.get_lettername

p 'sendconfirmletter'
p valDB.get_sendconfirmletter

p 'freerushshipping'
p valDB.get_freerushshipping

p 'updated_ip'
p valDB.get_updated_ip

p 'updated'
p valDB.get_updated

p 'added_ip'
p valDB.get_added_ip

p 'added'
p valDB.get_added

p 'active'
p valDB.get_active

p 'taxproductcodeid'
p valDB.get_taxproductcodeid

p 'zipcomm_item'
p valDB.get_zipcomm_item

p 'zipcomm_grp'
p valDB.get_zipcomm_grp

p 'adjustable'
p valDB.get_adjustable

p 'updated_csr'
p valDB.get_updated_csr

p 'added_csr'
p valDB.get_added_csr

p 'sortorder'
p valDB.get_sortorder

p 'product'
p valDB.get_product

p 'trialdays'
p valDB.get_trialdays

p 'glaccount'
p valDB.get_glaccount

p 'disc_enddate'
p valDB.get_disc_enddate

p 'disc_startdate'
p valDB.get_disc_startdate

p 'disc_type'
p valDB.get_disc_type

p 'disc'
p valDB.get_disc

p 'pieces'
p valDB.get_pieces

p 'cost'
p valDB.get_cost

p 'price'
p valDB.get_price

p 'description'
p valDB.get_description

p 'name'
p valDB.get_name

p 'category'
p valDB.get_category

p 'productcode'
p valDB.get_productcode

p 'productcodeid'
p valDB.get_productcodeid

p 'display_price'
p valDB.get_display_price

p 'product_type'
p valDB.get_product_type

p 'max_lines'
p valDB.get_max_lines

p 'required_productcodeid'
p valDB.get_required_productcodeid

p 'recurring_months'
p valDB.get_recurring_months

p 'feature'
p valDB.get_feature

p 'promovalue'
p valDB.get_promovalue

p 'image'
p valDB.get_image

p 'apple_item_id'
p valDB.get_apple_item_id

p 'rgid'
p valDB.get_rgid

p 'google_productcode'
p valDB.get_google_productcode

p 'apple_productcode'
p valDB.get_apple_productcode

p 'allow_ymaxcredit'
p valDB.get_allow_ymaxcredit

valDB.closeDBConnection

