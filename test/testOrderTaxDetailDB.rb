require_relative '../db/OrderTaxDetailDB.rb'
require "rspec"
require "date"
include RSpec::Expectations

valDB = OrderTaxDetailDB.new
valDB.set_order_tax_detail_data('239034')

p 'ORDERTAXDETAILID' 
p valDB.get_ordertaxdetailid

p 'ORDERID'
p valDB.get_orderid

p 'ACCOUNTID'
p valDB.get_accountid

p 'ORDERDETAILID'
p valDB.get_orderdetailid

p 'REFERENCEID'
p valDB.get_referenceid

p 'GEOCODE'
p valDB.get_geocode

p 'INOUTCITY'
p valDB.get_inoutcity

p 'INOUTLOCAL'
p valDB.get_inoutlocal

p 'EFFDATE'
p valDB.get_effdate

p 'TAXRATE'
p valDB.get_taxrate

p 'TAXAUTH'
p valDB.get_taxauth

p 'TAXTYPE'
p valDB.get_taxtype

p 'TAXCAT'
p valDB.get_taxcat

p 'DESCRIPTION'
p valDB.get_description

p 'FEE'
p valDB.get_fee

p 'UNITTYPE'
p valDB.get_unittype

p 'TAXABLEAMT'
p valDB.get_taxableamt

p 'QUANTITY'
p valDB.get_quantity

p 'SUBTOTAL'
p valDB.get_subtotal

p 'DETAIL_TYPE'
p valDB.get_detail_type

p 'ADDED'
p valDB.get_added

p 'ADDED_CSR'
p valDB.get_added_csr

p 'ADDED_IP'
p valDB.get_added_ip

p 'UPDATED'
p valDB.get_updated

p 'UPDATED_CSR'
p valDB.get_updated_csr

p 'UPDATED_IP'
p valDB.get_updated_ip

p 'PAID_THROUGH'
p valDB.get_paid_through

p 'PAYDATE'
p valDB.get_paydate


valDB.closeDBConnection

