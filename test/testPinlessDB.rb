require_relative '../db/PinlessDB.rb'


valDB = PinlessDB.new
valDB.set_pinless_data(248)

p 'calling_party_no'
p valDB.get_calling_party_no

p 'updated_ip'
p valDB.get_updated_ip

p 'updated_csr'
p valDB.get_updated_csr

p 'updated'
p valDB.get_updated

p 'added_ip'
p valDB.get_added_ip

p 'added_csr'
p valDB.get_added_csr

p 'added'
p valDB.get_added

p 'active'
p valDB.get_active

p 'outservice'
p valDB.get_outservice

p 'inservice'
p valDB.get_inservice

p 'members'
p valDB.get_members

p 'maxusers'
p valDB.get_maxusers

p 'recordingformat'
p valDB.get_recordingformat

p 'recordingfilename'
p valDB.get_recordingfilename

p 'adminopts'
p valDB.get_adminopts

p 'opts'
p valDB.get_opts

p 'adminpin'
p valDB.get_adminpin

p 'pin'
p valDB.get_pin

p 'cpnid'
p valDB.get_cpnid

p 'accountid'
p valDB.get_accountid

p 'pinlessid'
p valDB.get_pinlessid

valDB.closeDBConnection

