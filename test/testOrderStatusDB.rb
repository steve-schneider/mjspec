require_relative '../db/OrderStatusDB.rb'
require "rspec"
require "date"
include RSpec::Expectations


valDB = OrderStatusDB.new
valDB.set_order_status_data('9221735')

p 'ORDERSTATUSID'
p valDB.get_orderstatusid

p 'ACCOUNTID'
p valDB.get_accountid

p 'ORDERID'
p valDB.get_orderid

p 'ORDER_STATUS'
p valDB.get_order_status

p 'STATUSDATE'
p valDB.get_statusdate

p 'DESCRIPTION'
p valDB.get_description

p 'CSR'  
p valDB.get_csr

p 'ADDED'
p valDB.get_added

p 'ADDED_CSR'
p valDB.get_added_csr

p 'UPDATED'
p valDB.get_updated

p 'UPDATED_CSR'
p valDB.get_updated_csr

p 'ADDED_IP'
p valDB.get_added_ip

p 'UPDATED_IP'
p valDB.get_updated_ip

valDB.closeDBConnection

