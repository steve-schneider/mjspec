require_relative '../db/WebUsersDB.rb'


valDB = WebUsersDB.new
valDB.set_webusers_data(2830)

p 'last_login'
p valDB.get_last_login

p 'inactive_lock_days'
p valDB.get_inactive_lock_days

p 'lock_after'
p valDB.get_lock_after

p 'userid'
p valDB.get_userid

p 'username'
p valDB.get_username

p 'email'
p valDB.get_email

p 'password'
p valDB.get_password

p 'name'
p valDB.get_name

p 'active'
p valDB.get_active

p 'timeout'
p valDB.get_timeout

p 'internal'
p valDB.get_internal

p 'added_dt'
p valDB.get_added_dt

p 'added_usr'
p valDB.get_added_usr

p 'updated_dt'
p valDB.get_updated_dt

p 'updated_usr'
p valDB.get_updated_usr

p 'subnet'
p valDB.get_subnet

p 'localid'
p valDB.get_localid

p 'updated_pswd'
p valDB.get_updated_pswd

p 'pswd_exp'
p valDB.get_pswd_exp

p 'lp_nickname'
p valDB.get_lp_nickname

p 'added'
p valDB.get_added

p 'added_csr'
p valDB.get_added_csr

p 'added_ip'
p valDB.get_added_ip

p 'updated'
p valDB.get_updated

p 'updated_csr'
p valDB.get_updated_csr

p 'updated_ip'
p valDB.get_updated_ip

p 'historylength'
p valDB.get_historylength



valDB.closeDBConnection

