require_relative '../db/DeliveryCostContainerDB.rb'
require "rspec"
require "date"

valDB = DeliveryCostContainerDB.new
valDB.set_delivery_cost_container_data('1')


p 'get_deliverycostcontainerid'
p valDB.get_deliverycostcontainerid

p 'get_active'
p valDB.get_active

p 'get_added'
p valDB.get_added

p 'get_added_csr'
p valDB.get_added_csr

p 'get_added_ip'
p valDB.get_added_ip

p 'get_carrier'
p valDB.get_carrier

p 'get_country'
p valDB.get_country

p 'get_cubicindicator'           
p valDB.get_cubicindicator

p 'get_dirname'
p valDB.get_dirname

p 'get_labelname'      
p valDB.get_labelname

p 'get_mailclass'        
p valDB.get_mailclass

p 'get_maxpackagequantity'
p valDB.get_maxpackagequantity

p 'get_minpackagequantity'
p valDB.get_minpackagequantity

p 'get_parcel'
p valDB.get_parcel

p 'get_pkgdepth'         
p valDB.get_pkgdepth

p 'get_pkglength'      
p valDB.get_pkglength

p 'get_pkgwidth'        
p valDB.get_pkgwidth

p 'get_updated'        
p valDB.get_updated

p 'get_updated_csr' 
p valDB.get_updated_csr

p 'get_updated_ip'     
p valDB.get_updated_ip

p 'get_usecubic'       
p valDB.get_usecubic

valDB.closeDBConnection
