require_relative '../db/CpnDB.rb'

valDB = CpnDB.new
valDB.set_cpn_data(3041941)

p 'CPNID'
p valDB.get_cpnid
p 'CPN_TYPE'
p valDB.get_cpn_type
p 'ACCOUNTID'
p valDB.get_accountid
p 'CALLING_PARTY_NO'
p valDB.get_calling_party_no
p 'CLASS_OF_SERVICE'
p valDB.get_class_of_service
p 'TYPE_OF_SERVICE'
p valDB.get_type_of_service
p 'DIAL_TONE_COMPANY_ID'                         
p valDB.get_dial_tone_company_id
p 'COMPLETION_DATE'
p valDB.get_completion_date
p 'E911_STATUS'                      
p valDB.get_e911_status
p 'E911_TCSI_CODE'
p valDB.get_e911_tcsi_code
p 'E911_TCSI_DATE'                    
p valDB.get_e911_tcsi_date
p 'E911_CONFIRM'                      
p valDB.get_e911_confirm
p 'ORDER_NO'             
p valDB.get_order_no
p 'ACTIVE'                   
p valDB.get_active
p 'ORDER_DATE'                     
p valDB.get_order_date
p 'ORDER_CSR'                      
p valDB.get_order_csr
p 'ORDER_IPADDR'                        
p valDB.get_order_ipaddr
p 'ORDER_TRACKING_NO'                   
p valDB.get_order_tracking_no
p 'ADDED'                        
p valDB.get_added
p 'ADDED_CSR'                   
p valDB.get_added_csr
p 'UPDATED'                        
p valDB.get_updated
p 'UPDATED_CSR'                     
p valDB.get_updated_csr
p 'LAST_ACCESSED'                       
p valDB.get_last_accessed
p 'OUTSERVICE'               
p valDB.get_outservice
p 'INSERVICE'        
p valDB.get_inservice
p 'ADDED_IP'       
p valDB.get_added_ip
p 'UPDATED_IP'                       
p valDB.get_updated_ip
p 'NUMBER_PRODUCTCODEID'                     
p valDB.get_number_productcodeid
p 'EMAILADDRESSID'                           
p valDB.get_emailaddressid
p 'STATE'           
p valDB.get_state
p 'COUNTRY'                       
p valDB.get_country
p 'LATA'                     
p valDB.get_lata
p 'USAGEBITS'                    
p valDB.get_usagebits
p 'OPTIONS'                     
p valDB.get_options
p 'AUTORENEWDATE'                 
p valDB.get_autorenewdate
p 'AUTORENEW'      
p valDB.get_autorenew
p 'LAST_ASSIGNED_DATE'  
p valDB.get_last_assigned_date
p 'RENEWALSTATE'                   
p valDB.get_renewalstate
p 'ENDPOINTID'                    
p valDB.get_endpointid
p 'FORWARD_NUMBER'                      
p valDB.get_forward_number
p 'VERSIONTIME'             
p valDB.get_versiontime
p 'SUBSCRIPTIONID'                     
p valDB.get_subscriptionid

valDB.closeDBConnection

