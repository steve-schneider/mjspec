require_relative '../db/LettersDB.rb'


valDB = LettersDB.new
valDB.set_letters_data(31091)

p 'lettersid'
p valDB.get_lettersid

p 'name'
p valDB.get_name

p 'printer'
p valDB.get_printer

p 'document'
p valDB.get_document

p 'text'
p valDB.get_text

p 'subject'
p valDB.get_subject

p 'from'
p valDB.get_from

p 'pg_cols'
p valDB.get_pg_cols

p 'pg_rows'
p valDB.get_pg_rows

p 'tmargin'
p valDB.get_tmargin

p 'bmargin'
p valDB.get_bmargin

p 'lmargin'
p valDB.get_lmargin

p 'rmargin'
p valDB.get_rmargin

p 'justified'
p valDB.get_justified

p 'html'
p valDB.get_html

p 'envelope'
p valDB.get_envelope

p 'active'
p valDB.get_active

p 'added'
p valDB.get_added

p 'added_csr'
p valDB.get_added_csr

p 'updated'
p valDB.get_updated

p 'updated_csr'
p valDB.get_updated_csr

p 'added_ip'
p valDB.get_added_ip

p 'updated_ip'
p valDB.get_updated_ip

p 'add_note'
p valDB.get_add_note

p 'marketing'
p valDB.get_marketing

p 'language'
p valDB.get_language


valDB.closeDBConnection

