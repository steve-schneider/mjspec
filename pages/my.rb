require_relative 'base_page.rb'
require_relative '../db/RegDB.rb'


class My < BasePage

    MYFRAME            =  { id: 'ifContent' }
    MYSHOP             =  { id: 'current' }
    MYIPPSELECTOR      =  { css: 'a.selector' }
    MYPREPAID          =  { link_text: 'PrePaid International' }

	MYLOGINUSR    	   =  { xpath: "//input[@id='inpUsr']" }
	MYLOGINPSWD    	   =  { id: "inpPsw" }
	MYLOGIN      	   =  { id: "btnLogin" }
	
	MYBUYGO            =  { xpath: "html/body/div[2]/div[2]/div/div/form/div/div[3]/div[2]/div[3]/input" }
	MYBUYEXPRESS       =  { xpath: "html/body/div[2]/div[3]/div/div/form/div/div[3]/div[2]/div[3]/input" }
	
	CPNUSUSNUM         =  { id: "addRandomNumber_btnBeginSearch_1"}
	CPNIUNDERSTAND     =  { xpath: "html/body/div[2]/div[2]/div/div/div/form/div[2]/center/div/div[2]/table/tbody/tr/td[3]/a" }
	CPNVANITY          =  { id: "addVanityNumber_btnBeginSearch_1"}
    CPNCANADIAN        =  { id: "addCanadianNumber_btnBeginSearch_1"}
    CPNSEARCVAN        =  { xpath: 'html/body/div[2]/div[2]/div/div/div/form/div[2]/div[1]/div/div[2]/input[1]'}
    CPNVANRESULTS      =  { id: 'btnNumFilter'}
    CPNCHOOSEDIGITS    =  { id: 'choose-digits'}
    CPNCUSTOMNUM       =  { id: 'freenumber'}    
    CPNVANSTATE        =  { id: 'selFilterState'}
    CPNVANAREACODE     =  { id: 'selFilterNPA'}
    CPNAVANREAEXCH     =  { id: 'selFilterNXX'}
    CPNSTATE           =  { id: 'selNumState'}
    CPNAREACODE        =  { id: 'selNumNPA'}
    CPNAREAEXCHANGE    =  { id: 'selNumNXX'}
    CPNCALLINGPN       =  { id: 'activation:cpn[-1].calling_party_no'}
    CPNSEARCHBTN       =  { id: 'btnSubmit'}
    CPNADDTOCART       =  { id: 'btnSubmitBasic'}
    
	MYBUY1YR           =  { id: "addmonths12formform_subme_1" }
	MYBUY5YR           =  { id: "addmonths60formform_subme_1" }
	MYBUYIPP5          =  { xpath: "//form[@id='ippformform']/div/div/div/ul/li[1]" }
	MYADDIPP           =  { id: "btnAddIPP" }
	MYBUYIPP20         =  { xpath: "//form[@id='ippformform']/div/div/div/ul/li[3]" }
	MYBUYIPP40         =  { xpath: "//form[@id='ippformform']/div/div/div/ul/li[4]" }
	MYCHECKOUT         =  { partial_link_text: "Checkout" }
	MYCONTINUE         =  { id: "btn1" }
	MYCVV2POPUP        =  { id: "ifPopUp_Content" }
	MYCVV2      	   =  { id: "inpPopUpCVV2" }
	MYCONTINUECVV2     =  { css: "button.medium.green.btn-lightText" }
	MYLOGOUT           =  { css: "a.mj-btn.clear-white.login" }

    def initialize(driver)       
	    super
	    visit 'http://develop.talk4free.com/my/login.html?setModeCSS=CSS1'	      
    end    
    
    def ordTot     	                  
       return @ordTot
    end
    

 def shop_my_with(jack_type,fname, num_type, renewal_type, ipp_type)
   
   	email = "STEVE.SCHNEIDER%"+fname+"@magicjack.com" 
   	
   	wait_for(5){setPushable}
   	
    wait_for(5){switchFrame MYFRAME} 	
   	
   	wait_for(5){type email, MYLOGINUSR}
   	type  'vwvwvw12', MYLOGINPSWD
   	click MYLOGIN   	
   	
   	wait_for(5){setPushable}         
   	wait_for(5){switchFrame MYFRAME}
   	
   	
	case num_type
		when 'US'
		    wait_for(300){click CPNUSUSNUM}
		    wait_for(300){click CPNIUNDERSTAND}	    	
	    	wait_for(5){click CPNADDTOCART}
	    when 'Vanity'
	        wait_for(300){click CPNVANITY}
		    wait_for(300){click CPNIUNDERSTAND}
		    wait_for(10){is_displayed? CPNSEARCVAN}		   
		    wait_for(10){type  '99999', CPNSEARCVAN}
		    wait_for(5){click CPNVANRESULTS}
       		wait_for(5){selectByText CPNVANSTATE, 'TN'}
	    	selectByText CPNAREACODE, '999'	    	
	    	selectByIndex CPNAVANREAEXCH, '3'		    		
	    	click CPNADDTOCART
	    when 'Custom'
	        wait_for(300){click CPNUSUSNUM}
		    wait_for(300){click CPNIUNDERSTAND}
	    	click CPNCHOOSEDIGITS
	    	wait_for(5){selectByText CPNSTATE, 'TN'}
	    	selectByText CPNAREACODE, '999'	   
	    	selectByIndex CPNAREAEXCHANGE, '6'	
	    	selectByIndex CPNCUSTOMNUM, '1'	
	    	click CPNADDTOCART
	    when 'Canadian'	    	
	    	wait_for(300){click CPNCANADIAN}
		    wait_for(300){click CPNIUNDERSTAND} 
	    	wait_for(5){selectByText CPNSTATE, 'Ontario'}
	    	selectByText CPNAREACODE, '999'	   
	    	selectByIndex CPNAREAEXCHANGE, '1'	
	    	click CPNADDTOCART
	    else
	    	p 'Number skipped'
	    	
	end
   	
   	case jack_type
        when 'GO'
        	wait_for(5){click MYBUYGO}
        when 'EXPRESS'
        	wait_for(5){click MYBUYEXPRESS}        	
        else
            p "Jack skipped"
    end
    
    case renewal_type
        when '1yr'
        	wait_for(5){click MYBUY1YR}
        when '5yr'
        	wait_for(5){click MYBUY5YR}        	
        else
             p "renewal skipped"
    end
    
   case ipp_type
       	when 'IPP5'
       	    wait_for(5){click MYSHOP}
       	    wait_for(5){click MYPREPAID }
       	    wait_for(5){click MYIPPSELECTOR }
        	wait_for(5){click MYBUYIPP5}
        	wait_for(5){click MYADDIPP }
        when 'IPP10'
        	wait_for(5){click MYADDIPP }
        when 'IPP20'
        	wait_for(5){click MYBUYIPP20}
        when 'IPP40'
        	wait_for(5){click MYBUYIPP40}        	
        else
             p "IPP skipped"
    end
    
   	wait_for(5){click MYCHECKOUT}
   	wait_for(5){click MYCONTINUE}
   	wait_for(5){switchFrame MYCVV2POPUP}
   	wait_for(5){type '123',MYCVV2}
   	wait_for(5){click MYCONTINUECVV2}
   	wait_for(5){switchWindow}
   	wait_for(5){click MYLOGOUT}   	
   	
   	sleep(2)
   	getDB = RegDB.new
       @ordTot = getDB.getMyOrdTot(fname)
    getDB.closeDBConnection    
   
  end
end