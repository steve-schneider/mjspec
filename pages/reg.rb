require_relative 'base_page.rb'
require_relative '../spec/reg_constants.rb'
require_relative '../db/RegDB.rb'


class Reg < BasePage

    def initialize(driver)       
	    super
	    visit 'http://steve.schneider:17Aug1958@develop.talk4free.com/reg/login.html'	
    end
    
    def setRegWorked(boolean)
    	@regWorked = boolean
    end
    
    def getRegWorked
    	return @regWorked
    end    
    
    #PARM DEFINITIONS:
    # jackType:
	# 	magicJackEXPRESS = "Generate magicJackEXPRESS"
	# 	magicJackGO = "Generate magicJackGO"
	# 	magicJackGO Mexico = "Generate magicJackGO Mexico"
	# 	2014 magicJack PLUS = "Generate 2014 magicJack PLUS"
	# 	magicJack PLUS = "Generate magicJack PLUS"
	# 	magicJack = "Generate magicJack"
	# numType:`	'US'
	#			'Vanity'
	#			'Custom'
	#			'Canadian'
    # serviceType = 'IN' will cause the checkbox to be unchecked making the device inbound only.
    #		`		'BOTH' value results in [B]oth.
	# addr911 = 'SKIP' No 911
	#			'911addressCorrect' use address from dropdown
	#			'911ADD' add a 911 address
	# inCity = 'inCityYes' 911 address is in city limits
	#			'inCItyNo' 911 address is not in city limits
	# renewType = '5YR' 5-year renewal
	#				'1YR' 1-year renewal
	#				'SKIP' Skip renewals
	# ippType = 'IPP5'  $5 Ipp
	# 			'IPP10'	$10 IPP
	#			'IPP20' $20 IPP
	#			'IPP40'	$40 IPP
	#			'SKIP' No IPP
	#newbillLocation = �YES� will cause a new billing address to be added in the payment info screen.
	#					'NO' use address from dropdown
	def register_with(jack_type, fname, actCodeNotification, numType, serviceType, addr911, inCity, renewType, ippType, newbillLocation)
        email = "STEVE.SCHNEIDER%"+fname+"@magicjack.com"
      begin   	    
	    setPushable
	    wait_for(5){click CHKEMAIL}
	    selectByText JACKTYPE, jack_type
	    selectByIndex SERIALNUM, 24
	    serNum = getSerNum SERIALNUM
	    click ALAUNCHER	   	
	  	    
		switchWindow #to set pushable
		setPushable
		closeWindow	
		
    	switchWindow 
		click ALAUNCHER
		
		switchWindow #to create account
		
    	wait_for(5){click CREATEACCOUNT}
    	if numType == 'Canadian'
			selectByText COUNTRY, 'CANADA'
		end
		
    	type fname, FIRSTNAME
    	type 'Schneider', LASTNAME 
    	
    	if jack_type == "Generate magicJackGO Mexico"
			type 'Francisco Garza Sada 2402', STREETNAME
			type 'Nuevo Obispado', LOCATION
			type 'Monterrey', CITY
			selectByText STATE, 'NL'
			type '64040', ZIP
# Can't get accented letters to be accepted
# 			type 'Carretera M�rida-Puerto Ju�rez Km. 120', STREETNAME
#			type 'Zona Hotelera de Chich�n Itz�', LOCATION
#			type 'Chich�n Itz�', CITY
#			selectByText STATE, 'YU'     
#			type '97752', ZIP    
		else
	    	if numType == 'Canadian'
				selectByText COUNTRY, 'CANADA'
				type '470 GLENRIDGE AVE', STREETNAME     
				type 'ST CATHARINES', CITY
				selectByText STATE, 'Ontario'     
				type 'L2T4C3', ZIP
			else
				type '700 2nd Ave', STREETNAME     
				type 'Nashville', CITY
				selectByText STATE, 'TN'     
				type '37210', ZIP
			end
		end
    	
		type email, EMAIL    
		type email, EMAILVERIFY   
		type 'vwvwvw12', PASSWORD    
		type 'vwvwvw12', PASSWORDVERIFY
	
	   	click CREATEACCOUNT2

    	wait_for(5){switchWindow} #to enter activation code
      
	    wait_for(5){type fname, DEVICENAME}
	    
    	#get activation code and credit card from DB
    	
    	getDB = RegDB.new
    	if actCodeNotification == 'EMAIL' then
    	   click EMAILMETHOD
    	   wait_for(5){click SENDMYCODE}
    	   actCode = getDB.getActCodeEmail(fname)
    	else
    	   click TXTMETHOD
    	   type '6152498514', TXTPHONE
    	   wait_for(5){click SENDMYCODE}
    	   actCode = getDB.getActCodeSMS(serNum)
    	end
    	
    	if numType == 'Canadian'
			newCC   = getDB.getMCNum
    	else
			newCC   = getDB.getCCNum
    	end
    	getDB.closeDBConnection 
    	    	
    	wait_for(5){type actCode, ACTCODE}
    	click ACTIVATEMYDEVICE

    	click TERMSACCEPT
    	click CONTINUE
 
		#Select phone number
    	case numType
    		when 'US'
		    	wait_for(5){selectByText CPNSTATE, 'TN'}
		    	selectByText CPNAREACODE, '999'
		    	selectByIndex CPNAREAEXCHANGE, '6'
		    when 'Vanity'
		        click CPNVANITY
           		wait_for(5){selectByText CPNSTATE, 'TN'}
		    	selectByText CPNAREACODE, '999'		    	
		    	type  '99999', CPNSEARCVAN
		    	click CPNSEARCHBTN
		    	selectByIndex CPNCALLINGPN, '3'		    		
		    when 'Custom'
		    	click CPNCHOOSEDIGITS
		    	wait_for(5){selectByText CPNSTATE, 'TN'}
		    	selectByText CPNAREACODE, '999'	   
		    	selectByIndex CPNAREAEXCHANGE, '6'	
		    	selectByIndex CPNCUSTOMNUM, '1'	
		    when 'Canadian'
		    	click CPNCANADIAN 
		    	wait_for(5){selectByText CPNSTATE, 'Ontario'}
		    	selectByText CPNAREACODE, '999'	   
		    	selectByIndex CPNAREAEXCHANGE, '1'	
		    else
		    	p 'Invalid Number Type'
		    	exit
		end
    	   	
    	if serviceType == 'IN'				# Block outbound calling
			click RSUBSERVICETYPE
		end

	   	click CPNADDTOCART					# Finish the phone number
    		
		if addr911 == 'SKIP'			# Skip 911 completely
			click E911NONUSCHECK
			click E911CONFIRMIT
		else
			#selectByText E911LOCATION, '700 2nd Ave, Nashville TN 37210'
			#click E911CONFIRMLOC
			
			if addr911 == '911addressCorrect' then
				selectByText E911LOCATION, '700 2nd Ave, Nashville TN 37210'
				click E911CONFIRMIT
				click E911CONFIRM
			else
				#click E911CONFIRMNO        	
				click E911LOCATIONADD
				type 'TestFname', E911FIRSTNAME 
				type 'TestLname' , E911LASTNAME
				type '3880 Priest Lake Dr', E911STREETNAME
				type 'Apt 96', E911ADDR2
				type 'Nashville', E911CITY
				type 'TN', E911STATE
				type '37210', E911ZIPCODE
				click E911LOCADDCONF
				click E911CONFIRM
				click E911INCITY
			end
					
			if inCity == 'inCityYes' then
				click E911INCITY
			else
				click E911INCITYNO
			end
			
			click E911CONFIRMCITY
		end
        
        if jack_type == "Generate magicJackGO Mexico"  #Renewals and IPP pages don't appear for Mexico GO
			wait_for(5){click EXITBTNLOWER} 
			wait_for(5){click CLOSEREG}
		else
			case renewType
				when '5YR'
					click FIVEYEAR
					click AUTORENEWFIVEYEAR
					click ADDORENEWALTOCART
				when '1YR'
					click ONEYEAR
					click AUTORENEWONEYEAR
					click ADDORENEWALTOCART
				when 'SKIP'
					click SKIPFORNOW
				else
					click SKIPFORNOW
			end
		   
		    case ippType
				when 'IPP5'
					click TAKEIPP5
					click PROCEEDTOCHECKOUT        		
				when 'IPP10'
					click TAKEIPP10
					click PROCEEDTOCHECKOUT 
				when 'IPP20'
					click TAKEIPP20
					click PROCEEDTOCHECKOUT
				when 'IPP40'
					click TAKEIPP40
					click PROCEEDTOCHECKOUT       		
				when 'SKIP'
					click SKIPFORNOW
				else
					click SKIPFORNOW
		    end              
			
			if renewType == "SKIP" && ippType == "SKIP" && numType == "US" then
				wait_for(5){click PLACEORDER}
				wait_for(5){click EXITBTNLOWER}
				wait_for(5){click CLOSEREG}
			else
				if newbillLocation == 'NO'
					if numType == 'Canadian'
						wait_for(10){selectByText CCLOCATION, '470 GLENRIDGE AVE, ST CATHARINES ON L2T4C3'}
					else
						wait_for(10){selectByText CCLOCATION, '700 2nd Ave, Nashville TN 37210'}
					end
				else
					click ADDNEWBILLLOC
					type 'TestBill', NEWBILLFIRSTNAME 
					type 'TestLoc' , NEWBILLLASTNAME
					type '318 Seaboard Lane', NEWBILLSTREETNAME
					type 'Suite 308', NEWBILLLOCATION
					type 'Franklin', NEWBILLCITY
					type 'TN', NEWBILLSTATE
					type '37067', NEWBILLZIP			 
				end
				
			   type newCC, CARDNUMBER
			   type '123', CVV2
			   selectByIndex  CCEXPMONTH,'4'
			   selectByIndex CCEXPYEAR, '4'
			   wait_for(5){click REVIEWORDER}	      	            
			   wait_for(5){click PLACEORDER}	        
			   wait_for(5){click EXITBTNLOWER}
			   wait_for(5){click CLOSEREG}
			end
		end
        return "reg_successful"
	rescue => e
		 @regWorked=1
		@driver.save_screenshot('./reg_failed_screenshot.png')
		message = "REGISTRATION FAILED\n" + e.message + e.backtrace.join("\n")		
		return message		
	end
  end
end